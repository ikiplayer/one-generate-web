export async function getLeftMenuData() {
  return [
    {
      title: 'Wordpress',
      key: 'settings',
      url: '/wordpress',
      icon: 'icmn icmn-cog utils__spin-delayed--pseudo-selector',
    },
    {
      divider: true,
    },
    {
      title: 'Open Graph',
      key: 'open_graph',
      url: '/open-graph',
      icon: 'icmn icmn-books',
    },
    {
      divider: true,
    },
    {
      title: 'JSON-LD',
      key: 'json_ld',
      url: '/json-ld',
      icon: 'icmn icmn-books',
    },
    {
      divider: true,
    },
    {
      title: 'Microdata',
      key: 'microdata',
      url: '/microdata',
      icon: 'icmn icmn-books',
    },
    {
      divider: true,
    },
    {
      title: 'HTML',
      key: 'html',
      url: '/html',
      icon: 'icmn icmn-books',
    },
    {
      divider: true,
    },

    // {
    //   title: 'Dashboard Alpha',
    //   key: 'dashboardAlpha',
    //   url: '/dashboard/alpha',
    //   icon: 'icmn icmn-home',
    // },
  ]
}
export async function getTopMenuData() {
  return [
    // {
    //   title: 'Settings',
    //   key: 'settings',
    //   icon: 'icmn icmn-cog utils__spin-delayed--pseudo-selector',
    // },
    // {
    //   title: 'Docs',
    //   key: 'documentation',
    //   url: 'https://docs.cleanuitemplate.com/react/getting-started',
    //   target: '_blank',
    //   icon: 'icmn icmn-books',
    // },
    {
      title: 'Home',
      key: 'dashboards',
      icon: 'icmn icmn-home',
      children: [
        {
          title: 'Home',
          key: 'dashboardAlpha',
          url: '/home',
        },
      ],
    },
    {
      title: 'Wordpress',
      key: 'wordpressIndex',
      children: [
        {
          title: 'Wordpress',
          key: 'wordpress',
          url: '/wordpress',
        },
      ],
    },
    {
      title: 'Open Graph',
      key: 'openGraph',
      children: [
        {
          title: 'Open Graph',
          key: 'openGraph',
          url: '/open-graph',
        },
      ],
    },

    {
      title: 'JSON-LD',
      key: 'json-ld',
      children: [
        {
          title: 'JSON-LD',
          key: 'json-ld',
          url: '/json-ld',
        },
      ],
    },
    {
      title: 'Microdata',
      key: 'microdata',
      children: [
        {
          title: 'Microdata',
          key: 'microdata',
          url: '/microdata',
        },
      ],
    },
    {
      title: 'HTML',
      key: 'html',
      children: [
        // {
        //   title: 'HTML',
        //   key: 'html',
        //   url: '/html',
        // },
        {
          title: 'Bootstrap',
          key: 'bootstrap',
          url: '/html/bootstrap',
        },
      ],
    },
    {
      title: 'Upcoming',
      key: 'upcoming',
      children: [
        {
          title: 'CSS',
          key: 'CSS',
          url: '/',
        },
        {
          title: 'HTML',
          key: 'HTML',
          url: '/',
        },
        {
          title: 'Image',
          key: 'Image',
          url: '/',
        },
        {
          title: 'Search Engine Optimization',
          key: 'SEO',
          url: '/',
        },
        {
          title: 'Password',
          key: 'password',
          url: '/',
        },
        {
          title: 'Conversions',
          key: 'conversions',
          url: '/',
        },
      ],
    },
  ]
}
