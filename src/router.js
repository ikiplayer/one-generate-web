import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import Loadable from 'react-loadable'

import Loader from 'components/LayoutComponents/Loader/index.jsx'
import IndexLayout from 'layouts'
import NotFoundPage from 'pages/404.jsx'
// import htmlRoutes from 'router.html.js'
import wordpressRoutes from 'router.wordpress.js'
import openGraphRoutes from 'router.opengraph.js'
import jsonLdRoutes from 'router.jsonld.js'
import microdataRoutes from 'router.microdata.js'
import bootstrapRoutes from 'router.bootstrap.js'
import htmlRoutes from 'router.html.js'

const loadable = loader =>
  Loadable({
    loader,
    delay: false,
    loading: () => <Loader />,
  })

const mainRoutes = [
  {
    path: '/home',
    component: import('pages/Home/Home.jsx'),
  },
  {
    path: '/wordpress',
    component: import('pages/Wordpress/index.jsx'),
  },
  {
    path: '/open-graph',
    component: import('pages/Wordpress/index.jsx'),
  },
  {
    path: '/json-ld',
    component: import('pages/JSONLD/index.jsx'),
  },
  {
    path: '/microdata',
    component: import('pages/Microdata/index.jsx'),
  },
  {
    path: '/html',
    component: import('pages/HTML/modules/HTML/index.jsx'),
  },
  {
    path: '/html/bootstrap',
    component: import('pages/HTML/modules/Bootstrap4/index.jsx'),
  },
  ...wordpressRoutes,
  ...openGraphRoutes,
  ...jsonLdRoutes,
  ...microdataRoutes,
  ...htmlRoutes,
  ...bootstrapRoutes,
]

export class MainRouter extends React.Component {
  render() {
    const { history } = this.props
    return (
      <ConnectedRouter history={history}>
        <IndexLayout>
          <Switch>
            <Route exact path="/" render={() => <Redirect to="/home" />} />
            {mainRoutes.map(route => (
              <Route
                exact
                path={route.path}
                component={loadable(() => route.component)}
                key={route.path}
              />
            ))}
            <Route exact component={NotFoundPage} />
          </Switch>
        </IndexLayout>
      </ConnectedRouter>
    )
  }
}
