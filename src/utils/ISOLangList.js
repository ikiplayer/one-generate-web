export var countryToLanguages = {
    AD: ['ca'],
    AE: ['ar', 'fa', 'en', 'hi', 'ur'],
    AF: ['fa', 'ps', 'uz', 'tk'],
    AG: ['en'],
    AI: ['en'],
    AL: ['sq', 'el'],
    AM: ['hy'],
    AO: ['pt'],
    AR: ['es', 'en', 'it', 'de', 'fr', 'gn'],
    AS: ['en', 'sm', 'to'],
    AT: ['de', 'hr', 'hu', 'sl'],
    AU: ['en'],
    AW: ['nl', 'es', 'en'],
    AX: ['sv'],
    AZ: ['az', 'ru', 'hy'],
    BA: ['bs', 'hr', 'sr'],
    BB: ['en'],
    BD: ['bn', 'en'],
    BE: ['nl', 'fr', 'de'],
    BF: ['fr'],
    BG: ['bg', 'tr'],
    BH: ['ar', 'en', 'fa', 'ur'],
    BI: ['fr', 'rn'],
    BJ: ['fr'],
    BL: ['fr'],
    BM: ['en', 'pt'],
    BN: ['ms', 'en'],
    BO: ['es', 'qu', 'ay'],
    BQ: ['nl', 'en'],
    BR: ['pt', 'es', 'en', 'fr'],
    BS: ['en'],
    BT: ['dz'],
    BW: ['en', 'tn'],
    BY: ['be', 'ru'],
    BZ: ['en', 'es'],
    CA: ['en', 'fr', 'iu'],
    CC: ['ms', 'en'],
    CD: ['fr', 'ln', 'kg'],
    CF: ['fr', 'sg', 'ln', 'kg'],
    CG: ['fr', 'kg', 'ln'],
    CH: ['de', 'fr', 'it', 'rm'],
    CI: ['fr'],
    CK: ['en', 'mi'],
    CL: ['es'],
    CM: ['en', 'fr'],
    CN: ['zh', 'ug', 'za'],
    CO: ['es'],
    CR: ['es', 'en'],
    CU: ['es'],
    CV: ['pt'],
    CW: ['nl'],
    CX: ['en', 'zh', 'ms'],
    CY: ['el', 'tr', 'en'],
    CZ: ['cs', 'sk'],
    DE: ['de'],
    DJ: ['fr', 'ar', 'so', 'aa'],
    DK: ['da', 'en', 'fo', 'de'],
    DM: ['en'],
    DO: ['es'],
    DZ: ['ar'],
    EC: ['es'],
    EE: ['et', 'ru'],
    EG: ['ar', 'en', 'fr'],
    EH: ['ar'],
    ER: ['aa', 'ar', 'ti'],
    ES: ['es', 'ca', 'gl', 'eu', 'oc'],
    ET: ['am', 'en', 'om', 'ti', 'so'],
    FI: ['fi', 'sv'],
    FJ: ['en', 'fj'],
    FK: ['en'],
    FM: ['en'],
    FO: ['fo', 'da'],
    FR: ['fr', 'br', 'co', 'ca', 'eu', 'oc'],
    GA: ['fr'],
    GB: ['en', 'cy', 'gd'],
    GD: ['en'],
    GE: ['ka', 'ru', 'hy', 'az'],
    GF: ['fr'],
    GG: ['en', 'fr'],
    GH: ['en', 'ak', 'ee', 'tw'],
    GI: ['en', 'es', 'it', 'pt'],
    GL: ['kl', 'da', 'en'],
    GM: ['en', 'wo', 'ff'],
    GN: ['fr'],
    GP: ['fr'],
    GQ: ['es', 'fr'],
    GR: ['el', 'en', 'fr'],
    GS: ['en'],
    GT: ['es'],
    GU: ['en', 'ch'],
    GW: ['pt'],
    GY: ['en'],
    HK: ['zh', 'zh', 'en'],
    HN: ['es'],
    HR: ['hr', 'sr'],
    HT: ['ht', 'fr'],
    HU: ['hu'],
    ID: ['id', 'en', 'nl', 'jv'],
    IE: ['en', 'ga'],
    IL: ['he', 'ar', 'en'],
    IM: ['en', 'gv'],
    IN: ['en', 'hi', 'bn', 'te', 'mr', 'ta', 'ur', 'gu', 'kn', 'ml', 'or', 'pa', 'as', 'bh', 'ks', 'ne', 'sd', 'sa', 'fr'],
    IO: ['en'],
    IQ: ['ar', 'ku', 'hy'],
    IR: ['fa', 'ku'],
    IS: ['is', 'en', 'de', 'da', 'sv', 'no'],
    IT: ['it', 'de', 'fr', 'sc', 'ca', 'co', 'sl'],
    JE: ['en', 'pt'],
    JM: ['en'],
    JO: ['ar', 'en'],
    JP: ['ja'],
    KE: ['en', 'sw'],
    KG: ['ky', 'uz', 'ru'],
    KH: ['km', 'fr', 'en'],
    KI: ['en'],
    KM: ['ar', 'fr'],
    KN: ['en'],
    KP: ['ko'],
    KR: ['ko', 'en'],
    XK: ['sq', 'sr'],
    KW: ['ar', 'en'],
    KY: ['en'],
    KZ: ['kk', 'ru'],
    LA: ['lo', 'fr', 'en'],
    LB: ['ar', 'fr', 'en', 'hy'],
    LC: ['en'],
    LI: ['de'],
    LK: ['si', 'ta', 'en'],
    LR: ['en'],
    LS: ['en', 'st', 'zu', 'xh'],
    LT: ['lt', 'ru', 'pl'],
    LU: ['lb', 'de', 'fr'],
    LV: ['lv', 'ru', 'lt'],
    LY: ['ar', 'it', 'en'],
    MA: ['ar', 'fr'],
    MC: ['fr', 'en', 'it'],
    MD: ['ro', 'ru', 'tr'],
    ME: ['sr', 'hu', 'bs', 'sq', 'hr'],
    MF: ['fr'],
    MG: ['fr', 'mg'],
    MH: ['mh', 'en'],
    MK: ['mk', 'sq', 'tr', 'sr'],
    ML: ['fr', 'bm'],
    MM: ['my'],
    MN: ['mn', 'ru'],
    MO: ['zh', 'zh', 'pt'],
    MP: ['tl', 'zh', 'ch', 'en'],
    MQ: ['fr'],
    MR: ['ar', 'fr', 'wo'],
    MS: ['en'],
    MT: ['mt', 'en'],
    MU: ['en', 'fr'],
    MV: ['dv', 'en'],
    MW: ['ny'],
    MX: ['es'],
    MY: ['ms', 'en', 'zh', 'ta', 'te', 'ml', 'pa', 'th'],
    MZ: ['pt'],
    NA: ['en', 'af', 'de', 'hz'],
    NC: ['fr'],
    NE: ['fr', 'ha', 'kr'],
    NF: ['en'],
    NG: ['en', 'ha', 'yo', 'ig', 'ff'],
    NI: ['es', 'en'],
    NL: ['nl', 'fy'],
    NO: ['no', 'nb', 'nn', 'se', 'fi'],
    NP: ['ne', 'en'],
    NR: ['na', 'en'],
    NU: ['en'],
    NZ: ['en', 'mi'],
    OM: ['ar', 'en', 'ur'],
    PA: ['es', 'en'],
    PE: ['es', 'qu', 'ay'],
    PF: ['fr', 'ty'],
    PG: ['en', 'ho'],
    PH: ['tl', 'en'],
    PK: ['ur', 'en', 'pa', 'sd', 'ps'],
    PL: ['pl'],
    PM: ['fr'],
    PN: ['en'],
    PR: ['en', 'es'],
    PS: ['ar'],
    PT: ['pt'],
    PW: ['en', 'ja', 'zh'],
    PY: ['es', 'gn'],
    QA: ['ar', 'es'],
    RE: ['fr'],
    RO: ['ro', 'hu'],
    RS: ['sr', 'hu', 'bs'],
    RU: ['ru', 'tt', 'kv', 'ce', 'cv', 'ba'],
    RW: ['rw', 'en', 'fr', 'sw'],
    SA: ['ar'],
    SB: ['en'],
    SC: ['en', 'fr'],
    SD: ['ar', 'en'],
    SS: ['en'],
    SE: ['sv', 'se', 'fi'],
    SG: ['en', 'ms', 'ta', 'zh'],
    SH: ['en'],
    SI: ['sl', 'sh'],
    SJ: ['no', 'ru'],
    SK: ['sk', 'hu'],
    SL: ['en'],
    SM: ['it'],
    SN: ['fr', 'wo'],
    SO: ['so', 'ar', 'it', 'en'],
    SR: ['nl', 'en', 'jv'],
    ST: ['pt'],
    SV: ['es'],
    SX: ['nl', 'en'],
    SY: ['ar', 'ku', 'hy', 'fr', 'en'],
    SZ: ['en', 'ss'],
    TC: ['en'],
    TD: ['fr', 'ar'],
    TF: ['fr'],
    TG: ['fr', 'ee', 'ha'],
    TH: ['th', 'en'],
    TJ: ['tg', 'ru'],
    TK: ['en'],
    TL: ['pt', 'id', 'en'],
    TM: ['tk', 'ru', 'uz'],
    TN: ['ar', 'fr'],
    TO: ['to', 'en'],
    TR: ['tr', 'ku', 'az', 'av'],
    TT: ['en', 'fr', 'es', 'zh'],
    TV: ['en', 'sm'],
    TW: ['zh', 'zh'],
    TZ: ['sw', 'en', 'ar'],
    UA: ['uk', 'ru', 'pl', 'hu'],
    UG: ['en', 'lg', 'sw', 'ar'],
    UM: ['en'],
    US: ['en', 'es', 'fr'],
    UY: ['es'],
    UZ: ['uz', 'ru', 'tg'],
    VA: ['la', 'it', 'fr'],
    VC: ['en', 'fr'],
    VE: ['es'],
    VG: ['en'],
    VI: ['en'],
    VN: ['vi', 'en', 'fr', 'zh', 'km'],
    VU: ['bi', 'en', 'fr'],
    WF: ['fr'],
    WS: ['sm', 'en'],
    YE: ['ar'],
    YT: ['fr'],
    ZA: ['zu', 'xh', 'af', 'en', 'tn', 'st', 'ts', 'ss', 've', 'nr'],
    ZM: ['en', 'ny'],
    ZW: ['en', 'sn', 'nr', 'nd'],
    CS: ['cu', 'hu', 'sq', 'sr'],
    AN: ['nl', 'en', 'es']
};

export function getLanguages(countryCode) {
    if (countryToLanguages.hasOwnProperty(countryCode)) {
        return countryToLanguages[countryCode];
    } else {
        return [];
    }
}