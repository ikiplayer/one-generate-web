module.exports = [
  {
    path: '/html',
    component: import('pages/HTML/modules/HTML/index.jsx'),
    exact: true,
  },
  // INPUT
  {
    path: '/html/button',
    component: import('pages/HTML/modules/HTML/Input/Button.jsx'),
  },
  {
    path: '/html/checkbox-radio-button',
    component: import('pages/HTML/modules/HTML/Input/CheckboxRadioButton.jsx'),
  },
  {
    path: '/html/color',
    component: import('pages/HTML/modules/HTML/Input/Color.jsx'),
  },
  {
    path: '/html/date-time',
    component: import('pages/HTML/modules/HTML/Input/DateTime.jsx'),
  },
  {
    path: '/html/email',
    component: import('pages/HTML/modules/HTML/Input/Email.jsx'),
  },
  {
    path: '/html/file-browser',
    component: import('pages/HTML/modules/HTML/Input/FileBrowser.jsx'),
  },
  {
    path: '/html/image',
    component: import('pages/HTML/modules/HTML/Media/Image.jsx'),
  },
  {
    path: '/html/number-range-slider',
    component: import('pages/HTML/modules/HTML/Input/NumberRangeSlider.jsx'),
  },
  {
    path: '/html/password',
    component: import('pages/HTML/modules/HTML/Input/Password.jsx'),
  },
  {
    path: '/html/search',
    component: import('pages/HTML/modules/HTML/Input/Search.jsx'),
  },
  {
    path: '/html/submit',
    component: import('pages/HTML/modules/HTML/Input/Submit.jsx'),
  },
  {
    path: '/html/telephone',
    component: import('pages/HTML/modules/HTML/Input/Telephone.jsx'),
  },
  {
    path: '/html/text',
    component: import('pages/HTML/modules/HTML/Input/Text.jsx'),
  },
  {
    path: '/html/text-area',
    component: import('pages/HTML/modules/HTML/Input/TextArea.jsx'),
  },
  {
    path: '/html/url',
    component: import('pages/HTML/modules/HTML/Input/Url.jsx'),
  },
  // MEDIA
  {
    path: '/html/audio',
    component: import('pages/HTML/modules/HTML/Media/Audio.jsx'),
  },
  {
    path: '/html/image',
    component: import('pages/HTML/modules/HTML/Media/Image.jsx'),
  },
  {
    path: '/html/video',
    component: import('pages/HTML/modules/HTML/Media/Video.jsx'),
  },
  // OTHER ELEMENTS
  {
    path: '/html/base-link',
    component: import('pages/HTML/modules/HTML/Other/BaseLink.jsx'),
  },

  {
    path: '/html/details',
    component: import('pages/HTML/modules/HTML/OtherElements/Details.jsx'),
  },
  {
    path: '/html/dialog',
    component: import('pages/HTML/modules/HTML/OtherElements/Dialog.jsx'),
  },

  {
    path: '/html/hyperlink',
    component: import('pages/HTML/modules/HTML/OtherElements/Hyperlink.jsx'),
  },
  {
    path: '/html/iframe',
    component: import('pages/HTML/modules/HTML/OtherElements/IFrame.jsx'),
  },

  {
    path: '/html/meter',
    component: import('pages/HTML/modules/HTML/OtherElements/Meter.jsx'),
  },
  {
    path: '/html/progress-bar',
    component: import('pages/HTML/modules/HTML/OtherElements/ProgressBar.jsx'),
  },
  {
    path: '/html/bidirectional-override',
    component: import('pages/HTML/modules/HTML/Text/BiDirectionalOverride.jsx'),
  },
  {
    path: '/html/bold',
    component: import('pages/HTML/modules/HTML/Text/Bold.jsx'),
  },
  {
    path: '/html/cite',
    component: import('pages/HTML/modules/HTML/Text/Cite.jsx'),
  },
  {
    path: '/html/code',
    component: import('pages/HTML/modules/HTML/Text/Code.jsx'),
  },
  {
    path: '/html/highlight',
    component: import('pages/HTML/modules/HTML/Text/Highlight.jsx'),
  },
  {
    path: '/html/italic',
    component: import('pages/HTML/modules/HTML/Text/Italic.jsx'),
  },
  {
    path: '/html/quote-blockquote',
    component: import('pages/HTML/modules/HTML/Text/QuoteBlockQuote.jsx'),
  },
  {
    path: '/html/strikethrough',
    component: import('pages/HTML/modules/HTML/Text/Strikethrough.jsx'),
  },
  {
    path: '/html/superscript-subscript',
    component: import('pages/HTML/modules/HTML/Text/SuperscriptSubscript.jsx'),
  },
  {
    path: '/html/underline',
    component: import('pages/HTML/modules/HTML/Text/Underline.jsx'),
  },
]
