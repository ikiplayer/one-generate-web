import React from 'react'
import { BackTop, Layout } from 'antd'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import classNames from 'classnames'
import TopBar from 'components/LayoutComponents/TopBar/index.jsx'
import Menu from 'components/LayoutComponents/Menu/index.jsx'
import Footer from 'components/LayoutComponents/Footer/index.jsx'
// import Footer from 'components/LayoutComponents/Footer'
import Breadcrumbs from 'components/LayoutComponents/Breadcrumbs/index.jsx'
// import Settings from 'components/LayoutComponents/Settings'

const mapStateToProps = ({ settings }) => ({
  isBorderless: false,
  isSquaredBorders: false,
  isFixedWidth: true,
  isMenuShadow: false,
  isMenuTop: true,
  isMenuCollapsed: true,
})

@withRouter
@connect(mapStateToProps)
class MainLayout extends React.PureComponent {
  render() {
    const {
      children,
      isBorderless,
      isSquaredBorders,
      isFixedWidth,
      isMenuShadow,
      isMenuTop,
    } = this.props
    return (
      <Layout
        className={classNames({
          settings__borderLess: isBorderless,
          settings__squaredBorders: isSquaredBorders,
          settings__fixedWidth: false,
          settings__menuShadow: isMenuShadow,
          settings__menuTop: isMenuTop,
        })}
      >
        <BackTop />
        <TopBar />
        {/* <Settings /> */}
        <Layout>
          <Layout.Header style={{ backgroundColor: 'transparent', height: 'auto' }}>
            <Menu />
          </Layout.Header>
          <Breadcrumbs />
          <Layout.Content
            style={{
              paddingLeft: '10%',
              paddingRight: '10%',
              height: '100%',
              position: 'relative',
            }}
          >
            <div className="utils__content">{children}</div>
          </Layout.Content>
          <Layout.Footer>
            <Footer />
          </Layout.Footer>
        </Layout>
      </Layout>
    )
  }
}

export default MainLayout
