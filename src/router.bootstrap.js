module.exports = [
  {
    path: '/html/bootstrap',
    component: import('pages/HTML/modules/Bootstrap4/index.jsx'),
    exact: true,
  },
  // BOOTSTRAP MODULES
  {
    path: '/html/bootstrap/alert',
    component: import('pages/HTML/modules/Bootstrap4/Alert.jsx'),
  },
  {
    path: '/html/bootstrap/badges',
    component: import('pages/HTML/modules/Bootstrap4/Badges.jsx'),
  },
  {
    path: '/html/bootstrap/button',
    component: import('pages/HTML/modules/Bootstrap4/Button.jsx'),
  },
  {
    path: '/html/bootstrap/button-group',
    component: import('pages/HTML/modules/Bootstrap4/ButtonGroup.jsx'),
  },
  {
    path: '/html/bootstrap/cards',
    component: import('pages/HTML/modules/Bootstrap4/Cards.jsx'),
  },
  {
    path: '/html/bootstrap/collapse',
    component: import('pages/HTML/modules/Bootstrap4/Collapse.jsx'),
  },
  {
    path: '/html/bootstrap/colors',
    component: import('pages/HTML/modules/Bootstrap4/Colors.jsx'),
  },
  {
    path: '/html/bootstrap/dropdown',
    component: import('pages/HTML/modules/Bootstrap4/Dropdown.jsx'),
  },
  {
    path: '/html/bootstrap/form',
    component: import('pages/HTML/modules/Bootstrap4/Form.jsx'),
  },
  {
    path: '/html/bootstrap/image',
    component: import('pages/HTML/modules/Bootstrap4/Image.jsx'),
  },
  {
    path: '/html/bootstrap/input',
    component: import('pages/HTML/modules/Bootstrap4/Input.jsx'),
  },
  {
    path: '/html/bootstrap/jumbotron',
    component: import('pages/HTML/modules/Bootstrap4/Jumbotron.jsx'),
  },
  {
    path: '/html/bootstrap/list-group',
    component: import('pages/HTML/modules/Bootstrap4/ListGroups.jsx'),
  },
  {
    path: '/html/bootstrap/modal',
    component: import('pages/HTML/modules/Bootstrap4/Modal.jsx'),
  },
  {
    path: '/html/bootstrap/nav',
    component: import('pages/HTML/modules/Bootstrap4/Nav.jsx'),
  },
  {
    path: '/html/bootstrap/pagination',
    component: import('pages/HTML/modules/Bootstrap4/Pagination.jsx'),
  },
  {
    path: '/html/bootstrap/popover',
    component: import('pages/HTML/modules/Bootstrap4/Popover.jsx'),
  },
  {
    path: '/html/bootstrap/progress-bar',
    component: import('pages/HTML/modules/Bootstrap4/ProgressBar.jsx'),
  },
  {
    path: '/html/bootstrap/spinner',
    component: import('pages/HTML/modules/Bootstrap4/Spinner.jsx'),
  },
  {
    path: '/html/bootstrap/table',
    component: import('pages/HTML/modules/Bootstrap4/Table.jsx'),
  },
  {
    path: '/html/bootstrap/toast',
    component: import('pages/HTML/modules/Bootstrap4/Toast.jsx'),
  },
  {
    path: '/html/bootstrap/tooltip',
    component: import('pages/HTML/modules/Bootstrap4/Tooltip.jsx'),
  },
  {
    path: '/html/bootstrap/typography',
    component: import('pages/HTML/modules/Bootstrap4/Typography.jsx'),
  },
]
