import React from 'react'
import { Button } from 'antd'
import styles from './style.module.scss'

const Footer = () => (
  <div className={styles.footer}>
    <div className={styles.inner}>
      {/* <div className="row">
        <div className="col-lg-9">
          <p>
            <strong>One Generator</strong>
          </p>
        </div>
      </div> */}
      <div className>
        <div className="row">
          <div className="col-sm-6">
            <p>
              <strong>One Generate</strong>
            </p>
            {/* <a
              href="https://themeforest.net/item/clean-ui-react-admin-template/21938700"
              target="_blank"
              rel="noopener noreferrer"
              className="mr-4"
            > */}
            {/* <Button type="primary">Buy Now 24$</Button> */}
            {/* </a> */}
          </div>
          <div className="col-sm-6">
            <div className={styles.copyright}>
              <img src="resources/images/og.png" rel="noopener noreferrer" alt="One Generate" />
              <span>
                © 2019{' '}
                <a href="http://mediatec.org/" target="_blank" rel="noopener noreferrer">
                  One Generate
                </a>
                <br />
                All rights reserved
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Footer
