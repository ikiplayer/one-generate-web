module.exports = [
  {
    path: '/wordpress',
    component: import('pages/Wordpress/index.jsx'),
    exact: true,
    lazy: false,
  },
  {
    path: '/wordpress/cron-job-event',
    component: import('./pages/Wordpress/Modules/CronJobEvent.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/custom-post-generator',
    component: import('./pages/Wordpress/Modules/CustomPostGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/dashboard-widgets-generator',
    component: import('./pages/Wordpress/Modules/DashboardWidgetsGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/login-form-generator',
    component: import('./pages/Wordpress/Modules/LoginFormGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/menu-generator',
    component: import('./pages/Wordpress/Modules/MenuGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/meta-box-generator',
    component: import('./pages/Wordpress/Modules/MetaBoxGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/post-status-generator',
    component: import('./pages/Wordpress/Modules/PostStatusGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/quicktag-for-classic-editor-generator',
    component: import('./pages/Wordpress/Modules/QuicktagForClassicEditorGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/register-enqueue-scripts-js',
    component: import('./pages/Wordpress/Modules/RegisterEnqueueScriptsJs.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/register-enqueue-styles-css',
    component: import('./pages/Wordpress/Modules/RegisterEnqueueStylesCss.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/register-image-size',
    component: import('./pages/Wordpress/Modules/RegisterImageSize.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/settings-options-page-generator',
    component: import('./pages/Wordpress/Modules/SettingsOptionsPageGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/shortcode-generator',
    component: import('./pages/Wordpress/Modules/ShortcodeGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/sidebar-generator',
    component: import('./pages/Wordpress/Modules/SidebarGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/tax-query-generator',
    component: import('./pages/Wordpress/Modules/TaxQueryGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/term-meta-generator',
    component: import('./pages/Wordpress/Modules/TermMetaGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/user-contact-methods-generator',
    component: import('./pages/Wordpress/Modules/UserContactMethodsGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/visual-composer-elementor-generator',
    component: import('./pages/Wordpress/Modules/VisualComposerElementorGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/widget-generator',
    component: import('./pages/Wordpress/Modules/WidgetGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/wp-mail-function-generator',
    component: import('./pages/Wordpress/Modules/WPMailFunctionGenerator.jsx'),
    lazy: true,
  },
  {
    path: '/wordpress/wp-query-loop-generator',
    component: import('./pages/Wordpress/Modules/WPQueryLoopGenerator.jsx'),
    lazy: true,
  },
]
