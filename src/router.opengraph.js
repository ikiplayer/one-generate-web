module.exports = [
  {
    path: '/open-graph',
    name: 'Open Graph',
    component: import('pages/OpenGraph/index.jsx'),
    exact: true,
  },
  {
    path: '/open-graph/article',
    name: 'Article',

    component: import('pages/OpenGraph/Modules/Article.jsx'),
    description: 'Generate code for articles for Open Graph',
  },
  {
    path: '/open-graph/book',
    name: 'Book',

    component: import('pages/OpenGraph/Modules/Book.jsx'),
    description: 'Generate code for books for Open Graph',
  },
  {
    path: '/open-graph/book-author',
    name: 'Book Author',

    component: import('pages/OpenGraph/Modules/BookAuthor.jsx'),
    description: 'Generate code for book author for Open Graph',
  },
  {
    path: '/open-graph/book-genre',
    name: 'Book Genre',

    component: import('pages/OpenGraph/Modules/BookGenre.jsx'),
    description: 'Generate code for book genre for Open Graph',
  },
  {
    path: '/open-graph/business',
    name: 'Business',

    component: import('pages/OpenGraph/Modules/Business.jsx'),
    description: 'Generate code for business for Open Graph',
  },
  {
    path: '/open-graph/fitness-course',
    name: 'Fitness Course',

    component: import('pages/OpenGraph/Modules/FitnessCourse.jsx'),
    description: 'Generate code for fitness course for Open Graph',
  },
  {
    path: '/open-graph/game-achievement',
    name: 'Game Achievement',

    component: import('pages/OpenGraph/Modules/GameAchievement.jsx'),
    description: 'Generate code for game achievement for Open Graph',
  },
  {
    path: '/open-graph/music-album',
    name: 'Music Album',

    component: import('pages/OpenGraph/Modules/MusicAlbum.jsx'),
    description: 'Generate code for music album for Open Graph',
  },
  {
    path: '/open-graph/music-playlist',
    name: 'Music Playlist',

    component: import('pages/OpenGraph/Modules/MusicPlaylist.jsx'),
    description: 'Generate code for music playlist for Open Graph',
  },
  {
    path: '/open-graph/music-radio-station',
    name: 'Music Radio Station',

    component: import('pages/OpenGraph/Modules/MusicRadioStation.jsx'),
    description: 'Generate code for music radio for Open Graph',
  },
  {
    path: '/open-graph/music-song',
    name: 'Music Song',

    component: import('pages/OpenGraph/Modules/MusicSong.jsx'),
    description: 'Generate code for music song for Open Graph',
  },
  {
    path: '/open-graph/place',
    name: 'Place',

    component: import('pages/OpenGraph/Modules/Place.jsx'),
    description: 'Generate code for place for Open Graph',
  },
  {
    path: '/open-graph/product',
    name: 'Product',

    component: import('pages/OpenGraph/Modules/Product.jsx'),
    description: 'Generate code for product for Open Graph',
  },
  {
    path: '/open-graph/product-group',
    name: 'Product Group',

    component: import('pages/OpenGraph/Modules/ProductGroup.jsx'),
    description: 'Generate code for product group for Open Graph',
  },
  {
    path: '/open-graph/product-item',
    name: 'Product Item',

    component: import('pages/OpenGraph/Modules/ProductItem.jsx'),
    description: 'Generate code for product item for Open Graph',
  },
  {
    path: '/open-graph/profile',
    name: 'Profile',

    component: import('pages/OpenGraph/Modules/Profile.jsx'),
    description: 'Generate code for profile for Open Graph',
  },
  {
    path: '/open-graph/restaurant',
    name: 'Restaurant',

    component: import('pages/OpenGraph/Modules/Restaurant.jsx'),
    description: 'Generate code for restaurant for Open Graph',
  },
  {
    path: '/open-graph/restaurant-menu',
    name: 'Restaurant Menu',

    component: import('pages/OpenGraph/Modules/RestaurantMenu.jsx'),
    description: 'Generate code for restaurant menu for Open Graph',
  },
  {
    path: '/open-graph/restaurant-menu-item',
    name: 'Restaurant Menu Item',

    component: import('pages/OpenGraph/Modules/RestaurantMenuItem.jsx'),
    description: 'Generate code for restaurant menu item for Open Graph',
  },
  {
    path: '/open-graph/restaurant-menu-section',
    name: 'Restaurant Menu Section',

    component: import('pages/OpenGraph/Modules/RestaurantMenuSection.jsx'),
    description: 'Generate code for restaurant menu section for Open Graph',
  },
  {
    path: '/open-graph/video-episode',
    name: 'Video Episode',

    component: import('pages/OpenGraph/Modules/VideoEpisode.jsx'),
    description: 'Generate code for video episode for Open Graph',
  },
  {
    path: '/open-graph/video-generic',
    name: 'Video Generic',

    component: import('pages/OpenGraph/Modules/VideoGeneric.jsx'),
    description: 'Generate code for video generic for Open Graph',
  },
  {
    path: '/open-graph/video-movie',
    component: import('pages/OpenGraph/Modules/VideoMovie.jsx'),
    description: 'Generate code for video movie for Open Graph',
  },
  {
    path: '/open-graph/video-tv-show',
    component: import('pages/OpenGraph/Modules/VideoTvShow.jsx'),
    description: 'Generate code for video tv show for Open Graph',
  },
]
