import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Snackbar, SnackbarContent, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Restaurant extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Microdata Restaurant',
      helmetDescription: 'Generate code for Microdata restaurant',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      restaurantName: '',
      acceptingReservations: 'No',
      menuUrl: '',
      cuisine: '',
      priceRange: '$',
      streetAddress: '',
      city: '',
      stateProvinceRegion: '',
      postalZipCode: '',
      phoneNumber: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let restaurantName = this.state.restaurantName
    let acceptingReservations = this.state.acceptingReservations
    let menuUrl = this.state.menuUrl
    let cuisine = this.state.cuisine
    let priceRange = this.state.priceRange
    let streetAddress = this.state.streetAddress
    let city = this.state.city
    let stateProvinceRegion = this.state.stateProvinceRegion
    let postalZipCode = this.state.postalZipCode
    let phoneNumber = this.state.phoneNumber

    let codeString = `
    <div itemscope itemtype="http://schema.org/Restaurant">
      <h2 itemprop="name">${restaurantName}</h2><br>
      Accepting reservations: <span itemprop="acceptsReservations">${acceptingReservations}</span><br>
      <a itemprop="menu" href="${menuUrl}">Menu</a><br>
      Cuisine: <span itemprop="servesCuisine">${cuisine}</span><br>
      Price range: <span itemprop="priceRange">${priceRange}</span><br>
      <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress">${streetAddress}</span><br>
          <span itemprop="addressLocality">${city}</span>, 
          <span itemprop="addressRegion">${stateProvinceRegion}</span> 
          <span itemprop="postalCode">${postalZipCode}</span>
      </div>
      Phone: <span itemprop="telephone">${phoneNumber}</span>
    </div>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Microdata_Restaurant.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Restaurant Name"
                      type="text"
                      name="restaurantName"
                      id=""
                      value={this.state.restaurantName}
                      onChange={this.handleInput('restaurantName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      select
                      label="Accepting Reservations"
                      type="text"
                      name="acceptingReservations"
                      id=""
                      value={this.state.acceptingReservations}
                      onChange={this.handleInput('acceptingReservations')}
                    >
                      <MenuItem key="0" value="Yes">
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value="No">
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Menu URL"
                      type="text"
                      name="menuUrl"
                      id=""
                      value={this.state.menuUrl}
                      onChange={this.handleInput('menuUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Cuisine"
                      type="text"
                      name="cuisine"
                      id=""
                      value={this.state.cuisine}
                      onChange={this.handleInput('cuisine')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Price Range"
                      type="text"
                      select
                      name="priceRange"
                      id=""
                      value={this.state.priceRange}
                      onChange={this.handleInput('priceRange')}
                    >
                      <MenuItem key="0" value="$">
                        $
                      </MenuItem>
                      <MenuItem key="1" value="$$">
                        $$
                      </MenuItem>
                      <MenuItem key="2" value="$$$">
                        $$$
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Street Address"
                      type="text"
                      name="streetAddress"
                      id=""
                      value={this.state.streetAddress}
                      onChange={this.handleInput('streetAddress')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="City"
                      type="text"
                      name="city"
                      id=""
                      value={this.state.city}
                      onChange={this.handleInput('city')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="State / Province / Region"
                      type="text"
                      name="stateProvinceRegion"
                      id=""
                      value={this.state.stateProvinceRegion}
                      onChange={this.handleInput('stateProvinceRegion')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Postal / Zip Code"
                      type="text"
                      name="postalZipCode"
                      id=""
                      value={this.state.postalZipCode}
                      onChange={this.handleInput('postalZipCode')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Phone Number"
                      type="text"
                      name="phoneNumber"
                      id=""
                      value={this.state.phoneNumber}
                      onChange={this.handleInput('phoneNumber')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Restaurant
