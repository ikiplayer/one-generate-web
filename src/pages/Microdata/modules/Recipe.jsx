import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import InputMask from 'react-input-mask'

const MaskInput = props => (
  <InputMask mask={props.mask} value={props.value} onChange={props.onChange}>
    {inputProps => (
      <TextField
        {...inputProps}
        fullWidth
        variant="outlined"
        label={props.label}
        type={props.type ? props.type : 'tel'}
      />
    )}
  </InputMask>
)

class Recipe extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Microdata Recipe',
      helmetDescription: 'Generate code for Microdata recipe',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      recipeName: '',
      photo: '',
      creator: '',
      description: '',
      ingredients: '',
      instructions: '',
      prepTime: '',
      cookTime: '',
      totalTime: '',
      yieldString: '',
      calories: '',
      fat: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let recipeName = this.state.recipeName
    let photo = this.state.photo
    let creator = this.state.creator
    let description = this.state.description
    let ingredients = this.state.ingredients
    let instructions = this.state.instructions
    let prepTime = this.state.prepTime
    let cookTime = this.state.cookTime
    let totalTime = this.state.totalTime
    let yieldString = this.state.yield
    let calories = this.state.calories
    let fat = this.state.fat

    let codeString = `
    <div itemscope itemtype="http://schema.org/Recipe">
        <h1 itemprop="name">${recipeName}</h1><br>
        <img itemprop="image" src="${photo}" alt="${recipeName}"><br>
        By <span itemprop="author">${creator}</span><br>
        <span itemprop="description">${description}</span><br>
        <strong>Ingredients</strong><br>
        <ul>
            <li><span itemprop="recipeIngredient">${ingredients}</span></li>
        </ul>
        <span itemprop="recipeInstructions">${instructions}</span><br>
        Prep time: <time datetime="${prepTime}" itemprop="prepTime">prepTime minutes</time><br>
        Cook time: <time datetime="${cookTime}" itemprop="cookTime">cookTime</time><br>
        Total time: <time datetime="${totalTime}" itemprop="totalTime">totalTime</time><br>
        Yield: <span itemprop="recipeYield">${yieldString}</span><br>
        <div itemprop="nutrition" itemscope itemtype="http://schema.org/NutritionInformation">
            Calories: <span itemprop="calories">${calories}</span><br>
            Fat: <span itemprop="fatContent">${fat}</span>
        </div>
    </div>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Microdata_Recipe.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Recipe Name"
                      type="text"
                      name="recipeName"
                      id=""
                      value={this.state.recipeName}
                      onChange={this.handleInput('recipeName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Photo (URL)"
                      type="text"
                      name="photo"
                      id=""
                      value={this.state.photo}
                      onChange={this.handleInput('photo')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Creator"
                      type="text"
                      name="creator"
                      id=""
                      value={this.state.creator}
                      onChange={this.handleInput('creator')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Ingredients (1 Per Line)"
                      type="text"
                      name="ingredients"
                      id=""
                      value={this.state.ingredients}
                      onChange={this.handleInput('ingredients')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Instructions"
                      type="text"
                      name="instructions"
                      id=""
                      value={this.state.instructions}
                      onChange={this.handleInput('instructions')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Prep Time (Minutes)"
                      type="number"
                      name="prepTime"
                      id=""
                      value={this.state.prepTime}
                      onChange={this.handleInput('prepTime')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <MaskInput
                      mask="99:99"
                      label="Cook time (HH:MM)"
                      value={this.state.cookTime}
                      onChange={this.handleInput('cookTime')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <MaskInput
                      mask="99:99"
                      label="Total Time (HH:MM)"
                      value={this.state.totalTime}
                      onChange={this.handleInput('totalTime')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Yield (Servings)"
                      type="text"
                      name="yield"
                      id=""
                      value={this.state.yield}
                      onChange={this.handleInput('yield')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Calories"
                      type="text"
                      name="calories"
                      id=""
                      value={this.state.calories}
                      onChange={this.handleInput('calories')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Fat"
                      type="text"
                      name="fat"
                      id=""
                      value={this.state.fat}
                      onChange={this.handleInput('fat')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Recipe
