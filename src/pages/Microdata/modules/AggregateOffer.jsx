import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Snackbar, SnackbarContent, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import numeral from 'numeral'

class AggregateOffer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Microdata Aggregate Offer ',
      helmetDescription: 'Generate code for Microdata aggregate offer',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      brand: '',
      productName: '',
      productImage: '',
      description: '',
      productNumberType: 'ASIN',
      productNumber: '',
      currency: '',
      lowPrice: '0.00',
      highPrice: '0.00',
      condition: 'New',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let brand = this.state.brand
    let productName = this.state.productName
    let productImage = this.state.productImage
    let description = this.state.description
    let productNumberType = this.state.productNumberType
    let productNumber = this.state.productNumber
    let currency = this.state.currency
    let lowPrice = this.state.lowPrice
    let highPrice = this.state.highPrice
    let condition = this.state.condition

    // let descriptionString = "";
    // if (description) {
    //   descriptionString = `\n\t<meta property="og:description" content="${description}">`;
    // }

    let codeString = `
    <div itemscope itemtype="http://schema.org/Product">
        <span itemprop="brand">${brand}</span>
        <span itemprop="name">${productName}</span><br>
        <img itemprop="image" src="${productImage}"><br>
        <span itemprop="description">${description}</span><br>
        Product number: <span itemprop="productID" content="${productNumberType}:${productNumber}"></span><br>
        <div itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
            <span itemprop="priceCurrency" content="${currency}"></span>
            From l<span itemprop="lowPrice">${lowPrice}</span><br>
            to h<span itemprop="highPrice">${highPrice}</span><br>
            Condition: <span itemprop="itemCondition" content="new">${condition}</span>
        </div>
    </div>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleTwoDecimal = value => event => {
    let stateValue = this.state[value]
    let decimalValue = numeral(stateValue).format('0.00')

    this.setState(
      {
        [value]: decimalValue,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Microdata_Aggregate_Offer.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Brand"
                      type="text"
                      name="brand"
                      id=""
                      value={this.state.brand}
                      onChange={this.handleInput('brand')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Product Name "
                      type="text"
                      name="productName"
                      id=""
                      value={this.state.productName}
                      onChange={this.handleInput('productName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Product Image"
                      type="text"
                      name="productImage"
                      id=""
                      value={this.state.productImage}
                      onChange={this.handleInput('productImage')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Product Number Type"
                      select
                      type="select"
                      name="productNumberType"
                      id=""
                      value={this.state.productNumberType}
                      onChange={this.handleInput('productNumberType')}
                    >
                      <MenuItem key="1" value="ASIN">
                        ASIN
                      </MenuItem>
                      <MenuItem key="2" value="ISBN">
                        ISBN
                      </MenuItem>
                      <MenuItem key="3" value="MPN">
                        MPN
                      </MenuItem>
                      <MenuItem key="4" value="SKU">
                        SKU
                      </MenuItem>
                      <MenuItem key="5" value="UPC">
                        UPC
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={16}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Product Number"
                      type="text"
                      name="productNumber"
                      id=""
                      value={this.state.productNumber}
                      onChange={this.handleInput('productNumber')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Currency"
                      type="text"
                      name="currency"
                      id=""
                      value={this.state.currency}
                      onChange={this.handleInput('currency')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Low Price"
                      type="number"
                      name="lowPrice"
                      id=""
                      value={this.state.lowPrice}
                      onChange={this.handleInput('lowPrice')}
                      onBlur={this.handleTwoDecimal('lowPrice')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="High Price"
                      type="number"
                      name="highPrice"
                      id=""
                      value={this.state.highPrice}
                      onChange={this.handleInput('highPrice')}
                      onBlur={this.handleTwoDecimal('highPrice')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      select
                      variant="outlined"
                      label="Condition"
                      type="text"
                      name="condition"
                      id=""
                      value={this.state.condition}
                      onChange={this.handleInput('condition')}
                    >
                      <MenuItem key="0" value="New">
                        New
                      </MenuItem>
                      <MenuItem key="1" value="Used">
                        Used
                      </MenuItem>
                      <MenuItem key="2" value="Refurbished">
                        Refurbished
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default AggregateOffer
