import React from 'react'
import { Button, Row, Col } from 'antd'
import { Helmet } from 'react-helmet'
// import PaymentCard from 'components/CleanUIComponents/PaymentCard'
// import PaymentAccount from 'components/CleanUIComponents/PaymentAccount'
// import PaymentTransaction from 'components/CleanUIComponents/PaymentTransaction'
// import ChartCard from 'components/CleanUIComponents/ChartCard'
// import Authorize from 'components/LayoutComponents/Authorize/index.jsx'
// import { tableData } from './data.json'
import { Link } from 'react-router-dom'

class MicrodataIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = { helmetTitle: '', helmetDescription: 'Generate code for bootstrap', siteName: 'OneGenerate',
      microdataItems: [
        { id: 0, name: 'Aggregate Offer', path: '/microdata/aggregate-offer' },
        { id: 1, name: 'Aggregate Rating', path: '/microdata/aggregate-rating' },
        {
          id: 2,
          name: 'Article',
          path: '/microdata/article',
        },
        {
          id: 3,
          name: 'Event',
          path: '/microdata/event',
        },
        {
          id: 4,
          name: 'Music Album',
          path: '/microdata/music-album',
        },
        {
          id: 5,
          name: 'Music Playlist',
          path: '/microdata/music-playlist',
        },
        {
          id: 6,
          name: 'Organization',
          path: '/microdata/organization',
        },
        {
          id: 7,
          name: 'Person',
          path: '/microdata/person',
        },
        {
          id: 8,
          name: 'Product Offer',
          path: '/microdata/product-offer',
        },
        {
          id: 9,
          name: 'Recipe',
          path: '/microdata/recipe',
        },
        {
          id: 10,
          name: 'Restaurant',
          path: '/microdata/restaurant',
        },
        {
          id: 11,
          name: 'Review',
          path: '/microdata/review',
        },
        {
          id: 12,
          name: 'Software Application',
          path: '/microdata/software-application',
        },
        {
          id: 13,
          name: 'Video',
          path: '/microdata/video',
        },
      ],
    }
  }

  linkTo = path => event => {
    window.location.href = path
  }

  render() {
    return (
      <div>
        <Helmet title="Microdata" />
        <Row gutter={16}>
          {this.state.microdataItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{
                    fontSize: 20,
                    marginTop: 20,
                    whiteSpace: 'unset',
                    height: 100,
                  }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
      </div>
    )
  }
}

export default MicrodataIndex
