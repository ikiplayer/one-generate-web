import React from 'react'
import { Button, Row, Col } from 'antd'
import { Helmet } from 'react-helmet'
// import PaymentCard from 'components/CleanUIComponents/PaymentCard'
// import PaymentAccount from 'components/CleanUIComponents/PaymentAccount'
// import PaymentTransaction from 'components/CleanUIComponents/PaymentTransaction'
// import ChartCard from 'components/CleanUIComponents/ChartCard'
// import Authorize from 'components/LayoutComponents/Authorize/index.jsx'
// import { tableData } from './data.json'
import { Link } from 'react-router-dom'

class HTMLIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'HTML Components',
      helmetDescription: 'Generate code for HTML',
      siteName: 'OneGenerate',
      HTMLMediaItems: [
        { id: 0, name: 'Media', path: '/html/media' },
        { id: 1, name: 'Image', path: '/html/image' },
        { id: 2, name: 'Video', path: '/html/video' },
      ],
      HTMLTextItems: [
        { id: 0, name: 'Bi-Directional Override', path: '/html/audio' },
        { id: 1, name: 'Bold', path: '/html/bold' },
        { id: 2, name: 'Cite', path: '/html/cite' },
        { id: 3, name: 'Code', path: '/html/audio' },
        { id: 4, name: 'Strikethrough', path: '/html/audio' },
        { id: 5, name: 'Italic', path: '/html/audio' },
        { id: 6, name: 'Highlight', path: '/html/audio' },
        { id: 7, name: 'Blockquote', path: '/html/audio' },
        { id: 8, name: 'Subscript & Superscript', path: '/html/audio' },
        { id: 9, name: 'Underline', path: '/html/audio' },
      ],
      HTMLOtherItems: [
        { id: 0, name: 'Detail', path: '/html/details' },
        { id: 1, name: 'Dialog', path: '/html/dialog' },
        { id: 2, name: 'Hyperlink', path: '/html/hyperlink' },
        { id: 3, name: 'IFrame', path: '/html/iframe' },
        { id: 4, name: 'Meter', path: '/html/meter' },
        { id: 5, name: 'Progress Bar', path: '/html/progress-bar' },
        { id: 6, name: 'Base link', path: '/html/base-link' },
      ],
      HTMLInputItems: [
        {
          id: 2,
          name: 'Button',
          path: '/html/button',
        },
        {
          id: 3,
          name: 'Checkbox Radio Button',
          path: '/html/checkbox-radio-button',
        },
        {
          id: 4,
          name: 'Color',
          path: '/html/color',
        },
        {
          id: 5,
          name: 'Date Time',
          path: '/html/date-time',
        },
        {
          id: 6,
          name: 'Details',
          path: '/html/details',
        },
        {
          id: 7,
          name: 'Dialog',
          path: '/html/dialog',
        },
        {
          id: 8,
          name: 'Email',
          path: '/html/email',
        },
        {
          id: 9,
          name: 'File Browser',
          path: '/html/file-browser',
        },
        {
          id: 10,
          name: 'Hyperlink',
          path: '/html/hyperlink',
        },
        {
          id: 11,
          name: 'IFrame',
          path: '/html/iframe',
        },
        {
          id: 12,
          name: 'Image',
          path: '/html/image',
        },
        {
          id: 13,
          name: 'Meter',
          path: '/html/meter',
        },
        {
          id: 14,
          name: 'Number Range Slider',
          path: '/html/number-range-slider',
        },
        {
          id: 15,
          name: 'Password',
          path: '/html/password',
        },
        {
          id: 16,
          name: 'Progress Bar',
          path: '/html/progress-bar',
        },
        {
          id: 17,
          name: 'Search',
          path: '/html/search',
        },
        {
          id: 18,
          name: 'Submit',
          path: '/html/submit',
        },
        {
          id: 19,
          name: 'Telephone',
          path: '/html/telephone',
        },
        {
          id: 20,
          name: 'Text',
          path: '/html/text',
        },
        {
          id: 21,
          name: 'Text Area',
          path: '/html/text-area',
        },
        {
          id: 22,
          name: 'Url',
          path: '/html/url',
        },
      ],
    }
  }

  linkTo = path => event => {
    window.location.href = path
  }

  render() {
    return (
      <div>
        <Helmet title="html" />
        <Row>
          <h1>Input</h1>
        </Row>
        <Row gutter={16}>
          {this.state.HTMLInputItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{ fontSize: 20, marginTop: 20, whiteSpace: 'unset', height: 100 }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
        <Row style={{ paddingTop: '50px' }}>
          <h1>Media</h1>
        </Row>
        <Row gutter={16}>
          {this.state.HTMLMediaItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{ fontSize: 20, marginTop: 20, whiteSpace: 'unset', height: 100 }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
        <Row style={{ paddingTop: '50px' }}>
          <h1>Text</h1>
        </Row>
        <Row gutter={16}>
          {this.state.HTMLTextItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{ fontSize: 20, marginTop: 20, whiteSpace: 'unset', height: 100 }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
        <Row style={{ paddingTop: '50px' }}>
          <h1>Other Elements</h1>
        </Row>
        <Row gutter={16}>
          {this.state.HTMLOtherItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{ fontSize: 20, marginTop: 20, whiteSpace: 'unset', height: 100 }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
      </div>
    )
  }
}

export default HTMLIndex
