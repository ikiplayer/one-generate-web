import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Audio extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'HTML Audio',
      helmetDescription: 'Generate code for HTML Audio',
      siteName: 'OneGenerate',
      codeItem: '',
      sourceOgg: 'media/sound.ogg',
      sourcemp3: 'media/sound.mp3',
      sourceWav: 'media/sound.wav',
      controls: 'No',
      autoplay: 'No',
      loop: 'No',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let sourceOgg = this.state.sourceOgg
    let sourcemp3 = this.state.sourcemp3
    let sourceWav = this.state.sourceWav
    let controls = this.state.controls
    let autoplay = this.state.autoplay
    let loop = this.state.loop

    let controlString = ''
    if (controls === 'Yes') {
      controlString = 'controls '
    }

    let autoplayString = ''
    if (autoplay === 'Yes') {
      autoplayString = 'autoplay '
    }

    let loopString = ''
    if (loop === 'Yes') {
      loopString = 'loop'
    }

    let codeString = `
    <audio ${controlString}${autoplayString}${loopString}>
        <source src="${sourceOgg}" type="audio/ogg">
        <source src="${sourcemp3}" type="audio/mpeg">
        <source src="${sourceWav}" type="audio/wav">
    </audio>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'HTML_Audio.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Source (OGG)"
                      type="text"
                      name="sourceOgg"
                      id=""
                      value={this.state.sourceOgg}
                      onChange={this.handleInput('sourceOgg')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Source (mp3)"
                      type="text"
                      name="sourcemp3"
                      id=""
                      value={this.state.sourcemp3}
                      onChange={this.handleInput('sourcemp3')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Source (WAV)"
                      type="text"
                      name="sourceWav"
                      id=""
                      value={this.state.sourceWav}
                      onChange={this.handleInput('sourceWav')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Controls"
                      type="text"
                      name="controls"
                      id=""
                      value={this.state.controls}
                      onChange={this.handleInput('controls')}
                      select
                    >
                      <MenuItem key="0" value="Yes">
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value="No">
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Autoplay"
                      type="text"
                      name="autoplay"
                      id=""
                      value={this.state.autoplay}
                      onChange={this.handleInput('autoplay')}
                      select
                    >
                      <MenuItem key="0" value="Yes">
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value="No">
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Loop"
                      type="text"
                      name="loop"
                      id=""
                      value={this.state.loop}
                      onChange={this.handleInput('loop')}
                      select
                    >
                      <MenuItem key="0" value="Yes">
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value="No">
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Audio
