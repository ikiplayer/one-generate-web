import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Text extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'HTML Text',
      helmetDescription: 'Generate code for html text',
      siteName: 'OneGenerate',
      name: 'textInput',
      placeholder: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let name = this.state.name
    let placeholder = this.state.placeholder

    let placeholderString = ''
    if (placeholder) {
      placeholderString = `placeholder="${placeholder}"`
    }

    let codeString = `<input type="text" name="${name}" ${placeholderString}>`

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'HTML_Text.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Name"
                      type="text"
                      name="name"
                      id=""
                      value={this.state.name}
                      onChange={this.handleInput('name')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Placeholder"
                      type="text"
                      name="placeholder"
                      id=""
                      value={this.state.placeholder}
                      onChange={this.handleInput('placeholder')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Text
