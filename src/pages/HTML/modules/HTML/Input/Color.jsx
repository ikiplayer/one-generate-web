import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import { SketchPicker } from 'react-color'
import ReactHtmlParser from 'react-html-parser'

class Color extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'HTML Color',
      helmetDescription: 'Generate code for HTML Color',
      siteName: 'OneGenerate',
      name: 'colorInput',
      placeholder: '',
      value: {
        r: 0,
        g: 0,
        b: 0,
      },
      showTextColorPicker: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let name = this.state.name
    let placeholder = this.state.placeholder
    let value = this.state.value

    let placeholderString = ''
    if (placeholder) {
      placeholderString = `placeholder="${placeholder}"`
    }

    let hex = this.RGBToHex(value.r, value.g, value.b)

    let valueString = ''
    if (value) {
      valueString = `value="${hex}"`
    }

    let codeString = `<input type="color" name="${name}" ${valueString} ${placeholderString}>`

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'HTML_Password.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  RGBToHex(r, g, b) {
    r = r.toString(16)
    g = g.toString(16)
    b = b.toString(16)

    if (r.length === 1) r = '0' + r
    if (g.length === 1) g = '0' + g
    if (b.length === 1) b = '0' + b

    return '#' + r + g + b
  }

  handleColorChangeComplete = stateItem => event => {
    // console.log(event)
    // event.persist()
    this.setState({ [stateItem]: event.rgb }, () => {
      this.updateCodeString()
    })
  }

  handleColorClick = stateItem => event => {
    console.log('handle color click', stateItem)
    // let stateToggle = this.state[stateItem]
    this.setState({ [stateItem]: !this.state[stateItem] })
  }

  handleColorClose = stateItem => event => {
    event.persist()
    this.setState({ [stateItem]: false })
  }

  ColorPicker(stateHasPicker, stateColor) {
    return (
      <div>
        <div
          style={{
            padding: '5px',
            background: '#fff',
            borderRadius: '3px',
            boxShadow: '0 0 0 1px rgba(0,0,0,.27)',
            display: 'inline-block',
            cursor: 'pointer',
            marginTop: '3.2px',
          }}
          onClick={this.handleColorClick(stateHasPicker)}
        >
          <div
            style={{
              width: '50px',
              minHeight: '2.9em',
              borderRadius: '3px',
              background: `rgba(${this.state[stateColor].r}, ${this.state[stateColor].g}, ${
                this.state[stateColor].b
              }, ${this.state[stateColor].a})`,
            }}
          />
        </div>
        {this.state[stateHasPicker] ? (
          <div
            style={{
              position: 'absolute',
              zIndex: '2',
            }}
          >
            <div
              style={{
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
              }}
              onClick={this.handleColorClose(stateHasPicker)}
            />
            <SketchPicker
              color={this.state[stateColor]}
              onChange={this.handleColorChangeComplete(stateColor)}
            />
          </div>
        ) : null}
      </div>
    )
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Name"
                      type="text"
                      name="name"
                      id=""
                      value={this.state.name}
                      onChange={this.handleInput('name')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={3}>
                  <Form>{this.ColorPicker('showTextColorPicker', 'value')}</Form>
                </Col>
                <Col md={21}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Placeholder"
                      type="text"
                      name="placeholder"
                      id=""
                      value={this.state.placeholder}
                      onChange={this.handleInput('placeholder')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4" />
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Color
