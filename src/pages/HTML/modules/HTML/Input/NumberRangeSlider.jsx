import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class NumberRangeSlider extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: '',
      helmetDescription: 'Generate code for bootstrap',
      siteName: 'OneGenerate',
      type: 'Number',
      name: '',
      defaultValue: 0,
      minimumValue: 0,
      maximumValue: 100,
      step: 1,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let type = this.state.type
    let name = this.state.name
    let defaultValue = this.state.defaultValue
    let minimumValue = this.state.minimumValue
    let maximumValue = this.state.maximumValue
    let step = this.state.step

    let nameString = ''
    let defaultValueString = ''
    let minimumValueString = ''
    let maximumValueString = ''
    let stepString = ''

    let typeString = ''
    if (type === 'Number') {
      typeString = `number`
    } else {
      typeString = `range`
    }

    if (name) {
      nameString = `name="${name}" `
    }

    if (defaultValue) {
      defaultValueString = `value="${defaultValue}" `
    }

    if (minimumValue) {
      minimumValueString = `min="${minimumValue}" `
    }

    if (maximumValue) {
      maximumValueString = `max="${maximumValue}" `
    }

    if (step) {
      stepString = `step="${step}" `
    }

    let codeString = `<input type="${typeString}"  ${nameString}${defaultValueString}${minimumValueString}${maximumValueString}${stepString}>`

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'HTML_NumberRangeSlider.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      select
                      label="Type"
                      type="text"
                      name="type"
                      id=""
                      value={this.state.type}
                      onChange={this.handleInput('type')}
                    >
                      <MenuItem key="0" value="Number">
                        Number
                      </MenuItem>
                      <MenuItem key="1" value="Range">
                        Range
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Name"
                      type="text"
                      name="name"
                      id=""
                      value={this.state.name}
                      onChange={this.handleInput('name')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Default Value"
                      type="number"
                      name="defaultValue"
                      id=""
                      value={this.state.defaultValue}
                      onChange={this.handleInput('defaultValue')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Step"
                      type="number"
                      name="step"
                      id=""
                      value={this.state.step}
                      onChange={this.handleInput('step')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Minimum Value"
                      type="number"
                      name="minimumValue"
                      id=""
                      value={this.state.minimumValue}
                      onChange={this.handleInput('minimumValue')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Maximum Value"
                      type="number"
                      name="maximumValue"
                      id=""
                      value={this.state.maximumValue}
                      onChange={this.handleInput('maximumValue')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default NumberRangeSlider
