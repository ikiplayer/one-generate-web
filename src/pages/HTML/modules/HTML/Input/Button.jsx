import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Switch, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import { SketchPicker } from 'react-color'
import { cssClass } from 'brace/theme/vibrant_ink'
import { Radio, Slider } from 'antd'

const pxSlider = {
  0: '0px',
  20: '20px',
  40: '40px',
  60: '60px',
  80: '80px',
  100: '100px',
}

const numberSlider = {
  0: '0',
  20: '20',
  40: '40',
  60: '60',
  80: '80',
  100: '100',
}

const paddingSlider = {
  0: '0%',
  20: '20%',
  40: '40%',
  60: '60%',
  80: '80%',
  100: '100%',
}

class HTMLButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'HTML Button',
      helmetDescription: 'Generate code for HTML button',
      siteName: 'OneGenerate',
      hasBasicFontSize: true,
      hasBasicFontWeight: true,
      elementCodeItem: '',
      cssCodeItem: '',
      text: '',
      className: '',
      font: '',
      showTextColorPicker: false,
      textColor: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },
      buttonSizeType: '',
      paddingBasePadding: '',
      customDimensionWidth: '',
      customDimensionHeight: '',
      enableFontSize: '',
      fontSize: '',
      fontWeight: '',
      padding: '',
      enableTextShadow: '',
      showTextShadowColorPicker: false,
      textShadowColor: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },
      textShadowHorizontal: '',
      textShadowVertical: '',
      textShadowBlur: '',
      enableBoxShadow: '',
      shadowType: '',
      showBoxShadowColor: false,
      boxShadowColor: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },

      boxShadowHorizontal: '',
      boxShadowVertical: '',
      boxShadowBlur: '',
      boxShadowSpread: '',
      enableBorder: '',
      borderType: '',
      showBorderColor: false,
      borderColor: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },

      borderWidth: '',
      borderRadius: '',
      backgroundType: '',
      showBackgroundGradientStartPicker: false,
      showBackgroundGradientEndPicker: false,
      backgroundGradientStart: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },
      backgroundGradientEnd: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },

      backgroundColor: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },

      enableHover: '',
      hoverBackgroundType: '',
      showHoverGradientStartPicker: false,
      showHoverGradientEndPicker: false,
      showHoverBackgroundColorPicker: false,
      hoverGradientStart: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },

      hoverGradientEnd: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },

      hoverBackgroundColor: {
        r: '241',
        g: '112',
        b: '19',
        a: '1',
      },
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let hasBasicFontSize = this.state.hasBasicFontSize
    let hasBasicFontWeight = this.state.hasBasicFontWeight
    // BASIC
    let basicText = this.state.text
    let basicClassName = this.state.className
    let basicFont = this.state.font
    let basicTextColor = this.state.textColor
    let basicButtonSizeType = this.state.buttonSizeType
    let basicCustomDimensionWidth = this.state.customDimensionWidth
    let basicCustomDimensionHeight = this.state.customDimensionHeight
    let basicFontSize = this.state.fontSize
    let basicFontWeight = this.state.fontWeight
    let basicPadding = this.state.padding
    // TEXT SHADOW
    let enableTextShadow = this.state.enableTextShadow
    let textShadowColor = this.state.textShadowColor
    let textShadowHorizontal = this.state.textShadowHorizontal
    let textShadowVertical = this.state.textShadowVertical
    let textShadowBlur = this.state.textShadowBlur
    // BOX SHADOW
    let enableBoxShadow = this.state.enableBoxShadow
    let shadowType = this.state.shadowType
    let boxShadowColor = this.state.boxShadowColor
    let boxShadowHorizontal = this.state.boxShadowHorizontal
    let boxShadowVertical = this.state.boxShadowVertical
    let boxShadowBlur = this.state.boxShadowBlur
    let boxShadowSpread = this.state.boxShadowSpread
    // BORDER
    let enableBorder = this.state.enableBorder
    let borderType = this.state.borderType
    let borderColor = this.state.borderColor
    let borderWidth = this.state.borderWidth
    let borderRadius = this.state.borderRadius

    // BACKGROUND
    let backgroundType = this.state.backgroundType
    let backgroundGradientStart = this.state.backgroundGradientStart
    let backgroundGradientEnd = this.state.backgroundGradientEnd
    let backgroundColor = this.state.backgroundColor
    // HOVER
    let enableHover = this.state.enableHover
    let hoverBackgroundType = this.state.hoverBackgroundType
    let hoverGradientStart = this.state.hoverGradientStart
    let hoverGradientEnd = this.state.hoverGradientEnd
    let hoverBackgroundColor = this.state.hoverBackgroundColor

    let basicSizeTypeCustomDimensionString = `
    height: ${basicCustomDimensionHeight},
    width: ${basicCustomDimensionWidth}
    `

    let textShadowString = `
    text-shadow: ${textShadowHorizontal}px ${textShadowVertical}px ${textShadowBlur}px ${textShadowColor}
    `

    let boxShadowString = `
    box-shadow: ${
      shadowType ? '' : 'inset'
    } ${boxShadowHorizontal}px ${boxShadowVertical}px ${boxShadowBlur}px ${boxShadowSpread}px ${boxShadowColor};
    -webkit-box-shadow: ${
      shadowType ? '' : 'inset'
    } ${boxShadowHorizontal}px ${boxShadowVertical}px ${boxShadowBlur}px ${boxShadowSpread}px ${boxShadowColor};
    -moz-box-shadow: ${
      shadowType ? '' : 'inset'
    } ${boxShadowHorizontal}px ${boxShadowVertical}px ${boxShadowBlur}px ${boxShadowSpread}px ${boxShadowColor};
    `

    let borderSettingsString = `
    border: ${borderType} ${borderColor} ${borderWidth} ${borderRadius}
    `

    let backgroundGradientString = `
    background: ${backgroundGradientStart};
    background-image: -webkit-linear-gradient(top, ${backgroundGradientStart}, ${backgroundGradientEnd});
    background-image: -moz-linear-gradient(top, ${backgroundGradientStart}, ${backgroundGradientEnd});
    background-image: -ms-linear-gradient(top, ${backgroundGradientStart}, ${backgroundGradientEnd});
    background-image: -o-linear-gradient(top, ${backgroundGradientStart}, ${backgroundGradientEnd});
    background-image: linear-gradient(to bottom, ${backgroundGradientStart}, ${backgroundGradientEnd});
    `

    let backgroundSolidColorString = `
    background-color: ${backgroundColor}
    `

    let backgroundString = `
    ${backgroundType === 'Gradient' ? backgroundGradientString : backgroundSolidColorString}
    `

    let hoverGradientString = `
    background: ${hoverGradientStart};
    background-image: -webkit-linear-gradient(top, ${hoverGradientStart}, ${hoverGradientEnd});
    background-image: -moz-linear-gradient(top, ${hoverGradientStart}, ${hoverGradientEnd});
    background-image: -ms-linear-gradient(top, ${hoverGradientStart}, ${hoverGradientEnd});
    background-image: -o-linear-gradient(top, ${hoverGradientStart}, ${hoverGradientEnd});
    background-image: linear-gradient(to bottom, ${hoverGradientStart}, ${hoverGradientEnd});
    text-decoration: none;
    `

    let hoverSolidColorString = `
    background: ${hoverBackgroundColor};
    text-decoration: none;
    `

    let hoverString = `
    ${hoverBackgroundType === 'Gradient' ? hoverGradientString : hoverSolidColorString}
    `

    let elementCodeString = `
    <a href="#" class="${basicClassName}">${basicText}</a>
    `

    let cssCodeString = `
    .${cssClass} {
    -webkit-border-radius: ${borderRadius}px;
    -moz-border-radius: ${borderRadius}px;
    border-radius: ${borderRadius}px;
    color: ${basicTextColor};
    font-family: ${basicFont};
    ${hasBasicFontSize ? `font-size: ${basicFontSize}px` : ``};
    ${hasBasicFontWeight ? `font-weight: ${basicFontWeight}px` : ``}
    padding: ${basicPadding}px;
    ${basicButtonSizeType === 'Custom Dimensions' ? basicSizeTypeCustomDimensionString : ''}
    text-decoration: none;
    display: inline-block;
    cursor: pointer;
    ${backgroundString}
    ${enableTextShadow ? textShadowString : ''}
    ${enableBoxShadow ? boxShadowString : ''}
    ${enableBorder ? borderSettingsString : ''}
    ${enableHover ? hoverString : ''}
    }
    `

    this.setState({
      elementCodeItem: elementCodeString,
    })

    this.setState({
      cssCodeItem: cssCodeString,
    })
  }

  handleColorClick = stateItem => event => {
    console.log('handle color click', stateItem)
    // let stateToggle = this.state[stateItem]
    this.setState({ [stateItem]: !this.state[stateItem] })
  }

  handleColorClose = stateItem => event => {
    event.persist()
    this.setState({ [stateItem]: false })
  }

  handleColorChangeComplete = stateItem => event => {
    // console.log(event)
    // event.persist()
    this.setState({ [stateItem]: event.rgb })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  ColorPicker(stateHasPicker, stateColor) {
    return (
      <div>
        <div
          style={{
            padding: '5px',
            background: '#fff',
            borderRadius: '3px',
            boxShadow: '0 0 0 1px rgba(0,0,0,.27)',
            display: 'inline-block',
            cursor: 'pointer',
            marginTop: '3.2px',
          }}
          onClick={this.handleColorClick(stateHasPicker)}
        >
          <div
            style={{
              width: '50px',
              minHeight: '2.9em',
              borderRadius: '3px',
              background: `rgba(${this.state[stateColor].r}, ${this.state[stateColor].g}, ${
                this.state[stateColor].b
              }, ${this.state[stateColor].a})`,
            }}
          />
        </div>
        {this.state[stateHasPicker] ? (
          <div
            style={{
              position: 'absolute',
              zIndex: '2',
            }}
          >
            <div
              style={{
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
              }}
              onClick={this.handleColorClose(stateHasPicker)}
            />
            <SketchPicker
              color={this.state[stateColor]}
              onChange={this.handleColorChangeComplete(stateColor)}
            />
          </div>
        ) : null}
      </div>
    )
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'CSS_Button.txt'
    element.click()
  }

  handleInput = name => event => {
    // event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text"
                      type="text"
                      name="text"
                      id=""
                      value={this.state.text}
                      onChange={this.handleInput('text')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="CSS Class Name"
                      type="text"
                      name="className"
                      id=""
                      value={this.state.className}
                      onChange={this.handleInput('className')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={20}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Font"
                      type="font"
                      name="font"
                      select
                      id=""
                      value={this.state.font}
                      onChange={this.handleInput('font')}
                    >
                      <MenuItem key="0" value="Arial">
                        Arial
                      </MenuItem>
                      <MenuItem key="1" value="Verdana">
                        Verdana
                      </MenuItem>
                      <MenuItem key="2" value="Open Sans">
                        Open Sans
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={4}>
                  <Form>{this.ColorPicker('showTextColorPicker', 'textColor')}</Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <Radio.Group
                      value={this.state.buttonSizeType}
                      onChange={this.handleInput('buttonSizeType')}
                    >
                      <Radio.Button value="large">Padding Based</Radio.Button>
                      <Radio.Button value="default">Custom Dimension</Radio.Button>
                    </Radio.Group>
                  </Form>
                </Col>
              </Row>
              {/* CUSTOM DIMENSIONS */}
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Padding"
                      type="text"
                      name="className"
                      id=""
                      value={this.state.padding}
                      onChange={this.handleInput('padding')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Slider marks={pxSlider} defaultValue={37} />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Padding"
                      type="text"
                      name="className"
                      id=""
                      value={this.state.padding}
                      onChange={this.handleInput('padding')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Slider marks={pxSlider} defaultValue={37} />
                  </Form>
                </Col>
              </Row>
              {/* PADDING AND CUSTOM DIMENSIONS */}
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Switch
                      checked={this.state.enableFontSize}
                      onChange={this.handleCheckbox('enableFontSize')}
                      value=""
                    />
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Font Size"
                      type="text"
                      name="className"
                      id=""
                      value={this.state.fontSize}
                      onChange={this.handleInput('fontSize')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Slider marks={pxSlider} defaultValue={37} />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Switch
                      checked={this.state.enableFontSize}
                      onChange={this.handleCheckbox('enableFontSize')}
                      value=""
                    />
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Font Weight"
                      type="text"
                      name="className"
                      id=""
                      value={this.state.fontWeight}
                      onChange={this.handleInput('fontWeight')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Slider marks={numberSlider} defaultValue={37} />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Padding"
                      type="text"
                      name="className"
                      id=""
                      value={this.state.padding}
                      onChange={this.handleInput('padding')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Slider marks={paddingSlider} defaultValue={37} />
                  </Form>
                </Col>
              </Row>
            </Card>
            {/* TEXT SHADOW */}
            {/* ENABLE TEXT SHADOW */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* TEXT SHADOW COLOR */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* HORIZONTAL */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* VERTICAL */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* BLUR */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* **BOX SHADOW** */}
            {/* ENABLE BOX SHADOW */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* SHADOW TYPE */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* BOX SHADOW COLOR */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* HORIZONTAL */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* VERTICAL */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* BLUR */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* SPREAD */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* **BORDER SETTINGS**  */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* ENABLE BORDER */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* BORDER TYPE */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* BORDER COLOR */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* BORDER WIDTH */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* BORDER HEIGHT */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* **BACKGROUND** */}
            {/* BACKGROUND TYPE */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* GRADIENT START */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* GRADIENT END */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* SOLID COLOR */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* **HOVER** */}
            {/* ENABLE HOVER */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* HOVER BACKGROUND TYPE */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* GRADIENT START */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* GRADIENT END */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
            {/* SOLID COLOR */}
            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>

            <Row gutter={8} className="m-4">
              <Col md={24}>
                <Form>
                  <i />
                </Form>
              </Col>
            </Row>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default HTMLButton
