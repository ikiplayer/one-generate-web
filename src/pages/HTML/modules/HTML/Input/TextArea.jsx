import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class TextArea extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'HTML Text Area',
      helmetDescription: 'Generate code for HTML text area',
      siteName: 'OneGenerate',
      codeItem: '',
      name: 'textAreaInput',
      placeholder: '',
      autofocus: '',
      columns: 30,
      rows: 15,
      maxCharacters: '90',
      required: 'No',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let name = this.state.name
    let placeholder = this.state.placeholder
    let autofocus = this.state.autofocus
    let columns = this.state.columns
    let rows = this.state.rows
    let maxCharacters = this.state.maxCharacters
    let required = this.state.required

    let maxCharactersString = ''
    if (maxCharacters) {
      maxCharactersString = `maxlength="${maxCharacters}" `
    }

    let rowsString = ''
    if (rows) {
      rowsString = `rows="${rows}" `
    }

    let columnsString = ''
    if (columns) {
      columnsString = `columns="${columns}" `
    }

    let autofocusString = ''
    if (autofocus) {
      autofocusString = `autofocus`
    }

    let placeholderString = ''
    if (placeholder) {
      placeholderString = `placeholder="${placeholder}" `
    }

    let requiredString = ''
    if (required === 'Required') {
      requiredString = `placeholder="${required}" `
    }

    let codeString = `
    <textarea
      name="${name}"${placeholderString}${rowsString}${columnsString}${maxCharactersString}${autofocusString}${requiredString}>
    </textarea>`

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'HTML_Text.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Name"
                      type="text"
                      name="name"
                      id=""
                      value={this.state.name}
                      onChange={this.handleInput('name')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Placeholder"
                      type="text"
                      name="placeholder"
                      id=""
                      value={this.state.placeholder}
                      onChange={this.handleInput('placeholder')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4" />
              <Row gutter={8} className="m-4">
                <Col md={6}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Autofocus"
                      type="text"
                      name="autofocus"
                      id=""
                      value={this.state.autofocus}
                      onChange={this.handleInput('autofocus')}
                      select
                    >
                      <MenuItem key="0" value="Yes">
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value="No">
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={6}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Columns"
                      type="text"
                      name="columns"
                      id=""
                      value={this.state.columns}
                      onChange={this.handleInput('columns')}
                    />
                  </Form>
                </Col>
                <Col md={6}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Rows"
                      type="text"
                      name="rows"
                      id=""
                      value={this.state.rows}
                      onChange={this.handleInput('rows')}
                    />
                  </Form>
                </Col>
                <Col md={6}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Max Characters"
                      type="text"
                      name="maxCharacters"
                      id=""
                      value={this.state.maxCharacters}
                      onChange={this.handleInput('maxCharacters')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default TextArea
