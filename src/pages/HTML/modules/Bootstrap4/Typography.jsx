import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Typography extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Typography',
      helmetDescription: 'Generate code for bootstrap Typography',
      siteName: 'OneGenerate',
      text: 'Typography Header',
      bodyText: 'Typography Message',
      elementType: 'Header',
      headerType: 'H1',
      displayType: 'Display 1',
      abbreviationTitle: 'Abbreviation Title',
      blockquoteFooter: 'Blockquote footer',
      descriptionDetail: 'DD Detail',
      typographyClasses: [
        { id: 0, class: 'font-weight-bold', description: '' },
        { id: 0, class: 'font-weight-bolder', description: '' },
        { id: 0, class: 'font-italic', description: '' },
        { id: 0, class: 'font-weight-light', description: '' },
        { id: 0, class: 'font-weight-lighter', description: '' },
        { id: 0, class: 'font-weight-normal', description: '' },
        { id: 0, class: 'lead', description: '' },
        { id: 0, class: 'small', description: '' },
        { id: 0, class: 'text-left', description: '' },
        { id: 0, class: 'text-break', description: '' },
        { id: 0, class: 'text-center', description: '' },
        { id: 0, class: 'text-decoration-none', description: '' },
        { id: 0, class: 'text-right', description: '' },
        { id: 0, class: 'text-justify', description: '' },
        { id: 0, class: 'text-monospace', description: '' },
        { id: 0, class: 'text-nowrap', description: '' },
        { id: 0, class: 'text-lowercase', description: '' },
        { id: 0, class: 'text-reset', description: '' },
        { id: 0, class: 'text-uppercase', description: '' },
        { id: 0, class: 'list-unstyled', description: '' },
        { id: 0, class: 'list-inline', description: '' },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let text = this.state.text
    let elementType = this.state.elementType
    let headerType = this.state.headerType
    let displayType = this.state.displayType
    let abbreviationTitle = this.state.abbreviationTitle
    let blockquoteFooter = this.state.blockquoteFooter
    let descriptionDetail = this.state.descriptionDetail

    let codeString = ``

    let elementTypeString = ``
    if (elementType === 'Header') {
      if (headerType === 'H1') {
        codeString = `<h1>${text}</h1>`
      } else if (headerType === 'H2') {
        codeString = `<h2>${text}</h2>`
      } else if (headerType === 'H3') {
        codeString = `<h3>${text}</h3>`
      } else if (headerType === 'H4') {
        codeString = `<h4>${text}</h4>`
      } else if (headerType === 'H5') {
        codeString = `<h5>${text}</h5>`
      } else if (headerType === 'H6') {
        codeString = `<h6>${text}</h6>`
      }
    } else if (elementType === 'Display') {
      if (displayType === 'Display 1') {
        codeString = `<h1 class="display-1">${text}</h1>`
      } else if (displayType === 'Display 2') {
        codeString = `<h1 class="display-2">${text}</h1>`
      } else if (displayType === 'Display 3') {
        codeString = `<h1 class="display-3">${text}</h1>`
      } else if (displayType === 'Display 4') {
        codeString = `<h1 class="display-4">${text}</h1>`
      }
    } else if (elementType === 'Small') {
      codeString = `<small>${text}</small>`
    } else if (elementType === 'Mark') {
      codeString = `<mark>${text}</mark>`
    } else if (elementType === 'Abbreviation') {
      codeString = `<abbr title="${abbreviationTitle}">${text}</abbr>`
    } else if (elementType === 'Blockquote') {
      codeString = `
        <blockquote class="blockquote">
            <p>${text}</p>
            <footer class="blockquote-footer">${blockquoteFooter}</footer>
        </blockquote>`
    } else if (elementType === 'Definition List') {
      codeString = `
        <dl>
            <dt>${text}</dt>
            <dd>- ${descriptionDetail}</dd>
        </dl> 

      `
    } else if (elementType === 'Code') {
      codeString = `<code>${text}</code>`
    } else if (elementType === 'Keyboard Input') {
      codeString = `<kbd>${text}</kbd>`
    } else if (elementType === 'Pre') {
      codeString = `<pre>${text}</pre>`
    }

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap_Typography.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Element Type"
                      type="text"
                      name="elementType"
                      id=""
                      value={this.state.elementType}
                      onChange={this.handleInput('elementType')}
                    >
                      <MenuItem key="0" value="Header">
                        Header
                      </MenuItem>
                      <MenuItem key="1" value="Display">
                        Display
                      </MenuItem>
                      <MenuItem key="2" value="Small">
                        Small
                      </MenuItem>
                      <MenuItem key="3" value="Mark">
                        Mark
                      </MenuItem>
                      <MenuItem key="4" value="Abbreviation">
                        Abbreviation
                      </MenuItem>
                      <MenuItem key="4" value="Blockquote">
                        Blockquote
                      </MenuItem>
                      <MenuItem key="4" value="Definition List">
                        Definition List
                      </MenuItem>
                      <MenuItem key="4" value="Code">
                        Code
                      </MenuItem>
                      <MenuItem key="4" value="Keyboard Input">
                        Keyboard Input
                      </MenuItem>
                      <MenuItem key="4" value="Pre">
                        Pre
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Element Type"
                      type="text"
                      name="elementType"
                      id=""
                      value={this.state.elementType}
                      onChange={this.handleInput('elementType')}
                    >
                      <MenuItem key="0" value="H1">
                        H1
                      </MenuItem>
                      <MenuItem key="1" value="H3">
                        H3
                      </MenuItem>
                      <MenuItem key="2" value="H4">
                        H4
                      </MenuItem>
                      <MenuItem key="3" value="H5">
                        H5
                      </MenuItem>
                      <MenuItem key="4" value="H6">
                        H6
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Display"
                      type="text"
                      name="displayType"
                      id=""
                      value={this.state.displayType}
                      onChange={this.handleInput('displayType')}
                    >
                      <MenuItem key="0" value="Display 1">
                        Display 1
                      </MenuItem>
                      <MenuItem key="1" value="Display 2">
                        Display 2
                      </MenuItem>
                      <MenuItem key="2" value="Display 3">
                        Display 3
                      </MenuItem>
                      <MenuItem key="3" value="Display 4">
                        Display 4
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Abbreviation Title"
                      type="text"
                      name="abbreviationTitle"
                      id=""
                      value={this.state.abbreviationTitle}
                      onChange={this.handleInput('abbreviationTitle')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Blockquote Footer"
                      type="text"
                      name="blockquoteFooter"
                      id=""
                      value={this.state.blockquoteFooter}
                      onChange={this.handleInput('blockquoteFooter')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description Detail?"
                      type="text"
                      name="descriptionDetail"
                      id=""
                      value={this.state.descriptionDetail}
                      onChange={this.handleInput('descriptionDetail')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Typography
