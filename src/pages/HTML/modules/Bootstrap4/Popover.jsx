import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Popover extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Alert',
      helmetDescription: 'Generate code for bootstrap alert',
      siteName: 'OneGenerate',
      title: 'Title',
      content: 'Message',
      position: 'Default',
      trigger: 'Default',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let title = this.state.title
    let content = this.state.content
    let position = this.state.position
    let trigger = this.state.trigger

    let positionString = ''
    if (position === 'Top') {
      positionString = ` data-placement="top"`
    } else if (position === 'Right') {
      positionString = ` data-placement="right"`
    } else if (position === 'Left') {
      positionString = ` data-placement="left"`
    } else if (position === 'Down') {
      positionString = ` data-placement="down"`
    }

    let triggerString = ''
    if (trigger === 'focus') {
      triggerString = ` data-trigger="focus`
    } else if (trigger === 'hover') {
      triggerString = ` data-trigger="hover`
    }

    let codeString = `
    <a href="#" data-toggle="popover" title="${title}"${triggerString}${positionString}data-content="${content}">${title}</a>
    <script>
    $(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
    });
    </script>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Popover.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Title"
                      type="text"
                      name="title"
                      id=""
                      value={this.state.title}
                      onChange={this.handleInput('title')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Content"
                      type="text"
                      name="content"
                      id=""
                      value={this.state.content}
                      onChange={this.handleInput('content')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Position"
                      type="text"
                      name="position"
                      id=""
                      value={this.state.position}
                      onChange={this.handleInput('position')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Top">
                        Top
                      </MenuItem>
                      <MenuItem key="2" value="Right">
                        Right
                      </MenuItem>
                      <MenuItem key="3" value="Left">
                        Left
                      </MenuItem>
                      <MenuItem key="4" value="Down">
                        Down
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Trigger"
                      type="text"
                      name="trigger"
                      id=""
                      value={this.state.trigger}
                      onChange={this.handleInput('trigger')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Focus">
                        Focus
                      </MenuItem>
                      <MenuItem key="2" value="Hover">
                        Hover
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            {/* <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row> */}
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Popover
