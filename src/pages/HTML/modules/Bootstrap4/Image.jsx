import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Image extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Image',
      helmetDescription: 'Generate code for bootstrap image',
      siteName: 'OneGenerate',
      src: '',
      alt: '',
      imageType: 'rounded',
      imagePosition: 'Default',
      isResponsive: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let src = this.state.src
    let alt = this.state.alt
    let imageType = this.state.imageType
    let imagePosition = this.state.imagePosition
    let isResponsive = this.state.isResponsive

    let imageTypeString = ''
    if (imageType === 'Rounded') {
      imageTypeString = `rounded `
    } else if (imageType === 'Circle') {
      imageTypeString = `circle `
    } else if (imageType === 'Thumbnail') {
      imageTypeString = `thumbnail `
    }

    let imagePositionString = ''
    if (imagePosition === 'Left') {
      imagePositionString = `float-left `
    } else if (imagePosition === 'Right') {
      imagePositionString = `float-right `
    } else if (imagePosition === 'Center') {
      imagePositionString = `mx-auto d-block `
    }

    let isResponsiveString = ''
    if (isResponsive) {
      isResponsiveString = `img-fluid`
    }

    let codeString = `
    <img src="${src}" class="${imageTypeString}${imagePositionString}${isResponsiveString}" alt="${alt}">
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Colors.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Src"
                      type="text"
                      name="src"
                      id=""
                      value={this.state.src}
                      onChange={this.handleInput('src')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Alt"
                      type="text"
                      name="alt"
                      id=""
                      value={this.state.alt}
                      onChange={this.handleInput('alt')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Image Type"
                      type="text"
                      name="element"
                      id=""
                      value={this.state.imageType}
                      onChange={this.handleInput('imageType')}
                      select
                    >
                      <MenuItem key="0" value="Rounded">
                        Rounded
                      </MenuItem>
                      <MenuItem key="1" value="Circle">
                        Circle
                      </MenuItem>
                      <MenuItem key="2" value="Thumbnail">
                        Thumbnail
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Image Position"
                      type="text"
                      name="imagePosition"
                      id=""
                      value={this.state.imagePosition}
                      onChange={this.handleInput('imagePosition')}
                      select
                    >
                      <MenuItem key="0" value="Left">
                        Left
                      </MenuItem>
                      <MenuItem key="1" value="Right">
                        Right
                      </MenuItem>
                      <MenuItem key="2" value="Center">
                        Center
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Responsive"
                      type="text"
                      name="isResponsive"
                      id=""
                      value={this.state.isResponsive}
                      onChange={this.handleInput('isResponsive')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Image
