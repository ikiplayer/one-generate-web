import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class ListGroups extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Boostrap List Groups',
      helmetDescription: 'Generate code for bootstrap list groups',
      siteName: 'OneGenerate',
      navType: 'Menu',
      navAlignment: 'Default',
      items: [{ value: 'Title', url: '#', isActive: true }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let navType = this.state.navType
    let navAlignment = this.state.navAlignment
    let items = this.state.items

    let navTypeString = ''
    if (navType === 'Menu') {
      navTypeString = `nav`
    } else if (navType === 'Vertical') {
      navTypeString = `nav flex-column`
    } else if (navType === 'Tab') {
      navTypeString = `nav nav-tabs`
    } else if (navType === 'Pill') {
      navTypeString = `nav nav-pills`
    } else if (navType === 'Justified Pill') {
      navTypeString = `nav nav-pills nav-justified`
    }

    let navAlignmentString = ''
    if (navAlignment === 'Center') {
      navAlignmentString = ` justify-content-center`
    } else if (navAlignment === 'End') {
      navAlignmentString = ` justify-content-end`
    }

    let codeString = `
    <ul class="${navTypeString}${navAlignmentString}">${items
      .map(item => {
        let isActiveString = ''
        if (!item.isActive) {
          isActiveString = ` disabled`
        }

        return `\n\t  <li class="nav-item">
        <a class="nav-link${isActiveString}" href="${item.url}">${item.value}</a>
      </li>`
      })
      .join('')}
    </ul>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap_ListGroups.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({ value: 'Title', url: '#', isActive: true })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Nav Type"
                      type="text"
                      name="navType"
                      id=""
                      value={this.state.navType}
                      onChange={this.handleInput('navType')}
                      select
                    >
                      <MenuItem key="0" value="Menu">
                        Menu
                      </MenuItem>
                      <MenuItem key="0" value="Vertical">
                        Menu Vertical
                      </MenuItem>
                      <MenuItem key="1" value="Tab">
                        Tab
                      </MenuItem>
                      <MenuItem key="1" value="Pill">
                        Pill
                      </MenuItem>
                      <MenuItem key="1" value="Justified Pill">
                        Justified Pill
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col
                  md={12}
                  className={
                    this.state.navType === 'Menu' ||
                    this.state.navType === 'Tab' ||
                    this.state.navType === 'Pill'
                      ? 'visible'
                      : 'invisible'
                  }
                >
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Nav Alignment"
                      type="text"
                      name="navAlignment"
                      id=""
                      value={this.state.navAlignment}
                      onChange={this.handleInput('navAlignment')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Center">
                        Center
                      </MenuItem>
                      <MenuItem key="2" value="End">
                        End
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.items.map((item, index) => {
                      return (
                        <div key={index}>
                          <h6>{'Nav Item ' + (index + 1)}</h6>
                          <Row gutter={8} className="m-4">
                            <Col md={8}>
                              <Form>
                                <TextField
                                  placeholder=" "
                                  fullWidth
                                  variant="outlined"
                                  label="Active?"
                                  type="text"
                                  name="isActive"
                                  id=""
                                  value={item.isActive}
                                  onChange={this.handleItem('items', 'isActive', index)}
                                  select
                                >
                                  <MenuItem key="0" value={true}>
                                    Yes
                                  </MenuItem>
                                  <MenuItem key="1" value={false}>
                                    No
                                  </MenuItem>
                                </TextField>
                              </Form>
                            </Col>
                            <Col md={8}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Value"
                                name="value"
                                id=""
                                value={item.value}
                                onChange={this.handleItem('items', 'value', index)}
                              />
                            </Col>
                            <Col md={8}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Url"
                                name="url"
                                id=""
                                value={item.url}
                                onChange={this.handleItem('items', 'url', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('items')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div class="container">
                  <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
                </div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ListGroups
