import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class ButtonGroups extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Button Groups',
      helmetDescription: 'Generate code for bootstrap button groups',
      siteName: 'OneGenerate',
      size: 'Default',
      isVertical: false,
      items: [
        {
          style: 'Primary',
          value: 'Text',
          dropdownItems: [
            {
              value: '',
            },
          ],
        },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let size = this.state.size
    let isVertical = this.state.isVertical
    let items = this.state.items

    let sizeString = ''
    if (size === 'Small') {
      sizeString = ` btn-group-sm`
    } else if (size === 'Large') {
      sizeString = ` btn-group-lg`
    }

    let isVerticalString = ''
    if (isVertical) {
      isVerticalString = `-vertical `
    }

    // let buttonStyleString = ''
    // if (buttonStyle === 'Primary') {
    //   buttonStyleString = `btn-primary`
    // } else if (buttonStyle === 'Secondary') {
    //   buttonStyleString = `btn-secondary`
    // } else if (buttonStyle === 'Success') {
    //   buttonStyleString = `btn-success`
    // } else if (buttonStyle === 'Danger') {
    //   buttonStyleString = `btn-danger`
    // } else if (buttonStyle === 'Warning') {
    //   buttonStyleString = `btn-warning`
    // } else if (buttonStyle === 'Info') {
    //   buttonStyleString = `btn-info`
    // } else if (buttonStyle === 'Light') {
    //   buttonStyleString = `btn-light`
    // } else if (buttonStyle === 'Dark') {
    //   buttonStyleString = `btn-dark`
    // }

    // let descriptionString = "";
    // if (description) {
    //   descriptionString = `\n\t<meta property="og:description" content="${description}">`;
    // }

    let codeString = `
    <div class="btn-group${isVerticalString}${sizeString}">
      ${items
        .map(item => {
          return `<button type="button" class="btn ${this.getButtonStyleString(item.style)}">${
            item.value
          }</button>
      `
        })
        .join('')}
    
    </div>

    `

    this.setState({
      codeItem: codeString,
    })
  }

  getButtonStyleString(style) {
    let styleString = ''
    if (style === 'Primary') {
      styleString = `btn-primary`
    } else if (style === 'Secondary') {
      styleString = `btn-secondary`
    } else if (style === 'Success') {
      styleString = `btn-success`
    } else if (style === 'Danger') {
      styleString = `btn-danger`
    } else if (style === 'Warning') {
      styleString = `btn-warning`
    } else if (style === 'Info') {
      styleString = `btn-info`
    } else if (style === 'Light') {
      styleString = `btn-light`
    } else if (style === 'Dark') {
      styleString = `btn-dark`
    }

    return styleString
  }

  handleSubItem = (name, subname, index) => event => {
    event.persist()

    let updatedItems = this.state[name][index]
    updatedItems = Object.assign({}, this.state[name], {
      [subname]: event.target.value,
    })

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Skeleton.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      style: 'Primary',
      value: 'Text',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Size"
                      type="text"
                      name="size"
                      id=""
                      value={this.state.size}
                      onChange={this.handleInput('size')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Small">
                        Small
                      </MenuItem>
                      <MenuItem key="1" value="Large">
                        Large
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Vertical?"
                      type="text"
                      name="isVertical"
                      id=""
                      value={this.state.isVertical}
                      onChange={this.handleInput('isVertical')}
                      select
                    >
                      <MenuItem key="0" value={false}>
                        No
                      </MenuItem>
                      <MenuItem key="1" value={true}>
                        Yes
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.items.map((item, index) => {
                      return (
                        <div key={index}>
                          <h6>{'Button Item ' + (index + 1)}</h6>
                          <Row gutter={8} className="m-4">
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Style"
                                name="style"
                                id=""
                                value={item.style}
                                onChange={this.handleItem('items', 'style', index)}
                                select
                              >
                                <MenuItem key="0" value="Primary">
                                  Primary
                                </MenuItem>
                                <MenuItem key="1" value="Secondary">
                                  Secondary
                                </MenuItem>
                                <MenuItem key="2" value="Success">
                                  Success
                                </MenuItem>
                                <MenuItem key="3" value="Danger">
                                  Danger
                                </MenuItem>
                                <MenuItem key="4" value="Warning">
                                  Warning
                                </MenuItem>
                                <MenuItem key="5" value="Info">
                                  Info
                                </MenuItem>
                                <MenuItem key="6" value="Light">
                                  Light
                                </MenuItem>
                                <MenuItem key="7" value="Dark">
                                  Dark
                                </MenuItem>
                              </TextField>
                            </Col>
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Value"
                                name="value"
                                id=""
                                value={item.value}
                                onChange={this.handleItem('items', 'value', index)}
                              />
                            </Col>
                          </Row>
                          {/* <Row gutter={8} className="m-4">
                            {item.dropdownItems.map((dropDownItem, dropDownIndex) => {
                              return (
                                <div>
                                  <Row gutter={8} className="m-4">
                                    <Col md={12}>
                                      <h6>{'Value ' + (dropDownIndex + 1)}</h6>
                                      <TextField
                                        placeholder=" "
                                        key={index}
                                        fullWidth
                                        variant="outlined"
                                        label="Value"
                                        name="value"
                                        id=""
                                        value={dropDownItem.value}
                                        onChange={this.handleItem(
                                          'items',
                                          'dropdown',
                                          index,
                                          dropDownIndex,
                                        )}
                                      />
                                    </Col>
                                  </Row>
                                </div>
                              )
                            })}
                          </Row> */}
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('items')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ButtonGroups
