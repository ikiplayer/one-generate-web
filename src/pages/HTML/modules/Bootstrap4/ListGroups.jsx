import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class ListGroups extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Boostrap List Groups',
      helmetDescription: 'Generate code for bootstrap list groups',
      siteName: 'OneGenerate',
      isHorizontal: false,
      isLinkItems: false,
      isNoBorders: false,
      items: [{ value: 'Title', isActive: false, style: 'Primary' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let isHorizontal = this.state.isHorizontal
    let isNoBorders = this.state.isNoBorders
    let isLinkItems = this.state.isLinkItems
    let items = this.state.items

    let isLinkItemsString = ''
    if (isLinkItems) {
      isLinkItemsString = `a`
    } else {
      isLinkItemsString = `li`
    }

    let codeString = `
    <${isLinkItems ? 'div' : 'ul'} class="list-group${isNoBorders ? ' list-group-flush' : ''}${
      isHorizontal ? ' list-group-horizontal' : ''
    }">${items
      .map(item => {
        let isActiveString = ''
        if (item.isActive === 'active') {
          isActiveString = ' active'
        } else if (item.isActive === 'disabled') {
          isActiveString = ' disabled'
        } else {
          isActiveString = ''
        }

        let styleString = ''
        if (item.style === 'Primary') {
          styleString = ` list-group-item-primary`
        } else if (item.style === 'Secondary') {
          styleString = ` list-group-item-secondary`
        } else if (item.style === 'Success') {
          styleString = ` list-group-item-success`
        } else if (item.style === 'Danger') {
          styleString = ` list-group-item-danger`
        } else if (item.style === 'Warning') {
          styleString = ` list-group-item-warning`
        } else if (item.style === 'Info') {
          styleString = ` list-group-item-info`
        } else if (item.style === 'Light') {
          styleString = ` list-group-item-light`
        } else if (item.style === 'Dark') {
          styleString = ` list-group-item-dark`
        }

        return `\n\t<${isLinkItemsString} class="list-group-item${styleString}${isActiveString}">${
          item.value
        }\n\t</${isLinkItemsString}>`
      })
      .join('')}
    </${isLinkItems ? 'div' : 'ul'}>

    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap_ListGroups.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({ value: 'Title', isActive: false, style: 'Primary' })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Horizontal?"
                      type="text"
                      name="isHorizontal"
                      id=""
                      value={this.state.isHorizontal}
                      onChange={this.handleInput('isHorizontal')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>

                <Col md={8}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Link Items?"
                      type="text"
                      name="isLinkedItems"
                      id=""
                      value={this.state.isLinkItems}
                      onChange={this.handleInput('isLinkItems')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Borders?"
                      type="text"
                      name="isNoBorders"
                      id=""
                      value={this.state.isNoBorders}
                      onChange={this.handleInput('isNoBorders')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.items.map((item, index) => {
                      return (
                        <div key={index}>
                          <h6>{'List Group Item ' + (index + 1)}</h6>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <Form>
                                <TextField
                                  placeholder=" "
                                  fullWidth
                                  variant="outlined"
                                  label="Style"
                                  type="text"
                                  name="style"
                                  id=""
                                  value={item.style}
                                  onChange={this.handleItem('items', 'style', index)}
                                  select
                                >
                                  <MenuItem key="0" value="Primary">
                                    Primary
                                  </MenuItem>
                                  <MenuItem key="1" value="Secondary">
                                    Secondary
                                  </MenuItem>
                                  <MenuItem key="2" value="Success">
                                    Success
                                  </MenuItem>
                                  <MenuItem key="3" value="Danger">
                                    Danger
                                  </MenuItem>
                                  <MenuItem key="4" value="Warning">
                                    Warning
                                  </MenuItem>
                                  <MenuItem key="5" value="Info">
                                    Info
                                  </MenuItem>
                                  <MenuItem key="6" value="Light">
                                    Light
                                  </MenuItem>
                                  <MenuItem key="7" value="Dark">
                                    Dark
                                  </MenuItem>
                                </TextField>
                              </Form>
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Item Value"
                                name="value"
                                id=""
                                value={item.value}
                                onChange={this.handleItem('items', 'value', index)}
                              />
                            </Col>
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Is Active"
                                name="isActive"
                                id=""
                                value={item.isActive}
                                onChange={this.handleItem('items', 'isActive', index)}
                                select
                              >
                                <MenuItem key="0" value={true}>
                                  Yes
                                </MenuItem>
                                <MenuItem key="1" value={false}>
                                  No
                                </MenuItem>
                              </TextField>
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('items')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div class="container">
                  <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
                </div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ListGroups
