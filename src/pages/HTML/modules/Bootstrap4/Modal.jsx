import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Modal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Color',
      helmetDescription: 'Generate code for bootstrap color',
      siteName: 'OneGenerate',
      buttonLabel: 'Open Modal',
      modalHeaderLabel: 'Title',
      modalBodyLabel: 'Message',
      modalFooterButtonLabel: 'Close',
      isAnimated: false,
      modalSize: 'Default',
      isCentered: false,
      isScrolling: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let buttonLabel = this.state.buttonLabel
    let modalHeaderLabel = this.state.modalHeaderLabel
    let modalBodyLabel = this.state.modalBodyLabel
    let modalFooterButtonLabel = this.state.modalFooterButtonLabel
    let isAnimated = this.state.isAnimated
    let modalSize = this.state.modalSize
    let isCentered = this.state.isCentered
    let isScrolling = this.state.isScrolling

    let modalSizeString = ``
    if (modalSize === 'Small') {
      modalSizeString = ' modal-sm'
    } else if (modalSize === 'Large') {
      modalSizeString = ' modal-lg'
    } else if (modalSize === 'Extra Large') {
      modalSizeString = ' modal-xl'
    }

    let isCenteredString = ``
    if (isCentered) {
      isCenteredString = ` modal-dialog-centered`
    }

    let isScrollingString = ``
    if (isScrolling) {
      isScrollingString = ` modal-dialog-scrollable`
    }

    let isAnimatedString = ``
    if (isAnimated) {
      isAnimatedString = ` fade`
    }

    let codeString = `
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    ${buttonLabel}
    </button>

    <div class="modal${isAnimatedString}" id="myModal">
    <div class="modal-dialog${modalSizeString}${isCenteredString}${isScrollingString}">
        <div class="modal-content">

        <div class="modal-header">
            <h4 class="modal-title">${modalHeaderLabel}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">
            ${modalBodyLabel}
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">${modalFooterButtonLabel}</button>
        </div>

        </div>
    </div>
    </div>

    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Modal.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Button Label"
                      type="text"
                      name="buttonLabel"
                      id=""
                      value={this.state.buttonLabel}
                      onChange={this.handleInput('buttonLabel')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Header Label"
                      type="text"
                      name="modalHeaderLabel"
                      id=""
                      value={this.state.modalHeaderLabel}
                      onChange={this.handleInput('modalHeaderLabel')}
                    />
                  </Form>
                </Col>
              </Row>

              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Body Label"
                      type="text"
                      name="modalBodyLabel"
                      id=""
                      value={this.state.modalBodyLabel}
                      onChange={this.handleInput('modalBodyLabel')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Footer Button Label"
                      type="text"
                      name="modalFooterButtonLabel"
                      id=""
                      value={this.state.modalFooterButtonLabel}
                      onChange={this.handleInput('modalFooterButtonLabel')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Animated?"
                      type="text"
                      name="isAnimated"
                      id=""
                      value={this.state.isAnimated}
                      onChange={this.handleInput('isAnimated')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Modal Size"
                      type="text"
                      name="modalSize"
                      id=""
                      value={this.state.modalSize}
                      onChange={this.handleInput('modalSize')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Small">
                        Small
                      </MenuItem>
                      <MenuItem key="2" value="Large">
                        Large
                      </MenuItem>
                      <MenuItem key="3" value="Extra Large">
                        Extra Large
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Centered?"
                      type="text"
                      name="isCentered"
                      id=""
                      value={this.state.isCentered}
                      onChange={this.handleInput('isCentered')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Scrolling?"
                      type="text"
                      name="isScrolling"
                      id=""
                      value={this.state.isScrolling}
                      onChange={this.handleInput('isScrolling')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Modal
