import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Dropdown extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Dropdown',
      helmetDescription: 'Generate code for bootstrap dropdown',
      siteName: 'OneGenerate',
      style: 'Primary',
      buttonText: 'Click Me',
      position: 'Down',
      items: [
        {
          urlLink: '',
          value: 'Message',
          dropdownType: 'Default',
        },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let style = this.state.style
    let buttonText = this.state.buttonText
    let position = this.state.position
    let items = this.state.items

    let styleString = ''
    if (style === 'Primary') {
      styleString = `btn-primary`
    } else if (style === 'Secondary') {
      styleString = `btn-secondary`
    } else if (style === 'Success') {
      styleString = `btn-success`
    } else if (style === 'Danger') {
      styleString = `btn-danger`
    } else if (style === 'Warning') {
      styleString = `btn-warning`
    } else if (style === 'Info') {
      styleString = `btn-info`
    } else if (style === 'Light') {
      styleString = `btn-light`
    } else if (style === 'Dark') {
      styleString = `btn-dark`
    }

    let positionString = ''
    if (position === 'Default') {
      positionString = `dropdown`
    } else if (position === 'Left') {
      positionString = `dropdown dropright`
    } else if (position === 'Right') {
      positionString = `dropdown dropleft`
    } else if (position === 'Up') {
      positionString = `dropup`
    }

    let codeString = `
    <div class="${positionString}">
      <button type="button" class="btn ${styleString} dropdown-toggle" data-toggle="dropdown">
        ${buttonText}
      </button>
      <div class="dropdown-menu">
      ${items
        .map(item => {
          let itemString = ''
          if (item.dropdownType === 'Default') {
            itemString = `<a class="dropdown-item" href="${item.urlLink}">${item.value}</a>`
          } else if (item.dropdownType === 'Divider') {
            itemString = `<div class="dropdown-divider"></div>`
          } else if (item.dropdownType === 'Header') {
            itemString = `<div class="dropdown-header">${item.value}</div>`
          } else if (item.dropdownType === 'Active') {
            itemString = `<a class="dropdown-item active" href="${item.urlLink}">${item.value}</a>`
          } else if (item.dropdownType === 'Disabled') {
            itemString = `<a class="dropdown-item disabled" href="${item.urlLink}">${
              item.value
            }</a>`
          }
          return `${itemString}`
        })
        .join('')}
      </div>
    </div>

    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Dropdown.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      urlLink: '',
      value: `Message ${this.state.items.length + 1}`,
      dropdownType: 'Default',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Button Text"
                      type="text"
                      name="buttonText"
                      id=""
                      value={this.state.buttonText}
                      onChange={this.handleInput('buttonText')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="ButtonStyle"
                      type="text"
                      name="style"
                      id=""
                      value={this.state.style}
                      onChange={this.handleInput('style')}
                      select
                    >
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="6" value="Light">
                        Light
                      </MenuItem>
                      <MenuItem key="7" value="Dark">
                        Dark
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Position"
                      type="text"
                      name="position"
                      id=""
                      value={this.state.position}
                      onChange={this.handleInput('position')}
                      select
                    >
                      <MenuItem key="0" value="Down">
                        Down
                      </MenuItem>
                      <MenuItem key="1" value="Right">
                        Right
                      </MenuItem>
                      <MenuItem key="2" value="Left">
                        Left
                      </MenuItem>
                      <MenuItem key="3" value="Up">
                        Up
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>

              <Row gutter={8} className="m-4" />
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.items.map((item, index) => {
                      return (
                        <div key={index}>
                          <h6>{'Dropdown ' + (index + 1)}</h6>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Dropdown Type"
                                name="dropdownType"
                                id=""
                                value={item.dropdownType}
                                onChange={this.handleItem('items', 'dropdownType', index)}
                                select
                              >
                                <MenuItem key="0" value="Default">
                                  Default
                                </MenuItem>
                                <MenuItem key="1" value="Divider">
                                  Divider
                                </MenuItem>
                                <MenuItem key="2" value="Header">
                                  Header
                                </MenuItem>
                                <MenuItem key="3" value="Active">
                                  Active
                                </MenuItem>
                                <MenuItem key="4" value="Disabled">
                                  Disabled
                                </MenuItem>
                              </TextField>
                            </Col>
                          </Row>
                          <Row
                            gutter={8}
                            className="m-4"
                            style={item.dropdownType !== 'Divider' ? {} : { display: 'none' }}
                          >
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Text"
                                name="value"
                                id=""
                                value={item.value}
                                onChange={this.handleItem('items', 'value', index)}
                              />
                            </Col>
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Url link"
                                name="urlLink"
                                id=""
                                value={item.urlLink}
                                onChange={this.handleItem('items', 'urlLink', index)}
                              />
                            </Col>
                          </Row>

                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('items')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Dropdown
