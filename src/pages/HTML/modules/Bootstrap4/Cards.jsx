import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Cards extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Card',
      helmetDescription: 'Generate code for bootstrap card',
      siteName: 'OneGenerate',
      headerValue: '',
      contentValue: 'Content',
      footerValue: '',
      element: 'H4',
      cardStyle: 'Primary',
      imageUrl: '',
      linkItems: [{ urlLink: '', value: 'Link' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let headerValue = this.state.headerValue
    let contentValue = this.state.contentValue
    let footerValue = this.state.footerValue
    let cardStyle = this.state.cardStyle
    let linkItems = this.state.linkItems
    let imageUrl = this.state.imageUrl
    let element = this.state.element

    let headerValueString = ''
    if (headerValue) {
      headerValueString = `<div class="card-header">${headerValue}</div>\n`
    }

    let footerValueString = ''
    if (footerValue) {
      footerValueString = `\n <div class="card-footer">${footerValue}</div>`
    }

    let imageUrlString = ''
    if (imageUrl) {
      imageUrlString = `\n<img class="card-img-top" src="${imageUrl}" alt="Card image">`
    }

    let elementString = ''
    if (element === 'H1') {
      elementString = 'h1'
    } else if (element === 'H2') {
      elementString = 'h2'
    } else if (element === 'H3') {
      elementString = 'h3'
    } else if (element === 'H4') {
      elementString = 'h4'
    } else if (element === 'H5') {
      elementString = 'h5'
    } else if (element === 'H6') {
      elementString = 'h6'
    }

    // let imageOverlayString = ''
    // if (imageOverlay) {
    //   imageOverlayString = `\n<div class="card-img-overlay">`
    // }

    let cardStyleString = ''
    if (cardStyle === 'Primary') {
      cardStyleString = `bg-primary`
    } else if (cardStyle === 'Secondary') {
      cardStyleString = `bg-secondary`
    } else if (cardStyle === 'Success') {
      cardStyleString = `bg-success`
    } else if (cardStyle === 'Danger') {
      cardStyleString = `bg-danger`
    } else if (cardStyle === 'Warning') {
      cardStyleString = `bg-warning`
    } else if (cardStyle === 'Info') {
      cardStyleString = `bg-info`
    } else if (cardStyle === 'Light') {
      cardStyleString = `bg-light`
    } else if (cardStyle === 'Dark') {
      cardStyleString = `bg-dark`
    }

    let codeString = `
    <div class="card ${cardStyleString}">
    ${imageUrlString}${headerValueString}<div class="card-body"><${elementString} class="card-title">${contentValue}</${elementString}>${linkItems
      .map(item => {
        return `\n\t\t<a href="${item.urlLink}" class="card-link">${item.value}</a>`
      })
      .join('')}
      </div>
      ${footerValueString}  
    </div>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Boostrap4_Card.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked }, () => {
      this.updateCodeString()
    })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      urlLink: '',
      value: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Header Text"
                      type="text"
                      name="headerValue"
                      id=""
                      value={this.state.headerValue}
                      onChange={this.handleInput('headerValue')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Content Text"
                      type="text"
                      name="contentValue"
                      id=""
                      value={this.state.contentValue}
                      onChange={this.handleInput('contentValue')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Footer Text"
                      type="text"
                      name="footerValue"
                      id=""
                      value={this.state.footerValue}
                      onChange={this.handleInput('footerValue')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Header Type"
                      type="text"
                      name="element"
                      id=""
                      value={this.state.element}
                      onChange={this.handleInput('element')}
                      select
                    >
                      <MenuItem key="0" value="H1">
                        H1
                      </MenuItem>
                      <MenuItem key="1" value="H2">
                        H2
                      </MenuItem>
                      <MenuItem key="2" value="H3">
                        H3
                      </MenuItem>
                      <MenuItem key="3" value="H4">
                        H4
                      </MenuItem>
                      <MenuItem key="4" value="H5">
                        H5
                      </MenuItem>
                      <MenuItem key="5" value="H6">
                        H6
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Card Style"
                      type="text"
                      name="cardStyle"
                      id=""
                      value={this.state.cardStyle}
                      onChange={this.handleInput('cardStyle')}
                      select
                    >
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="6" value="Light">
                        Light
                      </MenuItem>
                      <MenuItem key="7" value="Dark">
                        Dark
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Image Url"
                      type="text"
                      name="imageUrl"
                      id=""
                      value={this.state.imageUrl}
                      onChange={this.handleInput('imageUrl')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.linkItems.map((item, index) => {
                      return (
                        <div key={index}>
                          <h6>{'Link Item ' + (index + 1)}</h6>
                          <Row gutter={8} className="m-4">
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Link Url"
                                name="urlLink"
                                id=""
                                value={item.urlLink}
                                onChange={this.handleItem('linkItems', 'urlLink', index)}
                              />
                            </Col>
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Value"
                                name="value"
                                id=""
                                value={item.value}
                                onChange={this.handleItem('linkItems', 'value', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('linkItems')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Cards
