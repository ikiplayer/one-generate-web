import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import ReactHtmlParser from 'react-html-parser'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class ProgressBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Boostrap Progress Bar',
      helmetDescription: 'Generate code for bootstrap progress bar',
      siteName: 'OneGenerate',
      width: '20',
      showLabel: false,
      label: 'Label',
      style: 'Default',
      hasStripes: false,
      isAnimated: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let width = this.state.width
    let showLabel = this.state.showLabel
    let label = this.state.label
    let style = this.state.style
    let hasStripes = this.state.hasStripes
    let isAnimated = this.state.isAnimated

    let showLabelString = ''
    let showLabelString2 = ''
    if (!showLabel) {
      showLabelString = `<span class="sr-only">`
      showLabelString2 = `</span>`
    }

    let styleString = ''
    if (style === 'Success') {
      styleString = ` bg-success`
    } else if (style === 'Info') {
      styleString = ` bg-info`
    } else if (style === 'Warning') {
      styleString = ` bg-warning`
    } else if (style === 'Danger') {
      styleString = ` bg-danger`
    }

    let hasStripesString = ''
    if (hasStripes) {
      hasStripesString = ` progress-bar-striped`
    }

    let isAnimatedString = ''
    if (isAnimated) {
      isAnimatedString = ` progress-bar-animated`
    }

    // let descriptionString = "";
    // if (description) {
    //   descriptionString = `\n\t<meta property="og:description" content="${description}">`;
    // }

    let codeString = `
    <div class="progress">
      <div class="progress-bar${styleString}${hasStripesString}${isAnimatedString}" role="progressbar" aria-valuenow="70"
      aria-valuemin="0" aria-valuemax="100" style="width:${width}%">
        ${showLabelString}${label}${showLabelString2}
      </div>
    </div>

    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Skeleton.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Width (1-100)"
                      type="number"
                      name="width"
                      id=""
                      value={this.state.width}
                      onChange={this.handleInput('width')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Label"
                      type="text"
                      name="label"
                      id=""
                      value={this.state.label}
                      onChange={this.handleInput('label')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Show Label?"
                      type="text"
                      name="showLabel"
                      id=""
                      value={this.state.showLabel}
                      onChange={this.handleInput('showLabel')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Style"
                      type="text"
                      name="style"
                      id=""
                      value={this.state.style}
                      onChange={this.handleInput('style')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="2" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="3" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="4" value="Danger">
                        Danger
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Has Stripes?"
                      type="text"
                      name="hasStripes"
                      id=""
                      value={this.state.hasStripes}
                      onChange={this.handleInput('hasStripes')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Is Animated?"
                      type="text"
                      name="isAnimated"
                      id=""
                      value={this.state.isAnimated}
                      onChange={this.handleInput('isAnimated')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ProgressBar
