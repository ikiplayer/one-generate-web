import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Alert extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Badges',
      helmetDescription: 'Generate code for bootstrap badges',
      siteName: 'OneGenerate',
      badgeType: 'Badge',
      badgeCount: 'New',
      label: 'Badge Text',
      buttonStyle: 'Primary',
      labelStyle: '',
      urlLink: '',
      style: 'Primary',
      element: 'H1',
      isPill: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let badgeType = this.state.badgeType
    let badgeCount = this.state.badgeCount
    let label = this.state.label
    let buttonStyle = this.state.buttonStyle
    let labelStyle = this.state.labelStyle
    let urlLink = this.state.urlLink
    let element = this.state.element
    let style = this.state.style
    let isPill = this.state.isPill

    let styleString = ''
    if (style === 'Primary') {
      styleString = `-primary`
    } else if (style === 'Secondary') {
      styleString = `-secondary`
    } else if (style === 'Success') {
      styleString = `-success`
    } else if (style === 'Danger') {
      styleString = `-danger`
    } else if (style === 'Warning') {
      styleString = `-warning`
    } else if (style === 'Info') {
      styleString = `-info`
    } else if (style === 'Light') {
      styleString = `-light`
    } else if (style === 'Dark') {
      styleString = `-dark`
    }

    let buttonStyleString = ''
    if (buttonStyle === 'Primary') {
      buttonStyleString = `btn-primary`
    } else if (buttonStyle === 'Secondary') {
      buttonStyleString = `btn-secondary`
    } else if (buttonStyle === 'Success') {
      buttonStyleString = `btn-success`
    } else if (buttonStyle === 'Danger') {
      buttonStyleString = `btn-danger`
    } else if (buttonStyle === 'Warning') {
      buttonStyleString = `btn-warning`
    } else if (buttonStyle === 'Info') {
      buttonStyleString = `btn-info`
    } else if (buttonStyle === 'Light') {
      buttonStyleString = `btn-light`
    } else if (buttonStyle === 'Dark') {
      buttonStyleString = `btn-dark`
    }

    let labelStyleString = ''
    if (labelStyle === 'Success') {
      labelStyleString = `btn-success`
    } else if (labelStyle === 'Info') {
      labelStyleString = `btn-info`
    } else if (labelStyle === 'Danger') {
      labelStyleString = `btn-danger`
    } else if (labelStyle === 'Default') {
      labelStyleString = `btn-default`
    } else if (labelStyle === 'Primary') {
      labelStyleString = `btn-primary`
    }

    let elementString = ''
    if (element === 'H1') {
      elementString = 'h1'
    } else if (element === 'H2') {
      elementString = 'h2'
    } else if (element === 'H3') {
      elementString = 'h3'
    } else if (element === 'H4') {
      elementString = 'h4'
    } else if (element === 'H5') {
      elementString = 'h5'
    } else if (element === 'H6') {
      elementString = 'h6'
    }

    let isPillString = ''
    if (isPill) {
      isPillString = ` badge-pill`
    }

    let codeString = ``

    if (badgeType === 'Badge') {
      codeString = `
        <span class="badge${isPillString} badge${styleString}">${badgeCount}</span>
        `
    } else if (badgeType === 'Button') {
      codeString = `
        <button type="button" class="btn btn${styleString}">${label}<span class="badge">${badgeCount}</span></button>
        `
    } else if (badgeType === 'Label') {
      codeString = `
        <${elementString}>${label} <span class="badge${isPillString} badge${styleString}">${badgeCount}</span></${elementString}>
        `
    }

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Badge.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Badge Type"
                      type="text"
                      name="badgeType"
                      id=""
                      value={this.state.badgeType}
                      onChange={this.handleInput('badgeType')}
                      select
                    >
                      <MenuItem key="0" value="Badge">
                        Badge
                      </MenuItem>
                      <MenuItem key="1" value="Button">
                        Button
                      </MenuItem>
                      <MenuItem key="2" value="Label">
                        Label
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Badge Text"
                      type="text"
                      name="badgeCount"
                      id=""
                      value={this.state.badgeCount}
                      onChange={this.handleInput('badgeCount')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Badge Style"
                      type="text"
                      name="style"
                      id=""
                      value={this.state.style}
                      onChange={this.handleInput('style')}
                      select
                    >
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="6" value="Light">
                        Light
                      </MenuItem>
                      <MenuItem key="7" value="Dark">
                        Dark
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col
                  md={8}
                  className={
                    this.state.badgeType === 'Label' || this.state.badgeType === 'Badge'
                      ? 'visible'
                      : 'invisible'
                  }
                >
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Pill"
                      type="text"
                      name="isPill"
                      id=""
                      value={this.state.isPill}
                      onChange={this.handleInput('isPill')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={8} className={this.state.badgeType === 'Label' ? 'visible' : 'invisible'}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Header Type"
                      type="text"
                      name="element"
                      id=""
                      value={this.state.element}
                      onChange={this.handleInput('element')}
                      select
                    >
                      <MenuItem key="0" value="H1">
                        H1
                      </MenuItem>
                      <MenuItem key="1" value="H2">
                        H2
                      </MenuItem>
                      <MenuItem key="2" value="H3">
                        H3
                      </MenuItem>
                      <MenuItem key="3" value="H4">
                        H4
                      </MenuItem>
                      <MenuItem key="4" value="H5">
                        H5
                      </MenuItem>
                      <MenuItem key="5" value="H6">
                        H6
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Alert
