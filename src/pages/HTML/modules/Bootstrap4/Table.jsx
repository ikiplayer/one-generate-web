import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class BoostrapTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap table',
      helmetDescription: 'Generate code for bootstrap table',
      siteName: 'OneGenerate',
      isStriped: false,
      tableBorderType: 'Default',
      isTableBordered: false,
      isTableHovered: false,
      isTableDark: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let isStriped = this.state.isStriped
    // let isTableBordered = this.state.isTableBordered
    let tableBorderType = this.state.tableBorderType
    let isTableHovered = this.state.isTableHovered
    let isTableDark = this.state.isTableDark

    let tableBorderTypeString = ``
    if (tableBorderType === 'Bordered') {
      tableBorderTypeString = ` table-bordered`
    } else if (tableBorderType === 'Borderless') {
      tableBorderTypeString = ` table-borderless`
    }

    let isStripedString = ``
    if (isStriped) {
      isStripedString = ` table-striped`
    }
    let isTableHoveredString = ``
    if (isTableHovered) {
      isTableHoveredString = ` table-hover`
    }
    let isTableDarkString = ``
    if (isTableDark) {
      isTableDarkString = ` table-dark`
    }

    let codeString = `
    <table class="table${isStripedString}${tableBorderTypeString}${isTableHoveredString}${isTableDarkString}">
        <thead>
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>John</td>
            <td>Doe</td>
            <td>johndoe@example.com</td>
        </tr>
        <tr>
            <td>Mary</td>
            <td>Moe</td>
            <td>marymoe@example.com</td>
        </tr>
        <tr>
            <td>Tooley</td>
            <td>Dooley</td>
            <td>tooleydooley@example.com</td>
        </tr>
        </tbody>
    </table>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap_Table.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Striped?"
                      type="text"
                      name="isStriped"
                      id=""
                      value={this.state.isStriped}
                      onChange={this.handleInput('isStriped')}
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Growing?"
                      type="text"
                      name="isGrowing"
                      id=""
                      value={this.state.isGrowing}
                      onChange={this.handleInput('isGrowing')}
                    >
                      <MenuItem key="7" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="8" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Size"
                      type="text"
                      name="spinnerSize"
                      id=""
                      value={this.state.spinnerSize}
                      onChange={this.handleInput('spinnerSize')}
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Small">
                        Small
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Button?"
                      type="text"
                      name="isSpinnerButton"
                      id=""
                      value={this.state.isSpinnerButton}
                      onChange={this.handleInput('isSpinnerButton')}
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col className={this.state.isSpinnerButton ? 'visible' : 'invisible'} md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Button Label"
                      type="text"
                      name="spinnerButtonLabel"
                      id=""
                      value={this.state.spinnerButtonLabel}
                      onChange={this.handleInput('spinnerButtonLabel')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default BoostrapTable
