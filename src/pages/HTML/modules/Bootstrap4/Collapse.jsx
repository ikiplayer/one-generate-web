import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Collapse extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Boostrap collapse',
      helmetDescription: 'Generate code for bootstrap collapse',
      siteName: 'OneGenerate',
      collapseType: 'Basic',
      basicCollapseButtonLabel: 'Click',
      basicCollapseDetailLabel: 'Details...',
      items: [{ value: 'Label Click', detailValue: 'Details...' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let collapseType = this.state.collapseType
    let basicCollapseButtonLabel = this.state.basicCollapseButtonLabel
    let basicCollapseDetailLabel = this.state.basicCollapseDetailLabel
    let items = this.state.items

    let collapseTypeString = ''
    if (collapseType === 'Basic') {
      collapseTypeString = `
  <button data-toggle="collapse" data-target="#demo">${basicCollapseButtonLabel}</button>
  <div id="demo" class="collapse">
  ${basicCollapseDetailLabel}
  </div>`
    } else if (collapseType === 'Accordian') {
      collapseTypeString = `
  <div id="accordion">
  ${items
    .map((item, index) => {
      return `
  <div class="card">
    <div class="card-header">
      <a class="card-link" data-toggle="collapse" href="#collapse${index + 1}">
        ${item.value}
      </a>
    </div>
    <div id="collapse${index + 1}" class="collapse show" data-parent="#accordion">
      <div class="card-body">
        ${item.detailValue}
      </div>
    </div>
  </div>
  `
    })
    .join('')}
  </div>
  `
    }

    let codeString = `${collapseTypeString}
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Collapse.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked }, () => {
      this.updateCodeString()
    })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      value: `Label Click ${this.state.items.length + 1}`,
      detailValue: `Details ${this.state.items.length + 1} ...`,
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card style={this.state.collapseType === 'Basic' ? {} : { display: 'none' }}>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Collapse Type"
                      type="text"
                      name="collapseType"
                      id=""
                      value={this.state.collapseType}
                      onChange={this.handleInput('collapseType')}
                      select
                    >
                      <MenuItem key="0" value="Basic">
                        Basic
                      </MenuItem>
                      <MenuItem key="1" value="Accordian">
                        Accordian
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Button Label"
                      type="text"
                      name="basicCollapseButtonLabel"
                      id=""
                      value={this.state.basicCollapseButtonLabel}
                      onChange={this.handleInput('basicCollapseButtonLabel')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Detail"
                      type="text"
                      name="basicCollapseDetailLabel"
                      id=""
                      value={this.state.basicCollapseDetailLabel}
                      onChange={this.handleInput('basicCollapseDetailLabel')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card style={this.state.collapseType === 'Accordian' ? {} : { display: 'none' }}>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.items.map((item, index) => {
                      return (
                        <div key={index}>
                          <h6>{'Accordian Item ' + (index + 1)}</h6>
                          <Row gutter={8} className="m-4">
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Item Title Label"
                                name="value"
                                id=""
                                value={item.value}
                                onChange={this.handleItem('items', 'value', index)}
                              />
                            </Col>
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Item Detail Label"
                                name="detailValue"
                                id=""
                                value={item.detailValue}
                                onChange={this.handleItem('items', 'detailValue', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('items')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Collapse
