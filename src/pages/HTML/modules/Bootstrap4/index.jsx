import React from 'react'
import { Button, Row, Col } from 'antd'
import { Helmet } from 'react-helmet'
// import PaymentCard from 'components/CleanUIComponents/PaymentCard'
// import PaymentAccount from 'components/CleanUIComponents/PaymentAccount'
// import PaymentTransaction from 'components/CleanUIComponents/PaymentTransaction'
// import ChartCard from 'components/CleanUIComponents/ChartCard'
// import Authorize from 'components/LayoutComponents/Authorize/index.jsx'
// import { tableData } from './data.json'
import { Link } from 'react-router-dom'

class Bootstrap4 extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap 4 Components',
      helmetDescription: 'Generate code for bootstrap components',
      siteName: 'OneGenerate',
      bootstrapItems: [
        {
          id: 0,
          name: 'Alert',
          path: '/html/bootstrap/alert',
        },
        {
          id: 1,
          name: 'Badges',
          path: '/html/bootstrap/badges',
        },
        {
          id: 2,
          name: 'Button',
          path: '/html/bootstrap/button',
        },
        {
          id: 3,
          name: 'ButtonGroup',
          path: '/html/bootstrap/button-group',
        },
        {
          id: 4,
          name: 'Cards',
          path: '/html/bootstrap/cards',
        },
        {
          id: 5,
          name: 'Collapse',
          path: '/html/bootstrap/collapse',
        },
        {
          id: 6,
          name: 'Colors',
          path: '/html/bootstrap/colors',
        },
        {
          id: 7,
          name: 'Dropdown',
          path: '/html/bootstrap/dropdown',
        },
        // {
        //   id: 8,
        //   name: 'Form',
        //   path: '/html/bootstrap/form',
        // },
        {
          id: 9,
          name: 'Image',
          path: '/html/bootstrap/image',
        },
        // TODO
        // {
        //   id: 10,
        //   name: 'Input',
        //   path: '/html/bootstrap/input',
        // },

        {
          id: 11,
          name: 'Jumbotron',
          path: '/html/bootstrap/jumbotron',
        },
        {
          id: 12,
          name: 'List Groups',
          path: '/html/bootstrap/list-group',
        },
        {
          id: 13,
          name: 'Modal',
          path: '/html/bootstrap/modal',
        },
        {
          id: 14,
          name: 'Nav',
          path: '/html/bootstrap/nav',
        },
        {
          id: 15,
          name: 'Pagination',
          path: '/html/bootstrap/pagination',
        },
        {
          id: 16,
          name: 'Popover',
          path: '/html/bootstrap/popover',
        },
        {
          id: 17,
          name: 'Progress Bar',
          path: '/html/bootstrap/progress-bar',
        },
        {
          id: 18,
          name: 'Spinner',
          path: '/html/bootstrap/spinner',
        },
        // {
        //   id: 19,
        //   name: 'Table',
        //   path: '/html/bootstrap/table',
        // },
        {
          id: 20,
          name: 'Toast',
          path: '/html/bootstrap/toast',
        },
        {
          id: 21,
          name: 'Tooltip',
          path: '/html/bootstrap/tooltip',
        },
        {
          id: 22,
          name: 'Typography',
          path: '/html/bootstrap/typography',
        },
      ],
    }
  }

  linkTo = path => event => {
    window.location.href = path
  }

  render() {
    return (
      <div>
        <Helmet title="Bootstrap" />
        <Row gutter={16}>
          {this.state.bootstrapItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{ fontSize: 20, marginTop: 20, whiteSpace: 'unset', height: 100 }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
      </div>
    )
  }
}

export default Bootstrap4
