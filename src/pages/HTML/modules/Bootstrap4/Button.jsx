import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class BootstrapButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Button',
      helmetDescription: 'Generate code for bootstrap button',
      siteName: 'OneGenerate',
      style: 'Primary',
      size: 'Default',
      isFullWidth: '',
      isOutline: false,
      isActive: 'Active',
      spinnerType: 'None',
      spinnerColor: 'None',
      value: 'Bootstrap Button',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let style = this.state.style
    let size = this.state.size
    let isFullWidth = this.state.isFullWidth
    let isOutline = this.state.isOutline
    let isActive = this.state.isActive
    let spinnerType = this.state.spinnerType
    let spinnerColor = this.state.spinnerColor
    let value = this.state.value

    let isActiveString = ' active'
    if (isActive === 'Disabled') {
      isActiveString = ' disabled'
    }

    let isOutlineString = ''
    if (isOutline) {
      isOutlineString = `-outline`
    }

    let spinnerColorString = ''
    if (spinnerColor === 'Muted') {
      spinnerColorString = `text-muted`
    } else if (spinnerColor === 'Primary') {
      spinnerColorString = `text-primary`
    } else if (spinnerColor === 'Success') {
      spinnerColorString = `text-success`
    } else if (spinnerColor === 'Info') {
      spinnerColorString = `text-info`
    } else if (spinnerColor === 'Warning') {
      spinnerColorString = `text-warning`
    } else if (spinnerColor === 'Danger') {
      spinnerColorString = `text-danger`
    } else if (spinnerColor === 'Light') {
      spinnerColorString = `text-light`
    } else if (spinnerColor === 'Dark') {
      spinnerColorString = `text-dark`
    }

    let spinnerTypeString = ''
    if (spinnerType === 'Spinner') {
      spinnerTypeString = `<span class="spinner-border spinner-border-sm ${spinnerColorString}"></span>`
    } else if (spinnerType === 'Grow') {
      spinnerTypeString = `<span class="spinner-grow spinner-border-sm ${spinnerColorString}"></span>`
    }

    let sizeString = ''
    if (size === 'Small') {
      sizeString = ' btn-sm'
    } else if (size === 'Large') {
      sizeString = ' btn-lg'
    }

    let isFullWidthString = ''
    if (isFullWidth) {
      isFullWidthString = ' btn-block'
    }

    let styleString = ''
    if (style === 'Success') {
      styleString = `-success`
    } else if (style === 'Info') {
      styleString = `-info`
    } else if (style === 'Danger') {
      styleString = `-danger`
    } else if (style === 'Default') {
      styleString = `-default`
    } else if (style === 'Primary') {
      styleString = `-primary`
    } else if (style === 'Link') {
      styleString = `-link`
    } else if (style === 'Dark') {
      styleString = `-dark`
    } else if (style === 'Warning') {
      styleString = `-warning`
    }

    let codeString = `
    <button type="button" class="btn btn${isOutlineString}${styleString}${
      !isOutline ? sizeString : ''
    }${isFullWidthString}${!isOutline ? isActiveString : ''}">${spinnerTypeString}${value}</button>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Button.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Value"
                      type="text"
                      name="value"
                      id=""
                      value={this.state.value}
                      onChange={this.handleInput('value')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Style"
                      type="text"
                      name="style"
                      id=""
                      value={this.state.style}
                      onChange={this.handleInput('style')}
                      select
                    >
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="6" value="Dark">
                        Dark
                      </MenuItem>
                      <MenuItem key="7" value="Light">
                        Light
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Outlined"
                      type="text"
                      name="isOutline"
                      id=""
                      value={this.state.isOutline}
                      onChange={this.handleInput('isOutline')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Size"
                      type="text"
                      name="size"
                      id=""
                      value={this.state.size}
                      onChange={this.handleInput('size')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Small">
                        Small
                      </MenuItem>
                      <MenuItem key="2" value="Large">
                        Large
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Full Width"
                      type="text"
                      name="isFullWidth"
                      id=""
                      value={this.state.isFullWidth}
                      onChange={this.handleInput('isFullWidth')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        True
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        False
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Active"
                      type="text"
                      name="isActive"
                      id=""
                      value={this.state.isActive}
                      onChange={this.handleInput('isActive')}
                      select
                    >
                      <MenuItem key="0" value="Active">
                        Active
                      </MenuItem>
                      <MenuItem key="1" value="Disabled">
                        Disabled
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Type"
                      type="text"
                      name="spinnerType"
                      id=""
                      value={this.state.spinnerType}
                      onChange={this.handleInput('spinnerType')}
                      select
                    >
                      <MenuItem key="0" value="None">
                        None
                      </MenuItem>
                      <MenuItem key="1" value="Spinner">
                        Spinner
                      </MenuItem>
                      <MenuItem key="2" value="Grow">
                        Grow
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Color"
                      type="text"
                      name="spinnerColor"
                      id=""
                      value={this.state.spinnerColor}
                      onChange={this.handleInput('spinnerColor')}
                      select
                    >
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="6" value="Dark">
                        Dark
                      </MenuItem>
                      <MenuItem key="7" value="Light">
                        Light
                      </MenuItem>
                      <MenuItem key="8" value="Link">
                        Link
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default BootstrapButton
