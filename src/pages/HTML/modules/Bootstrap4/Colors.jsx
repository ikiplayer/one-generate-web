import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Colors extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Color',
      helmetDescription: 'Generate code for bootstrap color',
      siteName: 'OneGenerate',
      element: 'H4',
      text: 'Message',
      textStyle: 'Primary',
      textBackgroundStyle: 'None',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let element = this.state.element
    let text = this.state.text
    let textStyle = this.state.textStyle
    let textBackgroundStyle = this.state.textBackgroundStyle

    let elementString = ''
    if (element === 'H1') {
      elementString = 'h1'
    } else if (element === 'H2') {
      elementString = 'h2'
    } else if (element === 'H3') {
      elementString = 'h3'
    } else if (element === 'H4') {
      elementString = 'h4'
    } else if (element === 'H5') {
      elementString = 'h5'
    } else if (element === 'H6') {
      elementString = 'h6'
    }

    let textStyleString = ''
    if (textStyle === 'Primary') {
      textStyleString = `text-primary`
    } else if (textStyle === 'Secondary') {
      textStyleString = `text-secondary`
    } else if (textStyle === 'Success') {
      textStyleString = `text-success`
    } else if (textStyle === 'Danger') {
      textStyleString = `text-danger`
    } else if (textStyle === 'Warning') {
      textStyleString = `text-warning`
    } else if (textStyle === 'Info') {
      textStyleString = `text-info`
    } else if (textStyle === 'Light') {
      textStyleString = `text-light`
    } else if (textStyle === 'Dark') {
      textStyleString = `text-dark`
    }

    let textBackgroundStyleString = ''
    if (textBackgroundStyle === 'Primary') {
      textBackgroundStyleString = ` bg-primary`
    } else if (textBackgroundStyle === 'Secondary') {
      textBackgroundStyleString = ` bg-secondary`
    } else if (textBackgroundStyle === 'Success') {
      textBackgroundStyleString = ` bg-success`
    } else if (textBackgroundStyle === 'Danger') {
      textBackgroundStyleString = ` bg-danger`
    } else if (textBackgroundStyle === 'Warning') {
      textBackgroundStyleString = ` bg-warning`
    } else if (textBackgroundStyle === 'Info') {
      textBackgroundStyleString = ` bg-info`
    } else if (textBackgroundStyle === 'Light') {
      textBackgroundStyleString = ` bg-light`
    } else if (textBackgroundStyle === 'Dark') {
      textBackgroundStyleString = ` bg-dark`
    }

    let codeString = `
  <${elementString} class="${textStyleString}${textBackgroundStyleString}">${text}</${elementString}>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Colors.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text"
                      type="text"
                      name="text"
                      id=""
                      value={this.state.text}
                      onChange={this.handleInput('text')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Header Type"
                      type="text"
                      name="element"
                      id=""
                      value={this.state.element}
                      onChange={this.handleInput('element')}
                      select
                    >
                      <MenuItem key="0" value="H1">
                        H1
                      </MenuItem>
                      <MenuItem key="1" value="H2">
                        H2
                      </MenuItem>
                      <MenuItem key="2" value="H3">
                        H3
                      </MenuItem>
                      <MenuItem key="3" value="H4">
                        H4
                      </MenuItem>
                      <MenuItem key="4" value="H5">
                        H5
                      </MenuItem>
                      <MenuItem key="5" value="H6">
                        H6
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Style"
                      type="text"
                      name="textStyle"
                      id=""
                      value={this.state.textStyle}
                      onChange={this.handleInput('textStyle')}
                      select
                    >
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="6" value="Light">
                        Light
                      </MenuItem>
                      <MenuItem key="7" value="Dark">
                        Dark
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Background Style"
                      type="text"
                      name="textBackgroundColor"
                      id=""
                      value={this.state.textBackgroundColor}
                      onChange={this.handleInput('textBackgroundColor')}
                      select
                    >
                      <MenuItem key="0" value="None">
                        None
                      </MenuItem>
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="6" value="Light">
                        Light
                      </MenuItem>
                      <MenuItem key="7" value="Dark">
                        Dark
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Colors
