import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import ReactHtmlParser from 'react-html-parser'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Spinner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap spinner',
      helmetDescription: 'Generate code for bootstrap spinner',
      siteName: 'OneGenerate',
      spinnerStyle: 'Default',
      isGrowing: false,
      spinnerSize: 'Default',
      isSpinnerButton: false,
      spinnerButtonLabel: 'Button Label',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let spinnerStyle = this.state.spinnerStyle
    let isGrowing = this.state.isGrowing
    let spinnerSize = this.state.spinnerSize
    let isSpinnerButton = this.state.isSpinnerButton
    let spinnerButtonLabel = this.state.spinnerButtonLabel

    let codeString = ''

    let spinnerStyleString = ''
    if (spinnerStyle === 'Default') {
      spinnerStyleString = ` text-muted`
    } else if (spinnerStyle === 'Primary') {
      spinnerStyleString = ` text-primary`
    } else if (spinnerStyle === 'Success') {
      spinnerStyleString = ` text-success`
    } else if (spinnerStyle === 'Info') {
      spinnerStyleString = ` text-info`
    } else if (spinnerStyle === 'Warning') {
      spinnerStyleString = ` text-warning`
    } else if (spinnerStyle === 'Danger') {
      spinnerStyleString = ` text-danger`
    } else if (spinnerStyle === 'Secondary') {
      spinnerStyleString = ` text-secondary`
    } else if (spinnerStyle === 'Dark') {
      spinnerStyleString = ` text-dark`
    } else if (spinnerStyle === 'Light') {
      spinnerStyleString = ` text-light`
    }

    let isGrowingString = ''
    if (isGrowing) {
      isGrowingString = `spinner-border`
    } else {
      isGrowingString = `spinner-grow`
    }

    let spinnerSizeString = ''
    if (spinnerSize === 'Small') {
      if (isGrowing) {
        spinnerSizeString = ` spinner-grow-sm`
      } else {
        spinnerSizeString = ` spinner-border-sm`
      }
    }

    if (isSpinnerButton) {
      codeString = `
      <button class="btn ${spinnerButtonLabel}">
        <span class="${isGrowingString}${spinnerSizeString}"></span>
        ${spinnerButtonLabel}
      </button>
      `
    } else {
      codeString = `\t<div class="${isGrowingString}${spinnerSizeString}${spinnerStyleString}"></div>`
    }

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Spinner.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Style"
                      type="text"
                      name="spinnerStyle"
                      id=""
                      value={this.state.spinnerStyle}
                      onChange={this.handleInput('spinnerStyle')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="6" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="7" value="Dark">
                        Dark
                      </MenuItem>
                      <MenuItem key="8" value="Light">
                        Light
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Growing?"
                      type="text"
                      name="isGrowing"
                      id=""
                      value={this.state.isGrowing}
                      onChange={this.handleInput('isGrowing')}
                      select
                    >
                      <MenuItem key="7" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="8" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Size"
                      type="text"
                      name="spinnerSize"
                      id=""
                      value={this.state.spinnerSize}
                      onChange={this.handleInput('spinnerSize')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Small">
                        Small
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Button?"
                      type="text"
                      name="isSpinnerButton"
                      id=""
                      value={this.state.isSpinnerButton}
                      onChange={this.handleInput('isSpinnerButton')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col className={this.state.isSpinnerButton ? 'visible' : 'invisible'} md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Spinner Button Label"
                      type="text"
                      name="spinnerButtonLabel"
                      id=""
                      value={this.state.spinnerButtonLabel}
                      onChange={this.handleInput('spinnerButtonLabel')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div class="container">
                  <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
                </div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Spinner
