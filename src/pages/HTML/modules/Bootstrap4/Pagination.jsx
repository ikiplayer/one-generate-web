import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Button, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Pagination extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap pagination',
      helmetDescription: 'Generate code for bootstrap pagination',
      siteName: 'OneGenerate',
      paginationType: 'Pagination',
      size: 'Default',
      alignment: 'Default',
      items: [{ label: 'Title', url: '#', isDisabled: false }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let paginationType = this.state.paginationType
    let size = this.state.size
    let alignment = this.state.alignment
    let items = this.state.items

    let sizeString = ''
    if (size === 'Small') {
      sizeString = ` pagination-sm`
    } else if (size === 'Large') {
      sizeString = ` pagination-lg`
    }

    let alignmentString = ''
    if (alignment === 'Center') {
      alignmentString = ` justify-content-center`
    } else if (alignment === 'End') {
      alignmentString = ` justify-content-end`
    }

    let codeString = ``

    if (paginationType === 'Pagination') {
      codeString = `
      <ul class="pagination${sizeString}${alignmentString}">${items
        .map(item => {
          let isDisabledString = ''
          if (item.isDisabled) {
            isDisabledString = ` disabled`
          }
          return `\n\t\t<li class="page-item${isDisabledString}"><a class="page-link" href="${
            item.url
          }">${item.label}</a></li>`
        })
        .join('')}
      </ul>
      `
    } else if (paginationType === 'Breadcrumbs') {
      codeString = `
      <ul class="breadcrumb">${items
        .map(item => {
          let isDisabledString = ''

          if (item.isDisabled) {
            isDisabledString = ` disabled`
          }
          return `\n\t\t<li class="breadcrumb-item${isDisabledString}"><a href="${item.url}">${
            item.label
          }</a></li>`
        })
        .join('')}
      </ul>
      `
    }

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Pagination.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({ label: this.state.items.length, url: '#', isDisabled: false })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Pagination Type"
                      type="text"
                      name="paginationType"
                      id=""
                      value={this.state.paginationType}
                      onChange={this.handleInput('paginationType')}
                      select
                    >
                      <MenuItem key="0" value="Pagination">
                        Pagination
                      </MenuItem>
                      <MenuItem key="1" value="Breadcrumbs">
                        Breadcrumbs
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Alignment"
                      type="text"
                      name="alignment"
                      id=""
                      value={this.state.alignment}
                      onChange={this.handleInput('alignment')}
                      select
                    >
                      <MenuItem key="0" value="Default">
                        Default
                      </MenuItem>
                      <MenuItem key="1" value="Small">
                        Small
                      </MenuItem>
                      <MenuItem key="2" value="Large">
                        Large
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.items.map((item, index) => {
                      return (
                        <div key={index}>
                          <h6>{'Pagination ' + (index + 1)}</h6>
                          <Row gutter={8} className="m-4">
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Label"
                                name="label"
                                id=""
                                value={item.label}
                                onChange={this.handleItem('items', 'label', index)}
                              />
                            </Col>
                            <Col md={12}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="url"
                                name="url"
                                id=""
                                value={item.url}
                                onChange={this.handleItem('items', 'url', index)}
                              />
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Disabled?"
                                name="isDisabled"
                                id=""
                                value={item.isDisabled}
                                onChange={this.handleItem('items', 'isDisabled', index)}
                                select
                              >
                                <MenuItem key="0" value={true}>
                                  Yes
                                </MenuItem>
                                <MenuItem key="1" value={false}>
                                  No
                                </MenuItem>
                              </TextField>
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('items')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div class="container">
                  <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
                </div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Pagination
