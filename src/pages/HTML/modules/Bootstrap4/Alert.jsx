import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, MenuItem, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import ReactHtmlParser from 'react-html-parser'

class Alert extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Bootstrap Alert',
      helmetDescription: 'Generate code for bootstrap alert',
      siteName: 'OneGenerate',
      color: 'Success',
      strongValue: 'Alert',
      value: 'Message',
      urlLink: '',
      urlLinkMessage: 'This is the link message',
      dismissable: false,
      animationFade: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let color = this.state.color
    let strongValue = this.state.strongValue
    let value = this.state.value
    let urlLink = this.state.urlLink
    let urlLinkMessage = this.state.urlLinkMessage
    let dismissable = this.state.dismissable
    let animationFade = this.state.animationFade

    let colorString = ''
    if (color === 'Primary') {
      colorString = `alert-primary`
    } else if (color === 'Secondary') {
      colorString = `alert-secondary`
    } else if (color === 'Success') {
      colorString = `alert-success`
    } else if (color === 'Danger') {
      colorString = `alert-danger`
    } else if (color === 'Warning') {
      colorString = `alert-warning`
    } else if (color === 'Info') {
      colorString = `alert-info`
    } else if (color === 'Light') {
      colorString = `alert-light`
    } else if (color === 'Dark') {
      colorString = `alert-dark`
    }

    let strongValueString = ''
    if (strongValue) {
      strongValueString = `<strong>${strongValue}</strong>`
    }

    let dismissableString = ''
    let dismissableString2 = ''
    if (dismissable) {
      dismissableString = `alert-dismissible`
      dismissableString2 = `<button type="button" class="close" data-dismiss="alert">&times;</button>
      `
    }

    let animationFadeString = ''
    if (animationFade) {
      animationFadeString = 'fade show'
    }

    let codeString = `
    <div class="alert ${colorString} ${dismissableString} ${animationFadeString}">
    ${dismissableString2}${strongValueString} ${value} <a href="${urlLink}" class="alert-link">${urlLinkMessage}</a>
    </div>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Bootstrap4_Alert.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
  
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Color"
                      type="text"
                      name="color"
                      id=""
                      value={this.state.color}
                      onChange={this.handleInput('color')}
                      select
                    >
                      <MenuItem key="0" value="Primary">
                        Primary
                      </MenuItem>
                      <MenuItem key="1" value="Secondary">
                        Secondary
                      </MenuItem>
                      <MenuItem key="2" value="Success">
                        Success
                      </MenuItem>
                      <MenuItem key="3" value="Danger">
                        Danger
                      </MenuItem>
                      <MenuItem key="4" value="Warning">
                        Warning
                      </MenuItem>
                      <MenuItem key="5" value="Info">
                        Info
                      </MenuItem>
                      <MenuItem key="6" value="Light">
                        Light
                      </MenuItem>
                      <MenuItem key="7" value="Dark">
                        Dark
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Bold Text"
                      type="text"
                      name="strongValue"
                      id=""
                      value={this.state.strongValue}
                      onChange={this.handleInput('strongValue')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Value"
                      type="text"
                      name="value"
                      id=""
                      value={this.state.value}
                      onChange={this.handleInput('value')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Url Link"
                      type="text"
                      name="urlLink"
                      id=""
                      value={this.state.urlLink}
                      onChange={this.handleInput('urlLink')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Url Link Text"
                      type="text"
                      name="urlLinkMessage"
                      id=""
                      value={this.state.urlLinkMessage}
                      onChange={this.handleInput('urlLinkMessage')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Dismissable"
                      type="text"
                      name="dismissable"
                      id=""
                      value={this.state.dismissable}
                      onChange={this.handleInput('dismissable')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Animation"
                      type="text"
                      name="animationFade"
                      id=""
                      value={this.state.animationFade}
                      onChange={this.handleInput('animationFade')}
                      select
                    >
                      <MenuItem key="0" value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Col md={24}>
                <div style={{ paddingLeft: '5%' }}>{ReactHtmlParser(this.state.codeItem)}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Alert
