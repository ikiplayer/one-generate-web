import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import { TextField, Button, Divider, Toolbar, Snackbar, SnackbarContent } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import AceEditor from 'react-ace'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Business extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Open Graph Business',
      helmetDescription: 'Generate code for Open Graph business',
      siteName: 'OneGenerate',
      title: '',
      url: '',
      streetAddress: '',
      city: '',
      stateProvinceRegion: '',
      postalZipcode: '',
      country: '',
      imageItems: [{ url: '' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // if (
    //   this.state.title !== prevState.title ||
    //   this.state.url !== prevState.url ||
    //   this.state.streetAddress !== prevState.streetAddress ||
    //   this.state.city !== prevState.city ||
    //   this.state.stateProvinceRegion !== prevState.stateProvinceRegion ||
    //   this.state.postalZipcode !== prevState.postalZipcode ||
    //   this.state.country !== prevState.country
    // ) {
    //   this.updateCodeString()
    // }
  }

  updateCodeString() {
    let title = this.state.title
    let url = this.state.url
    let streetAddress = this.state.streetAddress
    let city = this.state.city
    let stateProvinceRegion = this.state.stateProvinceRegion
    let postalZipcode = this.state.postalZipcode
    let country = this.state.country
    let imageItems = this.state.imageItems

    let codeString = `
    <meta property="og:type" content="business.business">
    <meta property="og:title" content="${title}">
    <meta property="og:url" content="${url}">
    <meta property="og:image" content="">
    <meta property="business:contact_data:street_address" content="${streetAddress}">
    <meta property="business:contact_data:locality" content="${city}">
    <meta property="business:contact_data:region" content="${stateProvinceRegion}">
    <meta property="business:contact_data:postal_code" content="${postalZipcode}">
    <meta property="business:contact_data:country_name" content="${country}">${imageItems
      .map(item => {
        return `\n\t<meta property="og:image" content="${item.url}">`
      })
      .join('')}
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Open_Graph_Business.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState(
      { [name]: event.target.value },
      () => {
        this.updateCodeString()
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleImageItemClick = () => event => {
    let items = this.state.imageItems
    items.push({
      url: '',
    })

    this.setState(
      {
        items: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleSongItemClick = () => event => {
    let items = this.state.authorItems
    items.push({
      url: '',
    })

    this.setState(
      {
        items: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleBookItemClick = () => event => {
    let items = this.state.bookItems
    items.push({
      url: '',
    })

    this.setState(
      {
        items: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Title"
                      type="text"
                      name="title"
                      id=""
                      value={this.state.title}
                      onChange={this.handleInput('title')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Url</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Url"
                      type="text"
                      name="url"
                      id=""
                      value={this.state.url}
                      onChange={this.handleInput('url')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Street Address</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Street Address"
                      type="text"
                      name="streetAddress"
                      id=""
                      value={this.state.streetAddress}
                      onChange={this.handleInput('streetAddress')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">City</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="City"
                      type="text"
                      name="city"
                      id=""
                      value={this.state.city}
                      onChange={this.handleInput('city')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">State / Province / Region</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="State / Province / Region"
                      type="text"
                      name="stateProvinceRegion"
                      id=""
                      value={this.state.stateProvinceRegion}
                      onChange={this.handleInput('stateProvinceRegion')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Postal / Zipcode</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Postal / Zipcode"
                      type="text"
                      name="postalZipcode"
                      id=""
                      value={this.state.postalZipcode}
                      onChange={this.handleInput('postalZipcode')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Country</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Country"
                      type="text"
                      name="country"
                      id=""
                      value={this.state.country}
                      onChange={this.handleInput('country')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Image Url</Label> */}
                    {this.state.imageItems.map((item, index) => {
                      return (
                        <div key={index}>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <h6>{'Image ' + (index + 1)}</h6>
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Image Url"
                                name="url"
                                value={item.url}
                                id=""
                                onChange={this.handleItem('imageItems', 'url', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleImageItemClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Business
