import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import { TextField, Button, Divider, Toolbar, Snackbar, SnackbarContent } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import AceEditor from 'react-ace'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Restaurant extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Open Graph Restaurant',
      helmetDescription: 'Generate code for Open Graph restaurant',
      siteName: 'OneGenerate',
      title: '',
      url: '',
      website: '',
      description: '',
      delete: '',
      streetAddress: '',
      city: '',
      stateProvinceRegion: '',
      postalZipcode: '',
      country: '',
      email: '',
      phoneNumber: '',
      menuItems: [{ url: '' }],
      imageItems: [{ url: '' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.title !== prevState.title ||
      this.state.url !== prevState.url ||
      this.state.website !== prevState.website ||
      this.state.description !== prevState.description ||
      this.state.streetAddress !== prevState.streetAddress ||
      this.state.city !== prevState.city ||
      this.state.stateProvinceRegion !== prevState.stateProvinceRegion ||
      this.state.postalZipcode !== prevState.postalZipcode ||
      this.state.country !== prevState.country ||
      this.state.email !== prevState.email ||
      this.state.phoneNumber !== prevState.phoneNumber
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let title = this.state.title
    let url = this.state.url
    let website = this.state.website
    let description = this.state.description
    let streetAddress = this.state.streetAddress
    let city = this.state.city
    let stateProvinceRegion = this.state.stateProvinceRegion
    let postalZipcode = this.state.postalZipcode
    let country = this.state.country
    let email = this.state.email
    let phoneNumber = this.state.phoneNumber
    let menuItems = this.state.menuItems
    let imageItems = this.state.imageItems

    let descriptionString = ''
    if (description) {
      descriptionString = `\n\t<meta property="og:description" content="${description}">`
    }
    let codeString = `
    <meta property="og:type" content="restaurant.restaurant">
    <meta property="og:title" content="${title}">
    <meta property="og:url" content="${url}">${descriptionString}
    <meta property="restaurant:contact_info:website" content="${website}">
    <meta property="restaurant:contact_info:street_address" content="${streetAddress}">
    <meta property="restaurant:contact_info:locality" content="${city}">
    <meta property="restaurant:contact_info:region" content="${stateProvinceRegion}">
    <meta property="restaurant:contact_info:postal_code" content="${postalZipcode}">
    <meta property="restaurant:contact_info:country_name" content="${country}">
    <meta property="restaurant:contact_info:email" content="${email}">
    <meta property="restaurant:contact_info:phone_number" content="${phoneNumber}">${menuItems
      .map(item => {
        return `\n\t<meta property="restaurant:menu" content="${item.url}">`
      })
      .join('')}${imageItems
      .map((item, index) => {
        return `\n\t<meta property="og:image" content="${item.url}">`
      })
      .join('')}
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Open_Graph_Restaurant.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleImageItemClick = () => event => {
    let items = this.state.imageItems
    items.push({
      url: '',
    })

    this.setState(
      {
        imageItems: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleMenuItemClick = () => event => {
    let items = this.state.menuItems
    items.push({
      url: '',
    })

    this.setState(
      {
        menuItems: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  //   handleClick = () => event => {
  //     let items = this.state.items;
  //     items.push({
  //       methodName: "",
  //       methodstreetAddress: "",
  //       addOrRemove: "Add method"
  //     });

  //     this.setState(
  //       {
  //         items: items
  //       },
  //       () => {
  //         this.updateCodeString();
  //       }
  //     );
  //   };

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Title"
                      type="text"
                      name="title"
                      id=""
                      value={this.state.title}
                      onChange={this.handleInput('title')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Url</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Url"
                      type="text"
                      name="url"
                      id=""
                      value={this.state.url}
                      onChange={this.handleInput('url')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Website</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Website"
                      type="text"
                      name="website"
                      id=""
                      value={this.state.website}
                      onChange={this.handleInput('website')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Description</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Street Address</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Street Address"
                      type="text"
                      name="streetAddress"
                      id=""
                      value={this.state.streetAddress}
                      onChange={this.handleInput('streetAddress')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">City</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="City"
                      type="text"
                      name="city"
                      id=""
                      value={this.state.city}
                      onChange={this.handleInput('city')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">State / Province / Region</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="State / Province / Region"
                      type="text"
                      name="stateProvinceRegion"
                      id=""
                      value={this.state.stateProvinceRegion}
                      onChange={this.handleInput('stateProvinceRegion')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Postal / Zipcode</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Postal / Zipcode"
                      type="text"
                      name="postalZipcode"
                      id=""
                      value={this.state.postalZipcode}
                      onChange={this.handleInput('postalZipcode')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Country</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Country"
                      type="text"
                      name="country"
                      id=""
                      value={this.state.country}
                      onChange={this.handleInput('country')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Email</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Email"
                      type="text"
                      name="email"
                      id=""
                      value={this.state.email}
                      onChange={this.handleInput('email')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Phone Number</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Phone Number"
                      type="text"
                      name="phoneNumber"
                      id=""
                      value={this.state.phoneNumber}
                      onChange={this.handleInput('phoneNumber')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {this.state.menuItems.map((item, index) => {
                    return (
                      <div key={index}>
                        <Row gutter={8} className="m-4">
                          <Col md={24}>
                            <h6>{'Menu ' + (index + 1)}</h6>
                            <TextField
                              placeholder=" "
                              fullWidth
                              variant="outlined"
                              label="Menu Url"
                              name="url"
                              value={item.url}
                              id=""
                              onChange={this.handleItem('menuItems', 'url', index)}
                            />
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleMenuItemClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {this.state.imageItems.map((item, index) => {
                    return (
                      <div key={index}>
                        <Row gutter={8} className="m-4">
                          <Col md={24}>
                            <h6>{'Image ' + (index + 1)}</h6>
                            <TextField
                              placeholder=" "
                              fullWidth
                              variant="outlined"
                              label="Image Url"
                              name="url"
                              value={item.url}
                              id=""
                              onChange={this.handleItem('imageItems', 'url', index)}
                            />
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleImageItemClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Restaurant
