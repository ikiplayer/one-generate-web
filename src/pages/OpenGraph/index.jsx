import React from 'react'
import { Button, Row, Col, Card, Avatar } from 'antd'
import { Helmet } from 'react-helmet'
// import PaymentCard from 'components/CleanUIComponents/PaymentCard'
// import PaymentAccount from 'components/CleanUIComponents/PaymentAccount'
// import PaymentTransaction from 'components/CleanUIComponents/PaymentTransaction'
// import ChartCard from 'components/CleanUIComponents/ChartCard'
// import Authorize from 'components/LayoutComponents/Authorize/index.jsx'
// import { tableData } from './data.json'
import { Link } from 'react-router-dom'
const { Meta } = Card

class OpenGraphIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = { helmetTitle: '', helmetDescription: 'Generate code for bootstrap', siteName: 'OneGenerate',
      openGraphItems: [
        {
          id: 0,
          name: 'Article',
          path: '/open-graph/article',
        },
        {
          id: 1,
          name: 'Book',
          path: '/open-graph/book',
        },
        {
          id: 2,
          name: 'Book Author',
          path: '/open-graph/book-author',
        },
        {
          id: 3,
          name: 'Book Genre',
          path: '/open-graph/book-genre',
        },
        {
          id: 4,
          name: 'Business',
          path: '/open-graph/business',
        },
        {
          id: 5,
          name: 'Fitness Course',
          path: '/open-graph/fitness-course',
        },
        {
          id: 6,
          name: 'Game Achievement',
          path: '/open-graph/game-achievement',
        },
        {
          id: 7,
          name: 'Music Album',
          path: '/open-graph/music-album',
        },
        {
          id: 8,
          name: 'Music Playlist',
          path: '/open-graph/music-playlist',
        },
        {
          id: 9,
          name: 'Music Radio Station',
          path: '/open-graph/music-radio-station',
        },
        {
          id: 10,
          name: 'Music Song',
          path: '/open-graph/music-song',
        },
        {
          id: 11,
          name: 'Place',
          path: '/open-graph/place',
        },
        {
          id: 12,
          name: 'Product',
          path: '/open-graph/product',
        },
        {
          id: 13,
          name: 'Product Group',
          path: '/open-graph/product-group',
        },
        {
          id: 14,
          name: 'Product Item',
          path: '/open-graph/product-item',
        },
        {
          id: 15,
          name: 'Profile',
          path: '/open-graph/profile',
        },
        {
          id: 16,
          name: 'Restaurant',
          path: '/open-graph/restaurant',
        },
        {
          id: 17,
          name: 'Restaurant Menu',
          path: '/open-graph/restaurant-menu',
        },
        {
          id: 18,
          name: 'Restaurant Menu Item',
          path: '/open-graph/restaurant-menu-item',
        },
        {
          id: 19,
          name: 'Restaurant Menu Section',
          path: '/open-graph/restaurant-menu-section',
        },
        {
          id: 20,
          name: 'Video Episode',
          path: '/open-graph/video-episode',
        },
        {
          id: 21,
          name: 'Video Generic',
          path: '/open-graph/video-generic',
        },
        {
          id: 22,
          name: 'Video Movie',
          path: '/open-graph/video-movie',
        },
        {
          id: 23,
          name: 'Video Tv Show',
          path: '/open-graph/video-tv-show',
        },
      ],
    }
  }

  linkTo = path => event => {
    window.location.href = path
  }

  render() {
    return (
      <div>
        <Helmet title="Open Graph" />
        <Row gutter={16}>
          {this.state.openGraphItems.map(item => {
            return (
              <Col xs={12} md={8} lg={6} span={6} key={item.id}>
                {/* <Button
                  value="large"
                  block={true}
                  style={{
                    fontSize: 20,
                    marginTop: 20,
                    whiteSpace: 'unset',
                    height: 100,
                  }}
                >
                  <Link to={item.path}>{item.name}</Link>

                </Button> */}
                <Link to={item.path}>
                  <Card
                    // cover={
                    //   <img
                    //     alt="example"
                    //     src={item.image}
                    //     style={{ height: 50, objectFit: 'cover' }}
                    //   />
                    // }
                    style={{ marginBottom: 15 }}
                    // actions={[<Icon type="" />, <Icon type="heart" />, <Icon type="" />]}
                  >
                    <Meta title={item.name} description="" />
                  </Card>
                </Link>
              </Col>
            )
          })}
        </Row>
      </div>
    )
  }
}

export default OpenGraphIndex
