import React, { Component } from 'react'
import { Row, Col, Card } from 'antd'
import { Link } from 'react-router-dom'
import { Icon } from 'react-fa'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel'
import styles from './style.module.scss'
import { Helmet } from 'react-helmet'
import 'pure-react-carousel/dist/react-carousel.es.css'

const { Meta } = Card

export default class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      helmetTitle: 'One Generate',
      helmetDescription: 'Easily create code for Wordpress, HTML, JSON-LD, Microdata, Bootstrap',
      siteName: 'One Generate',
      moduleItems: [
        {
          title: 'Wordpress',
          path: '/wordpress',
          image: 'resources/images/icons/wordpress.png',
          description:
            'With half of the world using Wordpress as their main content management system, use this to help speed up the creation of your website',
        },
        {
          title: 'JSON-LD',
          path: '/json-ld',
          image: 'resources/images/icons/jsonld.png',
          description:
            'JSON-LD stands for JavaScript Object Notation for Linked Data, which consists of multi-dimensional arrays. This contains a multiple of quick and easy generators to create JSON-LD code',
        },
        {
          title: 'Open Graph',
          path: '/open-graph',
          image: 'resources/images/icons/opengraph.png',
          description:
            "Integrate Open Graph meta tags into your page's content, you can identify which elements of your page you want to show when someone share's your page",
        },
        {
          title: 'Microdata',
          path: '/microdata',
          image: 'resources/images/icons/microdata.png',
          description:
            'Creates HTML attributes to embed simple machine-readable data in HTML documents.',
        },
      ],
      wordpressItems: [
        {
          id: 0,
          name: 'Cron Job Event',
          path: '/wordpress/cron-job-event',
          image: 'resources/images/wordpress/wordpress-1.jpg',
          description:
            'WordPress comes with its own cron system which allows it to perform scheduled tasks. For example, checking for updates, deleting old comments from trash, etc.',
        },
        {
          id: 1,
          name: 'Custom Post Generator',
          path: '/wordpress/custom-post-generator',
          image: 'resources/images/wordpress/wordpress-2.jpg',
          description:
            'Generate Custom Post Types from the WordPress administration which is easy to understand. It’s a must have for any user working with WordPress.',
        },
        {
          id: 2,
          name: 'Dashboard Widgets Generator',
          path: '/wordpress/dashboard-widgets-generator',
          image: 'resources/images/wordpress/wordpress-3.jpg',
          description:
            'The Dashboard Widgets Generator makes it very simple to add new widgets to the administration dashboard',
        },
        {
          id: 3,
          name: 'Menu Generator',
          path: '/wordpress/menu-generator',
          image: 'resources/images/wordpress/wordpress-4.jpg',
          description:
            'With WordPress, you can easily create a custom menu, which usually serves as a navigation menu for your site.',
        },
        {
          id: 4,
          name: 'Post Status Generator',
          path: '/wordpress/post-status-generator',
          image: 'resources/images/wordpress/wordpress-5.jpg',
          description:
            'Post status allows users to set a workflow status for a post in WordPress. WordPress themes and plugins can also define custom post statuses for more complex websites.',
        },
        {
          id: 5,
          name: 'Quicktag for Classic Editor Generator',
          path: '/wordpress/quicktag-for-classic-editor-generator',
          image: 'resources/images/wordpress/wordpress-6.jpg',
          description:
            'Quicktags for Classic Editor Generator allows you to include additional buttons in the Text (HTML) mode of the WordPress editor.',
        },
        {
          id: 6,
          name: 'Register / Enqueue Scripts (JS)',
          path: '/wordpress/register-enqueue-scripts-js',
          image: 'resources/images/wordpress/wordpress-7.jpg',
          description:
            'Use this register/enqueue generator for javascript. Registering it only tells WordPress about it, enqueuing it adds it to the website.',
        },
        {
          id: 7,
          name: 'Shortcode Generator',
          path: '/wordpress/shortcode-generator',
          image: 'resources/images/wordpress/wordpress-9.jpg',
          description:
            'Shortcode Generator is a simple way to create the shortcodes without the necessity to write the code. Those are components, which can be inserted in the Posts or Pages.',
        },
      ],
      openGraphItems: [
        {
          id: 0,
          name: 'Article',
          path: '/open-graph/article',
        },
        {
          id: 1,
          name: 'Book',
          path: '/open-graph/book',
        },
        {
          id: 2,
          name: 'Book Author',
          path: '/open-graph/book-author',
        },
        {
          id: 3,
          name: 'Book Genre',
          path: '/open-graph/book-genre',
        },
        {
          id: 4,
          name: 'Business',
          path: '/open-graph/business',
        },
        {
          id: 5,
          name: 'Fitness Course',
          path: '/open-graph/fitness-course',
        },
        {
          id: 6,
          name: 'Game Achievement',
          path: '/open-graph/game-achievement',
        },
        {
          id: 7,
          name: 'Music Album',
          path: '/open-graph/music-album',
        },
        {
          id: 8,
          name: 'Music Playlist',
          path: '/open-graph/music-playlist',
        },
        {
          id: 9,
          name: 'Music Radio Station',
          path: '/open-graph/music-radio-station',
        },
        {
          id: 10,
          name: 'Music Song',
          path: '/open-graph/music-song',
        },
        {
          id: 11,
          name: 'Place',
          path: '/open-graph/place',
        },
        {
          id: 12,
          name: 'Product',
          path: '/open-graph/product',
        },
        {
          id: 13,
          name: 'Product Group',
          path: '/open-graph/product-group',
        },
        {
          id: 14,
          name: 'Product Item',
          path: '/open-graph/product-item',
        },
        {
          id: 15,
          name: 'Profile',
          path: '/open-graph/profile',
        },
        {
          id: 16,
          name: 'Restaurant',
          path: '/open-graph/restaurant',
        },
        {
          id: 17,
          name: 'Restaurant Menu',
          path: '/open-graph/restaurant-menu',
        },
        {
          id: 18,
          name: 'Restaurant Menu Item',
          path: '/open-graph/restaurant-menu-item',
        },
        {
          id: 19,
          name: 'Restaurant Menu Section',
          path: '/open-graph/restaurant-menu-section',
        },
        {
          id: 20,
          name: 'Video Episode',
          path: '/open-graph/video-episode',
        },
        {
          id: 21,
          name: 'Video Generic',
          path: '/open-graph/video-generic',
        },
        {
          id: 22,
          name: 'Video Movie',
          path: '/open-graph/video-movie',
        },
        {
          id: 23,
          name: 'Video Tv Show',
          path: '/open-graph/video-tv-show',
        },
      ],
      jsonLdItems: [
        { id: 0, name: 'Aggregate Offer', path: '/json-ld/aggregate-offer' },
        { id: 1, name: 'Aggregate Rating', path: '/json-ld/aggregate-rating' },
        {
          id: 2,
          name: 'Article',
          path: '/json-ld/article',
        },
        {
          id: 3,
          name: 'Event',
          path: '/json-ld/event',
        },
        {
          id: 4,
          name: 'Music Album',
          path: '/json-ld/music-album',
        },
        {
          id: 5,
          name: 'Music Playlist',
          path: '/json-ld/music-playlist',
        },
        {
          id: 6,
          name: 'Organization',
          path: '/json-ld/organization',
        },
        {
          id: 7,
          name: 'Person',
          path: '/json-ld/person',
        },
        {
          id: 8,
          name: 'Product Offer',
          path: '/json-ld/product-offer',
        },
        {
          id: 9,
          name: 'Recipe',
          path: '/json-ld/recipe',
        },
        {
          id: 10,
          name: 'Restaurant',
          path: '/json-ld/restaurant',
        },
        {
          id: 11,
          name: 'Review',
          path: '/json-ld/review',
        },
        {
          id: 12,
          name: 'Software Application',
          path: '/json-ld/software-application',
        },
        {
          id: 13,
          name: 'Video',
          path: '/json-ld/video',
        },
      ],
      microdataItems: [
        { id: 0, name: 'Aggregate Offer', path: '/microdata/aggregate-offer' },
        { id: 1, name: 'Aggregate Rating', path: '/microdata/aggregate-rating' },
        {
          id: 2,
          name: 'Article',
          path: '/microdata/article',
        },
        {
          id: 3,
          name: 'Event',
          path: '/microdata/event',
        },
        {
          id: 4,
          name: 'Music Album',
          path: '/microdata/music-album',
        },
        {
          id: 5,
          name: 'Music Playlist',
          path: '/microdata/music-playlist',
        },
        {
          id: 6,
          name: 'Organization',
          path: '/microdata/organization',
        },
        {
          id: 7,
          name: 'Person',
          path: '/microdata/person',
        },
        {
          id: 8,
          name: 'Product Offer',
          path: '/microdata/product-offer',
        },
        {
          id: 9,
          name: 'Recipe',
          path: '/microdata/recipe',
        },
        {
          id: 10,
          name: 'Restaurant',
          path: '/microdata/restaurant',
        },
        {
          id: 11,
          name: 'Review',
          path: '/microdata/review',
        },
        {
          id: 12,
          name: 'Software Application',
          path: '/microdata/software-application',
        },
        {
          id: 13,
          name: 'Video',
          path: '/microdata/video',
        },
      ],
    }
  }

  render() {
    return (
      <div>
        <Helmet>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        {/* GENERATORS YOU WILL LOVE SECITON */}
      
        <Row>
          <Row>
            <h1
              style={{
                wordBreak: 'break-word',
                textAlign: 'left',
                paddingRight: '5%',
                paddingLeft: '5%',
                fontSize: '20px',
              }}
            >
              There are plenty of generators out there,
              but OneGenerate really saves you time. Don't doubt it? Then explore OneGenerate generators,
              whether it's the HTML generators,the Open Graph generators or the many Wordpress generators, with just a few clicks you'll speed up your code development time.
            </h1>
          </Row>

          <h3 style={{ paddingTop: '60px', paddingBottom: '30px', fontWeight: '900' }}>
            Generators You Will Love
          </h3>
          <Row gutter={16} type="flex">
            {this.state.moduleItems.map((item, index) => {
              return (
                <Col xs={12} md={8} lg={6} span={6} key={index} style={{ height: '100%' }}>
                  <Link to={item.path}>
                    <Card
                      key={index}
                      style={{
                        height: '100%',
                        paddingLeft: '10px',
                        paddingRight: '10px',
                      }}
                      cover={
                        <img
                          alt="example"
                          src={item.image}
                          style={{ height: 100, objectFit: 'contain' }}
                        />
                      }
                    >
                      <div>
                        <h3
                          style={{
                            textAlign: 'center',
                            fontWeight: '600',
                            paddingBottom: '20px',
                          }}
                        >
                          {item.title}
                        </h3>
                        <h6>{item.description}</h6>
                      </div>
                    </Card>
                  </Link>
                </Col>
              )
            })}
          </Row>
        </Row>
        {/* WORDPRESS GENERATORS */}
        <Row style={{ paddingTop: '60px', paddingBottom: '30px' }}>
          <Col xs={6}>
            <h3 style={{ fontWeight: '900' }}>Wordpress Code Generator</h3>
          </Col>
          <Col xs={6} style={{ paddingTop: '4px' }}>
            <Link to="/wordpress">
              See All
              <Icon name="angle-right" style={{ paddingLeft: '10px' }} />
            </Link>
          </Col>
        </Row>
        <Row gutter={16} type="flex">
          {this.state.wordpressItems.map((item, index) => {
            return (
              <Col
                xs={12}
                md={8}
                lg={6}
                span={6}
                key={index}
                style={{
                  height: '100%',
                  paddingBottom: '20px',
                }}
              >
                <Link to={item.path}>
                  <Card
                    key={index}
                    style={{
                      height: '200px',
                      paddingLeft: '10px',
                      paddingRight: '10px',
                    }}
                  >
                    <h3
                      style={{
                        textAlign: 'center',
                        fontWeight: '600',
                        paddingBottom: '20px',
                        height: '70px',
                      }}
                    >
                      {item.name}
                    </h3>
                    <h6>{item.description}</h6>
                  </Card>
                </Link>
              </Col>
            )
          })}
        </Row>
        {/* OPEN GRAPH GENERATORS */}
        <Row style={{ paddingTop: '60px', paddingBottom: '30px' }}>
          <Col xs={6}>
            <h3 style={{ fontWeight: '900' }}>Open Graph Generator</h3>
          </Col>
          <Col xs={6} style={{ paddingTop: '4px' }}>
            <Link to="/open-graph">
              See All
              <Icon name="angle-right" style={{ paddingLeft: '10px' }} />
            </Link>
          </Col>
        </Row>
        <Row>
          <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={200}
            totalSlides={13}
            visibleSlides={4}
          >
            <Slider style={{ height: '100px' }}>
              {this.state.openGraphItems.map((item, index) => {
                return (
                  <Slide key={index} index={index} innerClassName={styles.carouselPadding}>
                    <Link to={item.path}>
                      <Card
                        style={{
                          paddingLeft: '10px',
                          paddingRight: '10px',
                        }}
                      >
                        <h3
                          style={{
                            textAlign: 'center',
                            fontWeight: '600',
                          }}
                        >
                          {item.name}
                        </h3>
                      </Card>
                    </Link>
                  </Slide>
                )
              })}
            </Slider>
          </CarouselProvider>
        </Row>
        {/* JSON-LD GENERATORS */}
        <Row style={{ paddingTop: '60px', paddingBottom: '30px' }}>
          <Col xs={6}>
            <h3 style={{ fontWeight: '900' }}>JSON-LD Generator</h3>
          </Col>
          <Col xs={6} style={{ paddingTop: '4px' }}>
            <Link to="/json-ld">
              See All
              <Icon name="angle-right" style={{ paddingLeft: '10px' }} />
            </Link>
          </Col>
        </Row>
        <Row>
          <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={200}
            totalSlides={13}
            visibleSlides={4}
          >
            <Slider style={{ height: '100px' }}>
              {this.state.jsonLdItems.map((item, index) => {
                return (
                  <Slide key={index} index={index} innerClassName={styles.carouselPadding}>
                    <Link to={item.path}>
                      <Card
                        style={{
                          paddingLeft: '10px',
                          paddingRight: '10px',
                        }}
                      >
                        <h3
                          style={{
                            textAlign: 'center',
                            fontWeight: '600',
                          }}
                        >
                          {item.name}
                        </h3>
                      </Card>
                    </Link>
                  </Slide>
                )
              })}
            </Slider>
          </CarouselProvider>
        </Row>
        {/* MICRODATA GENERATORS */}
        <Row style={{ paddingTop: '60px', paddingBottom: '30px' }}>
          <Col xs={6}>
            <h3 style={{ fontWeight: '900' }}>Microdata Generator</h3>
          </Col>
          <Col xs={6} style={{ paddingTop: '4px' }}>
            <Link to="/microdata">
              See All
              <Icon name="angle-right" style={{ paddingLeft: '10px' }} />
            </Link>
          </Col>
        </Row>

        <Row>
          <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={200}
            totalSlides={13}
            visibleSlides={4}
          >
            <Slider style={{ height: '100px' }}>
              {this.state.microdataItems.map((item, index) => {
                return (
                  <Slide key={index} index={index} innerClassName={styles.carouselPadding}>
                    <Link to={item.path}>
                      <Card
                        style={{
                          paddingLeft: '10px',
                          paddingRight: '10px',
                        }}
                      >
                        <h3
                          style={{
                            textAlign: 'center',
                            fontWeight: '600',
                          }}
                        >
                          {item.name}
                        </h3>
                      </Card>
                    </Link>
                  </Slide>
                )
              })}
            </Slider>
          </CarouselProvider>
        </Row>
      </div>
    )
  }
}
