import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import InputMask from 'react-input-mask'

const DurationInput = props => (
  <InputMask mask="99:99" value={props.value} onChange={props.onChange}>
    {inputProps => (
      <TextField {...inputProps} fullWidth variant="outlined" label="Duration" type="tel" />
    )}
  </InputMask>
)

class MusicPlaylist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'JSON-LD Music Playlist',
      helmetDescription: 'Generate code for JSON-LD music playlist',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      playlistName: '',
      playlistCoverImage: '',
      downloadListenUrl: '',
      genre: '',
      trackItems: [{ name: '', artist: '', duration: '', originalAlbum: '', url: '' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let playlistName = this.state.playlistName
    let playlistCoverImage = this.state.playlistCoverImage
    let downloadListenUrl = this.state.downloadListenUrl
    let genre = this.state.genre
    let trackItems = this.state.trackItems

    let genreString = ''
    if (genre) {
      genreString = `\n\t"genre": "${genre}",`
    }

    let codeString = `
    <script type="application/ld+json">
    {
    "@context": "http://schema.org/",
    "@type": "MusicPlaylist",
    "name": "${playlistName}",
    "image": "${playlistCoverImage}",
    "url": "${downloadListenUrl}",${genreString}
    "numtracks": "${trackItems.length}",
    "track": [${trackItems
      .map((item, index) => {
        return `\n\t 
        {
            "@type": "MusicRecording",
            "position": "${index}",
            "name": "${item.name}",
            "byArtist": {
            "@type": "MusicGroup",
            "name": "${item.artist}"
            },
            "inAlbum": {
                "@type": "MusicAlbum",
                "name": "${item.originalAlbum}"
            },
            "duration": "${item.duration}",
            "url": "${item.url}"
            },`
      })
      .join('')}
      ]
    }
    </script>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'JSONLD_Music_Playlist.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({ name: '', artist: '', duration: '', originalAlbum: '', url: '' })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleAddTrackItemClick = name => event => {
    let items = this.state.trackItems
    items.push({
      name: '',
      artist: '',
      duration: '',
      originalAlbum: '',
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Playlist Name"
                      type="text"
                      name="playlistName"
                      id=""
                      value={this.state.playlistName}
                      onChange={this.handleInput('playlistName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Playlist Cover Image (URL)"
                      type="text"
                      name="playlistCoverImage"
                      id=""
                      value={this.state.playlistCoverImage}
                      onChange={this.handleInput('playlistCoverImage')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Download / Listen URL"
                      type="text"
                      name="downloadListenUrl"
                      id=""
                      value={this.state.downloadListenUrl}
                      onChange={this.handleInput('downloadListenUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Genre"
                      type="text"
                      name="genre"
                      id=""
                      value={this.state.genre}
                      onChange={this.handleInput('genre')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.trackItems.map((item, index) => {
                      return (
                        <div key={index}>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <h6>{'Track ' + (index + 1)}</h6>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Name"
                                name="name"
                                id=""
                                value={item.name}
                                onChange={this.handleItem('trackItems', 'name', index)}
                              />
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Artist"
                                name="artist"
                                id=""
                                value={item.artist}
                                onChange={this.handleItem('trackItems', 'artist', index)}
                              />
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <DurationInput
                                value={this.state.duration}
                                onChange={this.handleItem('trackItems', 'duration', index)}
                              />
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Original Album"
                                name="originalAlbum"
                                id=""
                                value={item.originalAlbum}
                                onChange={this.handleItem('trackItems', 'originalAlbum', index)}
                              />
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Track URL"
                                name="url"
                                id=""
                                value={item.url}
                                onChange={this.handleItem('trackItems', 'url', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('trackItems')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default MusicPlaylist
