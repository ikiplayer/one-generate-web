import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import InputMask from 'react-input-mask'

const Input = props => (
  <InputMask mask="9999/99/99" value={props.value} onChange={props.onChange}>
    {inputProps => (
      <TextField {...inputProps} fullWidth variant="outlined" label="Date Published" type="tel" />
    )}
  </InputMask>
)

// function TextMaskCustom(props) {
//   const { inputRef, ...other } = props

//   return (
//     <MaskedInput
//       {...other}
//       ref={ref => {
//         inputRef(ref ? ref.inputElement : null)
//       }}
//       mask={[/[1-9]/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]}
//       placeholderChar={'\u2000'}
//       showMask
//     />
//   )
// }

class Article extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'JSON-LD Article',
      helmetDescription: 'Generate code for JSON-LD article',
      siteName: 'OneGenerate',
      textMask: '    -  -  ',
      isSnackbarOpen: false,
      headline: '',
      description: '',
      datePublished: '',
      imageUrl: '',
      imageHeight: '',
      imageWidth: '',
      author: '',
      publisher: '',
      publisherLogoUrl: '',
      fullArticleBody: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let headline = this.state.headline
    let description = this.state.description
    let datePublished = this.state.datePublished
    let imageUrl = this.state.imageUrl
    let imageHeight = this.state.imageHeight
    let imageWidth = this.state.imageWidth
    let author = this.state.author
    let publisher = this.state.publisher
    let publisherLogoUrl = this.state.publisherLogoUrl
    let fullArticleBody = this.state.fullArticleBody

    let codeString = `
    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "NewsArticle",
      "headline": "${headline}",
      "datePublished": "${datePublished}",
      "description": "${description}",
      "image": {
        "@type": "ImageObject",
        "height": "${imageHeight}",
        "width": "${imageWidth}",
        "url": "${imageUrl}"
      },
      "author": "${author}",
      "publisher": {
        "@type": "Organization",
        "logo": {
          "@type": "ImageObject",
          "url": "${publisherLogoUrl}"
        },
        "name": "${publisher}"
      },
      "articleBody": "${fullArticleBody}"
    }
    </script>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'JSONLD_Article.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Headline"
                      type="text"
                      name="headline"
                      id=""
                      value={this.state.headline}
                      onChange={this.handleInput('headline')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <Input
                      value={this.state.datePublished}
                      onChange={this.handleInput('datePublished')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Image URL"
                      type="text"
                      name="imageUrl"
                      id=""
                      value={this.state.imageUrl}
                      onChange={this.handleInput('imageUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Image Height (px)"
                      type="number"
                      name="imageHeight"
                      id=""
                      value={this.state.imageHeight}
                      onChange={this.handleInput('imageHeight')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Image Width (px)"
                      type="number"
                      name="imageWidth"
                      id=""
                      value={this.state.imageWidth}
                      onChange={this.handleInput('imageWidth')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Author"
                      type="text"
                      name="author"
                      id=""
                      value={this.state.author}
                      onChange={this.handleInput('author')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Publisher"
                      type="text"
                      name="publisher"
                      id=""
                      value={this.state.publisher}
                      onChange={this.handleInput('publisher')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Publisher Logo Url"
                      type="text"
                      name="publisherLogoUrl"
                      id=""
                      value={this.state.publisherLogoUrl}
                      onChange={this.handleInput('publisherLogoUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Full Article Body"
                      type="text"
                      name="fullArticleBody"
                      id=""
                      value={this.state.fullArticleBody}
                      onChange={this.handleInput('fullArticleBody')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24} />
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Article
