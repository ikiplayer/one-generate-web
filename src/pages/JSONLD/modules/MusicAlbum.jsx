import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import InputMask from 'react-input-mask'

const DurationInput = props => (
  <InputMask mask="99:99" value={props.value} onChange={props.onChange}>
    {inputProps => (
      <TextField {...inputProps} fullWidth variant="outlined" label="Duration" type="tel" />
    )}
  </InputMask>
)

class MusicAlbum extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'JSON-LD Music Album',
      helmetDescription: 'Generate code for JSON-LD music album',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      albumTitle: '',
      artistName: '',
      albumCoverImageUrl: '',
      downloadListenUrl: '',
      genre: '',
      trackItems: [{ name: '', duration: '', url: '' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let albumTitle = this.state.albumTitle
    let artistName = this.state.artistName
    let albumCoverImageUrl = this.state.albumCoverImageUrl
    let downloadListenUrl = this.state.downloadListenUrl
    let genre = this.state.genre
    let trackItems = this.state.trackItems

    let codeString = `
      <script type="application/ld+json">
      {
        "@context": "http://schema.org/",
        "@type": "MusicAlbum",
        "name": "${albumTitle}",
        "byArtist": {
          "@type": "MusicGroup",
          "name": "${artistName}"
        },
        "image": "${albumCoverImageUrl}",
        "url": "${downloadListenUrl}",
        "genre": "${genre}",
        "numtracks": "${trackItems.length}",
        "track": [${trackItems
          .map((item, index) => {
            return `\n\t 
          {
            "@type": "MusicRecording",
            "position": "${index + 1}",
            "name": "${item.name}",
            "url": "${item.url}",
            "duration": "${item.duration}"
          }
        `
          })
          .join('')}
      ]
    }
    </script>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'JSONLD_Music_Album.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      name: '',
      duration: '',
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleAddTrackItemClick = name => event => {
    let items = this.state[name]
    items.push({
      position: '',
      name: '',
      url: '',
      duration: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Album Title"
                      type="text"
                      name="albumTitle"
                      id=""
                      value={this.state.albumTitle}
                      onChange={this.handleInput('albumTitle')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Artist Name"
                      type="text"
                      name="artistName"
                      id=""
                      value={this.state.artistName}
                      onChange={this.handleInput('artistName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Album Cover Image Url"
                      type="text"
                      name="albumCoverImageUrl"
                      id=""
                      value={this.state.albumCoverImageUrl}
                      onChange={this.handleInput('albumCoverImageUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Download / Listen Url"
                      type="text"
                      name="downloadListenUrl"
                      id=""
                      value={this.state.downloadListenUrl}
                      onChange={this.handleInput('downloadListenUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Genre"
                      type="text"
                      name="genre"
                      id=""
                      value={this.state.genre}
                      onChange={this.handleInput('genre')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.trackItems.map((item, index) => {
                      return (
                        <div key={index}>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <h6>{'Track ' + (index + 1)}</h6>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Name"
                                name="name"
                                id=""
                                value={item.name}
                                onChange={this.handleItem('trackItems', 'name', index)}
                              />
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <DurationInput
                                value={this.state.duration}
                                onChange={this.handleItem('trackItems', 'duration', index)}
                              />
                            </Col>
                          </Row>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Track Url"
                                name="url"
                                id=""
                                value={item.url}
                                onChange={this.handleItem('trackItems', 'url', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('trackItems')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default MusicAlbum
