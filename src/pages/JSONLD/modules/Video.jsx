import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import InputMask from 'react-input-mask'

const MaskInput = props => (
  <InputMask mask={props.mask} value={props.value} onChange={props.onChange}>
    {inputProps => (
      <TextField
        {...inputProps}
        fullWidth
        variant="outlined"
        label={props.label}
        type={props.type ? props.type : 'tel'}
      />
    )}
  </InputMask>
)

class Video extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'JSON-LD video',
      helmetDescription: 'Generate code for JSON-LD video',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      videoName: '',
      videoUrl: '',
      description: '',
      thumbnailImageUrl: '',
      uploadDate: '',
      duration: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let videoName = this.state.videoName
    let videoUrl = this.state.videoUrl
    let description = this.state.description
    let thumbnailImageUrl = this.state.thumbnailImageUrl
    let uploadDate = this.state.uploadDate
    let duration = this.state.duration

    let codeString = `
    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "VideoObject",
      "name": "${videoName}",
      "contentUrl": "${videoUrl}",
      "duration": "${duration}",
      "thumbnailUrl": "${thumbnailImageUrl}",
      "uploadDate": "${uploadDate}",
      "description": "${description}"
    }
    </script>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'JSONLD_Video.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Video Name"
                      type="text"
                      name="videoName"
                      id=""
                      value={this.state.videoName}
                      onChange={this.handleInput('videoName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Video URL"
                      type="text"
                      name="videoUrl"
                      id=""
                      value={this.state.videoUrl}
                      onChange={this.handleInput('videoUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
              </Row>

              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Thumbnail Image Url"
                      type="text"
                      name="thumbnailImageUrl"
                      id=""
                      value={this.state.thumbnailImageUrl}
                      onChange={this.handleInput('thumbnailImageUrl')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <MaskInput
                      mask="9999-99-99"
                      label="Upload Date (yyyy-mm-dd)"
                      value={this.state.uploadDate}
                      onChange={this.handleInput('uploadDate')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <MaskInput
                      mask="99:99:99"
                      label="Duration (HH:MM:SS)"
                      value={this.state.duration}
                      onChange={this.handleInput('duration')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Video
