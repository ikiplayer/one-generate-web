import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'

import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import InputMask from 'react-input-mask'

const StartDateInput = props => (
  <InputMask mask="9999/99/99" value={props.value} onChange={props.onChange}>
    {inputProps => (
      <TextField {...inputProps} fullWidth variant="outlined" label="Start Date" type="tel" />
    )}
  </InputMask>
)

const EndDateInput = props => (
  <InputMask mask="9999/99/99" value={props.value} onChange={props.onChange}>
    {inputProps => (
      <TextField {...inputProps} fullWidth variant="outlined" label="End Date" type="tel" />
    )}
  </InputMask>
)

class Event extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'JSON-LD Event',
      helmetDescription: 'Generate code for JSON-LD event',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      eventName: '',
      description: '',
      startDate: '',
      endDate: '',
      nameOfLocation: '',
      streetAddress: '',
      city: '',
      stateProvinceRegion: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let eventName = this.state.eventName
    let description = this.state.description
    let startDate = this.state.startDate
    let endDate = this.state.endDate
    let nameOfLocation = this.state.nameOfLocation
    let streetAddress = this.state.streetAddress
    let city = this.state.city
    let stateProvinceRegion = this.state.stateProvinceRegion

    let codeString = `
    <div itemscope itemtype="http://schema.org/Event">
        <span itemprop="name">${eventName}</span><br>
        <span itemprop="description">${description}</span><br>
        When: <time itemprop="startDate" datetime="${startDate}"></time> - 
        <time itemprop="endDate" datetime="${endDate}"></time><br>
        <div itemprop="location" itemscope itemtype="http://schema.org/Place">
            Where: <span itemprop="name">${nameOfLocation}</span><br>
            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress">${streetAddress}</span><br>
                <span itemprop="addressLocality">${city}</span><br>
                <span itemprop="addressRegion">${stateProvinceRegion}</span> 
            </div>
        </div>
    </div>
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'JSONLD_Event.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Event Name"
                      type="text"
                      name="eventName"
                      id=""
                      value={this.state.eventName}
                      onChange={this.handleInput('eventName')}
                    />
                  </Form>
                </Col>
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <StartDateInput
                      value={this.state.datePublished}
                      onChange={this.handleInput('startDate')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <EndDateInput
                      value={this.state.datePublished}
                      onChange={this.handleInput('endDate')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Name Of Location"
                      type="text"
                      name="nameOfLocation"
                      id=""
                      value={this.state.nameOfLocation}
                      onChange={this.handleInput('nameOfLocation')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Street Address"
                      type="text"
                      name="streetAddress"
                      id=""
                      value={this.state.streetAddress}
                      onChange={this.handleInput('streetAddress')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="City"
                      type="text"
                      name="city"
                      id=""
                      value={this.state.city}
                      onChange={this.handleInput('city')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="State / Province / Region"
                      type="text"
                      name="stateProvinceRegion"
                      id=""
                      value={this.state.stateProvinceRegion}
                      onChange={this.handleInput('stateProvinceRegion')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Event
