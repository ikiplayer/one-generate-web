import React from 'react'
import { Button, Row, Col } from 'antd'
import { Helmet } from 'react-helmet'
// import PaymentCard from 'components/CleanUIComponents/PaymentCard'
// import PaymentAccount from 'components/CleanUIComponents/PaymentAccount'
// import PaymentTransaction from 'components/CleanUIComponents/PaymentTransaction'
// import ChartCard from 'components/CleanUIComponents/ChartCard'
// import Authorize from 'components/LayoutComponents/Authorize/index.jsx'
// import { tableData } from './data.json'
import { Link } from 'react-router-dom'

class JSONLDIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = { helmetTitle: '', helmetDescription: 'Generate code for bootstrap', siteName: 'OneGenerate',
      JSONLDItems: [
        {
          id: 0,
          name: 'Aggregate Offer',
          path: '/json-ld/aggregate-offer',
          description: 'Generate code for aggregate offers using JSON-LD',
        },
        {
          id: 1,
          name: 'Aggregate Rating',
          path: '/json-ld/aggregate-rating',
          description: 'Generate code for aggregate ratings using JSON-LD',
        },
        {
          id: 2,
          name: 'Article',
          path: '/json-ld/article',
          description: 'Generate code for articles using JSON-LD',
        },
        {
          id: 3,
          name: 'Event',
          path: '/json-ld/event',
          description: 'Generate code for events using JSON-LD',
        },
        {
          id: 4,
          name: 'Music Album',
          path: '/json-ld/music-album',
          description: 'Generate code for music album using JSON-LD',
        },
        {
          id: 5,
          name: 'Music Playlist',
          path: '/json-ld/music-playlist',
          description: 'Generate code for music playlist using JSON-LD',
        },
        {
          id: 6,
          name: 'Organization',
          path: '/json-ld/organization',
          description: 'Generate code for organization using JSON-LD',
        },
        {
          id: 7,
          name: 'Person',
          path: '/json-ld/person',
          description: 'Generate code for person  using JSON-LD',
        },
        {
          id: 8,
          name: 'Product Offer',
          path: '/json-ld/product-offer',
          description: 'Generate code for product offers using JSON-LD',
        },
        {
          id: 9,
          name: 'Recipe',
          path: '/json-ld/recipe',
          description: 'Generate code for recipe using JSON-LD',
        },
        {
          id: 10,
          name: 'Restaurant',
          path: '/json-ld/restaurant',
          description: 'Generate code for restaurant using JSON-LD',
        },
        {
          id: 11,
          name: 'Review',
          path: '/json-ld/review',
          description: 'Generate code for review using JSON-LD',
        },
        {
          id: 12,
          name: 'Software Application',
          path: '/json-ld/software-application',
          description: 'Generate code for software application using JSON-LD',
        },
        {
          id: 13,
          name: 'Video',
          path: '/json-ld/video',
          description: 'Generate code for video using JSON-LD',
        },
      ],
    }
  }

  linkTo = path => event => {
    window.location.href = path
  }

  render() {
    return (
      <div>
        <Helmet title="JSON-LD" />
        <Row gutter={16}>
          {this.state.JSONLDItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{ fontSize: 20, marginTop: 20, whiteSpace: 'unset', height: 100 }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
      </div>
    )
  }
}

export default JSONLDIndex
