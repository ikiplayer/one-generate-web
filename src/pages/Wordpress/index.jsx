import React from 'react'
import { Button, Row, Col, Card, Icon } from 'antd'
import { Helmet } from 'react-helmet'
// import PaymentCard from 'components/CleanUIComponents/PaymentCard'
// import PaymentAccount from 'components/CleanUIComponents/PaymentAccount'
// import PaymentTransaction from 'components/CleanUIComponents/PaymentTransaction'
// import ChartCard from 'components/CleanUIComponents/ChartCard'
// import Authorize from 'components/LayoutComponents/Authorize/index.jsx'
import { Link } from 'react-router-dom'
const { Meta } = Card

// import { tableData } from './data.json'

class WordpressIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = { helmetTitle: '', helmetDescription: 'Generate code for bootstrap', siteName: 'OneGenerate',
      wordpressItems: [
        {
          id: 0,
          name: 'Cron Job Event',
          path: '/wordpress/cron-job-event',
          image: 'resources/images/wordpress/wordpress-1.jpg',
        },
        {
          id: 1,
          name: 'Custom Post Generator',
          path: '/wordpress/custom-post-generator',
          image: 'resources/images/wordpress/wordpress-2.jpg',
        },
        {
          id: 2,
          name: 'Dashboard Widgets Generator',
          path: '/wordpress/dashboard-widgets-generator',
          image: 'resources/images/wordpress/wordpress-3.jpg',
        },
        // TODO
        // { id: 3, name: 'Login Form Generator', path: '/wordpress/login-form-generator' },
        // TODO
        // { id: 4, name: 'Meta Box Generator', path: '/wordpress/meta-box-generator' },
        {
          id: 5,
          name: 'Menu Generator',
          path: '/wordpress/menu-generator',
          image: 'resources/images/wordpress/wordpress-4.jpg',
        },
        {
          id: 6,
          name: 'Post Status Generator',
          path: '/wordpress/post-status-generator',
          image: 'resources/images/wordpress/wordpress-5.jpg',
        },
        {
          id: 7,
          name: 'Quicktag for Classic Editor Generator',
          path: '/wordpress/quicktag-for-classic-editor-generator',
          image: 'resources/images/wordpress/wordpress-6.jpg',
        },
        {
          id: 8,
          name: 'Register / Enqueue Scripts (JS)',
          path: '/wordpress/register-enqueue-scripts-js',
          image: 'resources/images/wordpress/wordpress-7.jpg',
        },
        {
          id: 9,
          name: 'Register / Enqueue Styles (CSS)',
          path: '/wordpress/register-enqueue-styles-css',
          image: 'resources/images/wordpress/wordpress-8.jpg',
        },
        // TODO
        // { id: 10, name: 'Register Image Size', path: '/wordpress/register-image-size' },
        // TODO
        // {
        //   id: 11,
        //   name: 'Settings / Options Page Generator',
        //   path: '/wordpress/settings-options-page-generator',
        // },
        {
          id: 12,
          name: 'Shortcode Generator',
          path: '/wordpress/shortcode-generator',
          image: 'resources/images/wordpress/wordpress-9.jpg',
        },
        {
          id: 13,
          name: 'Sidebar Generator',
          path: '/wordpress/sidebar-generator',
          image: 'resources/images/wordpress/wordpress-10.jpg',
        },
        {
          id: 14,
          name: 'Tax Query Generator',
          path: '/wordpress/tax-query-generator',
          image: 'resources/images/wordpress/wordpress-11.jpg',
        },
        // TODO
        // { id: 15, name: 'Term Meta Generator', path: '/wordpress/term-meta-generator' },
        {
          id: 16,
          name: 'User Contact Methods Generator',
          path: '/wordpress/user-contact-methods-generator',
          image: 'resources/images/wordpress/wordpress-12.jpg',
        },
        {
          id: 17,
          name: 'Visual Composer Elementor Generator',
          path: '/wordpress/visual-composer-elementor-generator',
          image: 'resources/images/wordpress/wordpress-13.jpg',
        },
        {
          id: 18,
          name: 'Widget Generator',
          path: '/wordpress/widget-generator',
          image: 'resources/images/wordpress/wordpress-14.jpg',
        },
        {
          id: 19,
          name: 'WP Mail Function Generator',
          path: '/wordpress/wp-mail-function-generator',
          image: 'resources/images/wordpress/wordpress-15.jpg',
        },
        {
          id: 20,
          name: 'WP Query Loop Generator',
          path: '/wordpress/wp-query-loop-generator',
          image: 'resources/images/wordpress/wordpress-16.jpg',
        },
      ],
    }
  }

  linkTo = path => event => {
    window.location.href = path
  }

  render() {
    return (
      <div>
        <Helmet title="Wordpress" />
        <Row gutter={16}>
          {this.state.wordpressItems.map(item => {
            return (
              <Col span={6} xs={12} md={8} lg={6} key={item.id}>
                <Button
                  value="large"
                  block={true}
                  style={{ fontSize: 20, marginTop: 20, whiteSpace: 'unset', height: 100 }}
                >
                  <Link to={item.path}>{item.name}</Link>

                  {/* {item.name} */}
                </Button>
              </Col>
            )
          })}
        </Row>
      </div>
    )
  }
}

export default WordpressIndex
