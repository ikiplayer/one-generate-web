import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import { TextField, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'

// import { Label } from 'reactstrap'
import { Button, Divider, Snackbar, SnackbarContent, Toolbar } from '@material-ui/core'
import copy from 'copy-to-clipboard'
import { AddBox } from '@material-ui/icons'
import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class TaxQuery extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Tax Query',
      helmetDescription: 'Generate code for Wordpress tax query',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      queryName: '',
      relation: 'AND',
      items: [
        {
          taxonomySlug: '',
          terms: '',
          termField: 'Term ID',
          includeChildren: 'Yes',
          operator: 'IN',
        },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.queryName !== prevState.queryName ||
      this.state.relation !== prevState.relation
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let queryName = this.state.queryName
    let relation = this.state.relation
    let items = this.state.items

    let queryNameString = ''
    if (queryName === '') {
      queryNameString = 'tax_query'
    } else {
      queryNameString = queryName
    }

    let codeString = `
    // Custom Taxonomy Query
    $${queryNameString}= array(
      'relation' => '${relation}',${items.map(item => {
      let taxonomySlugString = ''
      if (item.taxonomySlug) {
        taxonomySlugString = `\n\t\t  'taxonomy' => '${item.taxonomySlug}'`
      }

      let termsString = ''
      if (item.terms) {
        termsString = `\n\t\t  'terms' => array('${item.terms}')`
      }

      let termFieldString = 'term_id'
      if (item.termField === 'Term ID') {
        termFieldString = 'term_id'
      } else if (item.termField === 'Term Slug') {
        termFieldString = 'slug'
      } else if (item.termField === 'Term Name') {
        termFieldString = 'name'
      } else if (item.termField === 'Term Taxonomy ID') {
        termFieldString = 'term_taxonomy_id'
      }

      let includeChildrenString = 'true'
      if (item.includeChildren === 'Yes') {
        includeChildrenString = 'true'
      } else {
        includeChildrenString = 'false'
      }

      return `
        array(${taxonomySlugString}${termsString}
          'field' => '${termFieldString}',
          'include_children' => ${includeChildrenString},
          'operator' => '${item.operator}',
        )`
    })}
    
    );
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Tax_Query_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()
    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      taxonomySlug: '',
      terms: '',
      termField: 'Term ID',
      includeChildren: 'Yes',
      operator: 'IN',
    })

    this.setState(
      {
        items: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Query Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Query Name"
                      type="text"
                      name="Query Name"
                      id=""
                      value={this.state.queryName}
                      onChange={this.handleInput('queryName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="exampleEmail">Relation</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Relation"
                      type="select"
                      select
                      value={this.state.relation}
                      onChange={this.handleInput('relation')}
                    >
                      <MenuItem key="0" value="AND">
                        AND
                      </MenuItem>
                      <MenuItem key="1" value="OR">
                        OR
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Taxonomy Parameters</Label> */}
                  {this.state.items.map((item, index) => {
                    return (
                      <div key={index}>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Taxonomy Slug (Key)</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Taxonomy Slug (Key)"
                                type="text"
                                name="taxonomySlug"
                                id=""
                                value={item.taxonomySlug}
                                onChange={this.handleItem('taxonomySlug', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Term(s)</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Term(s)"
                                type="text"
                                name="terms"
                                id=""
                                value={item.terms}
                                onChange={this.handleItem('terms', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Term(s)</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Term(s)"
                                select
                                type="select"
                                name="termField"
                                id=""
                                value={item.termField}
                                onChange={this.handleItem('termField', index)}
                              >
                                <MenuItem key="0" value="Term ID">
                                  Term ID
                                </MenuItem>
                                <MenuItem key="1" value="Term Slug">
                                  Term Slug
                                </MenuItem>
                                <MenuItem key="2" value="Term Name">
                                  Term Name
                                </MenuItem>
                                <MenuItem key="3" value="Term Taxonomy">
                                  Term Taxonomy ID
                                </MenuItem>
                              </TextField>
                            </Form>
                          </Col>
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Include Children</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Include Children"
                                type="select"
                                select
                                name="includeChildren"
                                id=""
                                value={item.includeChildren}
                                onChange={this.handleItem('includeChildren', index)}
                              >
                                <MenuItem key="0" value="Yes">
                                  Yes
                                </MenuItem>
                                <MenuItem key="1" value="No">
                                  No
                                </MenuItem>
                              </TextField>
                            </Form>
                          </Col>
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Operator</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Operator"
                                type="select"
                                select
                                name="operator"
                                id=""
                                value={item.operator}
                                onChange={this.handleItem('operator', index)}
                              >
                                <MenuItem key="0" value="IN">
                                  IN
                                </MenuItem>
                                <MenuItem key="1" value="NOT IN">
                                  NOT IN
                                </MenuItem>
                                <MenuItem key="2" value="AND">
                                  AND
                                </MenuItem>
                                <MenuItem key="3" value="EXISTS">
                                  EXISTS
                                </MenuItem>
                                <MenuItem key="4" value="NOT EXISTS">
                                  NOT EXISTS
                                </MenuItem>
                              </TextField>
                            </Form>
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default TaxQuery
