import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'

// import { Label } from 'reactstrap'
import { MenuItem, TextField, Snackbar, SnackbarContent, Toolbar, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import ChipInput from 'material-ui-chip-input'
import copy from 'copy-to-clipboard'
import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class WpMailFunction extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Wp Mail Function',
      helmetDescription: 'Generate code for wordpress wp mail function',
      siteName: 'OneGenerate',
      contentTypeOfTheEmail: '',
      subject: '',
      fromName: '',
      fromEmail: '',
      to: [],
      cc: [],
      bcc: [],
      replyToEmail: '',
      codeItem: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.contentTypeOfTheEmail !== prevState.contentTypeOfTheEmail ||
      this.state.subject !== prevState.subject ||
      this.state.fromName !== prevState.fromName ||
      this.state.fromEmail !== prevState.fromEmail ||
      this.state.to !== prevState.to ||
      this.state.cc !== prevState.cc ||
      this.state.bcc !== prevState.bcc ||
      this.state.replyToEmail !== prevState.replyToEmail
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let contentTypeOfTheEmail = this.state.contentTypeOfTheEmail
    let subject = this.state.subject
    let fromName = this.state.fromName
    let fromEmail = this.state.fromEmail
    let to = this.state.to
    let cc = this.state.cc
    let bcc = this.state.bcc
    let replyToEmail = this.state.replyToEmail

    let contentTypeOfTheEmailString = ''
    if (contentTypeOfTheEmail === 'HTML') {
      contentTypeOfTheEmailString = `\n\t$headers[] = 'Content-Type: text/html; charset=UTF-8';`
    }

    let fromNameString = ''
    if (fromName) {
      fromNameString = `\n\t$headers[] = 'From: ${fromName}';`
    }

    const toLength = to.length
    let toString = ''
    to.map((item, i) => {
      if (toLength === i + 1) {
        // console.log(item, i, toLength);
        return (toString += `'${item}'`)
      } else {
        return (toString += `'${item}',`)
      }
    })

    const ccLength = cc.length
    let ccString = ''
    cc.map((item, i) => {
      if (ccLength === i + 1) {
        // console.log(item, i, ccLength);
        return (ccString += `'${item}'`)
      } else {
        return (ccString += `'${item}',`)
      }
    })

    const bccLength = bcc.length
    let bccString = ''
    bcc.map((item, i) => {
      if (bccLength === i + 1) {
        // console.log(item, i, bccLength);
        return (bccString += `'${item}'`)
      } else {
        return (bccString += `'${item}',`)
      }
    })

    let replyToEmailString = ''
    if (replyToEmail) {
      replyToEmailString = `\n\t$headers[] = 'Reply-To: <${replyToEmail}>';`
    }

    let codeString = `
    // Custom WP Mail Function
    $to = ${to.length > 0 ? `array(${toString})` : ''}
    $subject = '${subject}';${contentTypeOfTheEmailString}${fromNameString}${
      bcc.length > 0 ? `\n\t$header[] = 'Bcc: ${bccString}';` : ''
    }${cc.length > 0 ? `\n\t$header[] = 'Cc: ${ccString}';` : ''}${replyToEmailString}
    $message = 'Your message';

    wp_mail( $to, $subject, $message );
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_WP_Mail_Function_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleAddChip(chip, state, key) {
    let stateArray = state
    // console.log(chip);

    stateArray.push(chip.replace(/[^a-zA-Z\d:]/gi, ''))

    this.setState(
      {
        [key]: stateArray,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleDeleteChip(chip, index, state, key) {
    console.log(chip, index)

    let stateArray = state
    if (index > -1) {
      stateArray.splice(index, 1)
    }

    this.setState(
      {
        [key]: stateArray,
      },
      () => this.updateCodeString(),
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="exampleEmail">Content-Type of the E-Mail</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Content-Type of the E-mail"
                      type="select"
                      select
                      value={this.state.contentTypeOfTheEmail}
                      onChange={this.handleInput('contentTypeOfTheEmail')}
                    >
                      <MenuItem key="0" value="Plain Text">
                        Plain Text
                      </MenuItem>
                      <MenuItem key="1" value="HTML">
                        HTML
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12} className=" ">
                  <Form>
                    {/* <Label for="">Subject</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Subject"
                      type="text"
                      name="Subject"
                      id=""
                      value={this.state.subject}
                      onChange={this.handleInput('subject')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12} className=" ">
                  <Form>
                    {/* <Label for="">From Name:</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="From Name:"
                      type="text"
                      name="From Name"
                      id=""
                      value={this.state.fromName}
                      onChange={this.handleInput('fromName')}
                    />
                  </Form>
                </Col>
                <Col md={12} className=" ">
                  <Form>
                    {/* <Label for="">From Email:</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="From Email:"
                      type="text"
                      name="From Email"
                      id=""
                      value={this.state.fromEmail}
                      onChange={this.handleInput('fromEmail')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="functionName">To:</Label> */}
                    <ChipInput
                      fullWidthInput
                      label="To:"
                      variant="outlined"
                      fullWidth
                      value={this.state.to}
                      onAdd={chip => this.handleAddChip(chip, this.state.to, 'to')}
                      onDelete={(chip, index) =>
                        this.handleDeleteChip(chip, index, this.state.to, 'to')
                      }
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="functionName">CC: </Label> */}
                    <ChipInput
                      fullWidthInput
                      variant="outlined"
                      fullWidth
                      label="CC:"
                      value={this.state.cc}
                      onAdd={chip => this.handleAddChip(chip, this.state.cc, 'cc')}
                      onDelete={(chip, index) =>
                        this.handleDeleteChip(chip, index, this.state.cc, 'cc')
                      }
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="functionName">BCC: </Label> */}
                    <ChipInput
                      fullWidthInput
                      variant="outlined"
                      fullWidth
                      label="BCC:"
                      value={this.state.bcc}
                      onAdd={chip => this.handleAddChip(chip, this.state.bcc, 'bcc')}
                      onDelete={(chip, index) =>
                        this.handleDeleteChip(chip, index, this.state.bcc, 'bcc')
                      }
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Reply to Email:</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Replay to Email:"
                      type="text"
                      name="From Email"
                      id=""
                      value={this.state.replyToEmail}
                      onChange={this.handleInput('replyToEmail')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                  </Button>
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default WpMailFunction
