import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
// import { Label } from 'reactstrap'
import {
  Switch,
  FormControlLabel,
  Button,
  Divider,
  MenuItem,
  Snackbar,
  SnackbarContent,
  Toolbar,
  TextField,
} from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Shortcode extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Shortcode',
      helmetDescription: 'Generate code for Wordpress shortcode',
      siteName: 'OneGenerate',
      shortcodeTagName: '',
      type: '',
      hasAttribute: true,
      items: [{ name: '', defaultValue: '' }],
      codeItem: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.shortcodeTagName !== prevState.shortcodeTagName ||
      this.state.type !== prevState.type ||
      this.state.hasAttribute !== prevState.hasAttribute
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let shortcodeTagName = this.state.shortcodeTagName
    let type = this.state.type
    let hasAttribute = this.state.hasAttribute
    let items = this.state.items

    let shortcodeTagNameString = ''
    if (shortcodeTagName === '') {
      shortcodeTagNameString = 'new_shortcode'
    } else {
      shortcodeTagNameString = shortcodeTagName
    }

    let typeString = ''
    if (type === 'Enclosing') {
      typeString = ', $content = null'
    } else {
      typeString = ''
    }

    let hasAttributeString = ''
    if (hasAttribute) {
      hasAttributeString = `
    $atts = shortcode_atts(
      array(${items.map(item => {
        if (item.name) {
          return `'\n\t\t ${item.name}' => '${item.defaultValue}',`
        }
        return ''
      })}
      ),
      $atts,
      '${shortcodeTagNameString}'
    );
    ${items.map(item => {
      if (item.name) {
        return `\n\t $${item.name} = $atts['${item.name}'];`
      }
      return ''
    })}
      `
    }
    let codeString = `
    // Create Shortcode ${shortcodeTagNameString}
    // Shortcode: [${shortcodeTagNameString}]
    function create_${shortcodeTagName}_shortcode($atts${typeString}) {
      ${hasAttributeString}
    }
    add_shortcode( '${shortcodeTagNameString}', 'create__shortcode' );
        
    `

    this.setState({
      codeItem: codeString,
    })
  }
  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Shortcode_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()
    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      name: '',
      defaultValue: '',
    })

    this.setState({
      items: items,
    })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Shortcode Tag Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Shortcode Tag Name"
                      type="text"
                      name="shortcodeTagName"
                      id=""
                      value={this.state.shortcodeTagName}
                      onChange={this.handleInput('shortcodeTagName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={16}>
                  <Form>
                    {/* <Label for="exampleEmail">Type</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Type"
                      type="select"
                      select
                      value={this.state.type}
                      onChange={this.handleInput('type')}
                    >
                      <MenuItem key="0" value="Self-Closing">
                        Self-Closing
                      </MenuItem>
                      <MenuItem key="1" value="Enclosing">
                        Enclosing
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.hasAttribute}
                        onChange={this.handleCheckbox('hasAttribute')}
                        value="hasAttribute"
                      />
                    }
                    label="Has Attributes?"
                  >
                    {/* <Label>Hierarchical</Label> */}
                  </FormControlLabel>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Attributes</Label> */}
                  {this.state.items.map((item, index) => {
                    return (
                      <div>
                        <Row key={index} gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Name</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Name"
                                type="text"
                                name="name"
                                id=""
                                value={item.name}
                                onChange={this.handleItem('name', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Default Value</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Default Value"
                                type="text"
                                name="defaultValue"
                                id=""
                                value={item.defaultValue}
                                onChange={this.handleItem('defaultValue', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Shortcode
