import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import { TextField, MenuItem } from '@material-ui/core'
import { Helmet } from 'react-helmet'

// import { Label } from 'reactstrap'
import { Switch, FormControlLabel, Button, Divider } from '@material-ui/core'
import { AddBox } from '@material-ui/icons'

import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class VisualComposerElement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Visual Composer Element',
      helmetDescription: 'Generate code for wordpress visual composer element',
      siteName: 'OneGenerate',
      elementName: '',
      shortcodeTag: '',
      description: '',
      category: '',
      classInBackend: '',
      textDomain: '',
      icon: '',
      createShortcodeFunction: false,
      showSettingsOnCreate: false,
      hasAttributes: false,
      items: [
        {
          type: '',
          showAdminLabel: 'Yes',
          heading: '',
          parameterName: '',
          defaultValue: '',
          description: '',
        },
      ],
      codeItem: '',
      typeOptions: [
        { id: 0, name: 'textfield' },
        { id: 1, name: 'textarea' },
        { id: 2, name: 'textarea_html' },
        { id: 3, name: 'dropdown' },
        { id: 4, name: 'attach_image' },
        { id: 5, name: 'attach_images' },
        { id: 6, name: 'posttypes' },
        { id: 7, name: 'colorpicker' },
        { id: 8, name: 'exploded_textarea' },
        { id: 9, name: 'widgetised_sidebars' },
        { id: 10, name: 'textarea_raw_html' },
        { id: 11, name: 'vc_link' },
        { id: 12, name: 'checkbox' },
        { id: 13, name: 'loop' },
        { id: 14, name: 'css' },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.elementName !== prevState.elementName ||
      this.state.shortcodeTag !== prevState.shortcodeTag ||
      this.state.description !== prevState.description ||
      this.state.category !== prevState.category ||
      this.state.classInBackend !== prevState.classInBackend ||
      this.state.textDomain !== prevState.textDomain ||
      this.state.icon !== prevState.icon ||
      this.state.createShortcodeFunction !== prevState.createShortcodeFunction ||
      this.state.showSettingsOnCreate !== prevState.showSettingsOnCreate ||
      this.state.hasAttributes !== prevState.hasAttributes
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let elementName = this.state.elementName
    let shortcodeTag = this.state.shortcodeTag
    let description = this.state.description
    let category = this.state.category
    let classInBackend = this.state.classInBackend
    let textDomain = this.state.textDomain
    let icon = this.state.icon
    let createShortcodeFunction = this.state.createShortcodeFunction
    let showSettingsOnCreate = this.state.showSettingsOnCreate
    let hasAttributes = this.state.hasAttributes
    let items = this.state.items

    let elementNameString = ''
    if (elementName === '') {
      elementNameString = 'new_shortcode'
    } else {
      elementNameString = elementName
    }

    let shortcodeTagString = ''
    if (shortcodeTag === '') {
      shortcodeTagString = 'new_shortcode'
    } else {
      shortcodeTagString = shortcodeTag
    }

    let descriptionString = ''
    if (description) {
      descriptionString = `\n\t\t\t'description' => __( '${description}', '${textDomain}' ),`
    }

    let categoryString = ''
    if (category === '') {
      categoryString = 'Content'
    } else {
      categoryString = category
    }

    let classInBackendString = ''
    if (classInBackend === '') {
      classInBackendString = `\n\t'class' => '${classInBackend}',`
    }

    let textDomainString = ''
    if (textDomain === '') {
      textDomain = 'textdomain'
    } else {
      textDomainString = textDomain
    }

    let iconString = ''
    if (icon) {
      iconString = `\n\t\t\t'icon' => '${icon}'`
    }

    let hasAttributesString = ''
    if (hasAttributes) {
      hasAttributesString = `
      	// Attributes
      $atts = shortcode_atts(
        array(
          ${items.map(item => {
            return `'${item.parameterName}' => '${item.defaultValue}',`
          })}
        ),
        $atts,
        '${shortcodeTagString}'
      );
      // Attributes in var
      ${items.map(item => {
        return `$${item.parameterName} = $atts['${item.parameterName}']`
      })}
      `
    }

    let createShortcodeFunctionString = ''
    if (createShortcodeFunction) {
      createShortcodeFunctionString = `
      // Create Shortcode ${shortcodeTagString}
      // Use the shortcode: [${shortcodeTagString}]
      function create__shortcode(${hasAttributes ? '$atts' : ''}) {
        ${
          items.length > 0
            ? `
        // Attributes
        $atts = shortcode_atts(
          array(
            ${items.map(item => {
              return `${item.parameterName} => ${item.defaultValue ? item.defaultValue : ''},`
            })}
          ),
          $atts,
          'new_shortcode'
        );
        // Attributes in var
       ${
         items.length > 0
           ? `${items.map(item => {
               return `$${item.parameterName} = $atts['${item.parameterName}']`
             })}`
           : ''
       }
        `
            : ''
        }
        // Attributes
        $atts = shortcode_atts(
          array(
            '' => '',
          ),
          $atts,
          '${shortcodeTagString}'
        );
        // Attributes in var
        $ = $atts[''];


        // Output Code
        $output = 'Here you can write';
        $output .= 'your code.';

        return $output;
      }
      add_shortcode( '${shortcodeTagString}', 'create__shortcode' );

      `
    }

    let codeString = `

    // Create ${elementName} element for Visual Composer
    add_action( 'vc_before_init', '_integrateWithVC' );
    function ${shortcodeTagString}_integrateWithVC() {
      vc_map( array(
        'name' => __( '${elementName}', '${textDomainString}' ),${descriptionString}
        'base' => '${shortcodeTagString}',${classInBackend}
        'show_settings_on_create' => ${showSettingsOnCreate},
        'category' => __( '${categoryString}', '${textDomainString}'),
        'params' => array(
          ${items.map(item => {
            let defaultValueString = ''
            if (item.defaultValue) {
              defaultValueString = `\n\t'value' => '${item.defaultValue}',`
            }

            let descriptionItemString = ''
            if (item.description) {
              descriptionItemString = `\n\t'description' => __( '${
                item.description
              }', '${textDomainString}' )`
            }
            return `
             array(
            'type' => '${item.type}',
            'holder' => 'div',
            'class' => '',
            'admin_label' => ${item.showInAdminLabel === 'No' ? 'false' : 'true'},
            'heading' => __( '${item.heading}', '${textDomainString}' ),
            'param_name' => '${
              item.parameterName ? item.parameterName : ''
            }',${defaultValueString}${descriptionItemString}
          ),
            `
          })}
         
        )
	    ));
      }
   
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()
    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      type: '',
      showAdminLabel: 'Yes',
      heading: '',
      parameterName: '',
      defaultValue: '',
      description: '',
    })

    this.setState({
      items: items,
    })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Element Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Element Name"
                      type="text"
                      name="Element Name"
                      id=""
                      value={this.state.elementName}
                      onChange={this.handleInput('elementName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Shortcode Tag</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Shortcode Tag"
                      type="text"
                      name="Shortcode Tag"
                      id=""
                      value={this.state.shortcodeTag}
                      onChange={this.handleInput('shortcodeTag')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Description</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="Description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Category</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Category"
                      type="text"
                      name="Category"
                      id=""
                      value={this.state.category}
                      onChange={this.handleInput('category')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Class in Backend</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Class in Backend"
                      type="text"
                      name="classInBackend"
                      id=""
                      value={this.state.classInBackend}
                      onChange={this.handleInput('classInBackend')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Domain"
                      type="text"
                      name="Text Domain"
                      id=""
                      value={this.state.textDomain}
                      onChange={this.handleInput('textDomain')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Icon</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Icon"
                      type="text"
                      name="Icon"
                      id=""
                      value={this.state.icon}
                      onChange={this.handleInput('icon')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.createShortcodeFunction}
                        onChange={this.handleCheckbox('createShortcodeFunction')}
                        value="createShortcodeFunction"
                      />
                    }
                    label="Create shortcode function?"
                  >
                    {/* <Label>Hierarchical</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showSettingsOnCreate}
                        onChange={this.handleCheckbox('showSettingsOnCreate')}
                        value="showSettingsOnCreate"
                      />
                    }
                    label="Create settings on create?"
                  />
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.hasAttribues}
                        onChange={this.handleCheckbox('hasAttribues')}
                        value="hasAttribues"
                      />
                    }
                    label="Has Attributes / Parameters?"
                  />
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Attributes</Label> */}
                  {this.state.items.map((item, index) => {
                    return (
                      <div>
                        <Row gutter={8} key={index} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Type</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Type"
                                select
                                type="select"
                                name="type"
                                id=""
                                multiple={false}
                                value={item.type}
                                onChange={this.handleItem('type', index)}
                              >
                                {this.state.typeOptions.map((item, index1) => {
                                  return <MenuItem key={index1}>{item.name}</MenuItem>
                                })}
                              </TextField>
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Show admin label?</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Show admin label?"
                                select
                                type="select"
                                name="showInAdminLabel"
                                id=""
                                value={item.showInAdminLabel}
                                onChange={this.handleItem('showInAdminLabel', index)}
                              >
                                <MenuItem key="0" value="Yes">
                                  Yes
                                </MenuItem>
                                <MenuItem key="0" value="No">
                                  No
                                </MenuItem>
                              </TextField>
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Heading</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Heading"
                                type="text"
                                name="heading"
                                id=""
                                value={item.heading}
                                onChange={this.handleItem('heading', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">parameterName</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Parameter Name"
                                type="text"
                                name="parameterName"
                                id=""
                                value={item.parameterName}
                                onChange={this.handleItem('parameterName', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Default Value</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Default Value"
                                type="text"
                                name="defaultValue"
                                id=""
                                value={item.defaultValue}
                                onChange={this.handleItem('defaultValue', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Description</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Descriptoin"
                                type="text"
                                name="description"
                                id=""
                                value={item.description}
                                onChange={this.handleItem('description', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={5}>
            <AceEditor
              width="100%"
              mode="php"
              theme="vibrant_ink"
              name="editor"
              readOnly
              wrapEnabled
              value={this.state.codeItem}
              editorProps={{ $blockScrolling: true }}
              onLoad={editor => {
                editor.focus()
                editor.getSession().setUseWrapMode(true)
              }}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export default VisualComposerElement
