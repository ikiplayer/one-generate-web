import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, MenuItem, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'
import AdSense from 'react-adsense'

// var editor = ace.edit("editor");
// editor.getSession().setMode("ace/mode/javascript");
// editor.setTheme("ace/theme/eclipse");

class CronJobEvent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Cron Job Event',
      helmetDescription: 'Generate code for Wordpress Cron Job Event',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      functionName: '',
      timeStamp: 'GMT Time',
      recurrenceInterval: 'Once Hourly',
      customName: '',
      customLabel: '',
      customInterval: '',
      codeItem: '',
    }
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle} />
          <meta
            property="og:description"
            content={this.state.helmetDescription + ' - ' + this.state.siteName}
          />

          <meta property="og:site_name" content={this.state.siteName} />
          <meta property="og:url" content={window.location.href} />
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Helmet title="Cron Job Event" />
        <Row gutter={8}>
          <Col md={24} lg={14}>
            <Card>
              <Row gutter={16} className="m-4">
                <Col xs={24} md={24} lg={8} className=" col-xs-6 ">
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Function Name"
                      type="text"
                      name="functionName"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleFunctionName.bind(this)}
                    />
                  </Form>
                </Col>
                <Col xs={24} md={24} lg={8} className=" col-xs-6 ">
                  <Form>
                    {/* <Label for="exampleEmail">Timestamp</Label> */}
                    <TextField
                      placeholder=" "
                      select
                      fullWidth
                      label="Timestamp"
                      variant="outlined"
                      value={this.state.timeStamp}
                      onChange={this.handleTimeStamp.bind(this)}
                    >
                      <MenuItem key="0" value="GMT Time">
                        GMT Time
                      </MenuItem>
                      <MenuItem key="1" value="Local Time">
                        Local Time
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col xs={24} md={24} lg={8} className=" col-xs-6 ">
                  <Form>
                    {/* <Label for="exampleEmail">Recurrence Interval</Label> */}
                    <TextField
                      placeholder=" "
                      select
                      fullWidth
                      label="Recurrence Interval"
                      variant="outlined"
                      value={this.state.recurrenceInterval}
                      onChange={this.handleRecurrenceInterval.bind(this)}
                    >
                      <MenuItem key="0" value="Once Hourly">
                        Once Hourly
                      </MenuItem>
                      <MenuItem key="1" value="Once Daily">
                        Once Daily
                      </MenuItem>
                      <MenuItem key="2" value="Twice Daily">
                        Twice Daily
                      </MenuItem>
                      <MenuItem key="3" value="Custom">
                        Custom
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card className={this.state.recurrenceInterval === 'Custom' ? 'visible' : 'invisible'}>
              <Row gutter={16} className="m-4">
                <Col md={8} className=" col-xs-6 ">
                  <Form>
                    {/* <Label for="functionName">Custom Name / Slug</Label> */}
                    <TextField
                      placeholder=" "
                      variant="outlined"
                      fullWidth
                      label="Custom Name / Slug"
                      type="text"
                      name="functionName"
                      id=""
                      value={this.state.customName}
                      onChange={this.handleCustomName.bind(this)}
                    />
                  </Form>
                </Col>
                <Col md={8} className=" col-xs-6 ">
                  <Form>
                    {/* <Label for="exampleEmail">Custom Label</Label> */}
                    <TextField
                      placeholder=" "
                      variant="outlined"
                      fullWidth
                      label="Custom Label"
                      type="text"
                      value={this.state.customLabel}
                      onChange={this.handleCustomLabel.bind(this)}
                    />
                  </Form>
                </Col>
                <Col md={8} className=" col-xs-6 ">
                  <Form>
                    {/* <Label for="exampleEmail">Custom Interval (in sec)</Label> */}
                    <TextField
                      placeholder=" "
                      variant="outlined"
                      fullWidth
                      label="Custom Interval (in sec)"
                      type="number"
                      value={this.state.customInterval}
                      onChange={this.handleCustomInterval}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={24} lg={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AdSense.Google client="ca-pub-4683176641682642" slot="7806394673" />
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.functionName !== prevState.functionName) {
      this.updateCodeString()
    }
    if (this.state.timeStamp !== prevState.timeStamp) {
      this.updateCodeString()
    }
    if (this.state.recurrenceInterval !== prevState.recurrenceInterval) {
      this.updateCodeString()
    }
    if (this.state.customName !== prevState.customName) {
      this.updateCodeString()
    }
    if (this.state.customLabel !== prevState.customLabel) {
      this.updateCodeString()
    }
    if (this.state.customInterval !== prevState.customInterval) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let functionName = this.state.functionName
    let timeStamp = this.state.timeStamp
    let recurrenceInterval = this.state.recurrenceInterval
    let customName = this.state.customName
    let customLabel = this.state.customLabel
    let customInterval = this.state.customInterval
    let codeString = ''

    let functionNameString = ''
    if (functionName === '') {
      functionNameString = 'your_cron_job_function'
    } else {
      functionNameString = functionName
    }

    let timeStampString = ''
    if (timeStamp === 'GMT Time') {
      timeStampString = 'time()'
    } else {
      timeStampString = "current_time('timestamp')"
    }

    let recurrenceIntervalString = ''
    if (recurrenceInterval === 'Once Hourly') {
      recurrenceIntervalString = 'hourly'
    } else if (recurrenceInterval === 'Once Daily') {
      recurrenceIntervalString = 'daily'
    } else if (recurrenceInterval === 'Twice Daily') {
      recurrenceIntervalString = 'twicedaily'
    }

    if (recurrenceInterval === 'Custom') {
      codeString = `
        // Schedule Cron Job Event
        // Custom Cron Schedule Interval
        function ${functionName}_own( $schedules ) {

          $schedules['${customName}'] = array(
            'interval'  => ${customInterval},
            'display'   => __( '${customLabel}' )
          );

          return $schedules;
        }
        add_filter( 'cron_schedules', '${functionNameString}_own' );

        if ( ! wp_next_scheduled( '${functionNameString}_hook' ) ) {
          wp_schedule_event( ${timeStampString}, '${customName}', '${functionNameString}_hook' );
        }

        add_action('${functionNameString}_hook', '${functionNameString}');

        function ${functionNameString}() {
          // Your cron function

        };
      `
    } else {
      codeString = `
      if ( ! wp_next_scheduled( '${functionNameString}_hook' ) ) {
        wp_schedule_event( ${timeStampString}, '${recurrenceIntervalString}', '${functionNameString}_hook' );
      }

      add_action('${functionNameString}_hook', '');

      function ${functionNameString}() {
        // Your cron function

      };
      `
    }

    this.setState({
      codeItem: codeString,
    })
  }

  validateNumber(event) {
    var key = window.event ? event.keyCode : event.which
    if (event.keyCode === 8 || event.keyCode === 46) {
      return true
    } else if (key < 48 || key > 57) {
      return false
    } else {
      return true
    }
  }

  handleCustomName(event) {
    event.persist()
    console.log(event.target.value)

    this.setState(prevState => ({
      customName: event.target.value,
    }))
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Cron_Job_Event.txt'
    element.click()
  }

  handleCustomLabel(event) {
    event.persist()
    console.log(event.target.value)

    this.setState(prevState => ({
      customLabel: event.target.value,
    }))
  }

  handleCustomInterval = event => {
    event.persist()
    console.log(event.target.value)

    this.setState(
      {
        customInterval: event.target.value,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleFunctionName(event) {
    event.persist()
    console.log(event.target)
    console.log(event.target.value)

    this.setState(prevState => ({
      functionName: event.target.value.trim(),
    }))
  }

  // Timestamp
  handleTimeStamp(event) {
    event.persist()
    console.log(event.target.value)

    this.setState(prevState => ({
      timeStamp: event.target.value,
    }))
  }

  // Recurrence Interval
  handleRecurrenceInterval(event) {
    event.persist()
    console.log(event.target.value)

    this.setState(prevState => ({
      recurrenceInterval: event.target.value,
    }))
  }
}

export default CronJobEvent
