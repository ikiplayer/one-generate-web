import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import {
  Switch,
  FormControlLabel,
  Checkbox,
  TextField,
  MenuItem,
  Button,
  Toolbar,
  Snackbar,
  SnackbarContent,
} from '@material-ui/core'
import { Helmet } from 'react-helmet'
import ChipInput from 'material-ui-chip-input'
import AceEditor from 'react-ace'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class CustomPostType extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Custom Post Generator',
      helmetDescription: 'Generate code for Wordpress custom post generator',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      postTypeNameSingular: '',
      postTypeNamePlural: '',
      postTypeKey: '',
      description: '',
      textDomain: '',
      publicState: false,
      showUI: false,
      showInMenu: false,
      showInAdminBar: false,
      showInNavBar: false,
      showInNavMenus: false,
      canExport: false,
      hasArchive: false,
      hierarchical: false,
      excludeFromSearch: false,
      showInRest: false,
      publiclyQueryable: false,
      menuPosition: { id: 0, value: '5', name: '5 - below Posts' },
      capabilityType: 'Post',
      capabilityTypeItems: [{ id: 0, value: 'Post' }, { id: 1, value: 'Page' }],
      title: false,
      editor: false,
      excerpt: false,
      featuredImage: false,
      revisions: false,
      author: false,
      comments: false,
      trackbacks: false,
      pageAttributes: false,
      postFormats: false,
      customFields: false,
      adminSidebarIcon: 'dashicons-menu',
      codeItem: '',
      customQueryVariable: '',
      slugAsUrlBase: false,
      feeds: false,
      pagination: false,
      taxonomiesKeys: [],
      supportItems: [],
      slugPermalinkText: '',
      rewritePermalink: 'Default (Post Type Key)',
      showInRestApi: false,
      restBase: '',
      restControllerClass: '',
      dashicons: [
        'dashicons-menu',
        'dashicons-dashboard',
        'dashicons-admin-site',
        'dashicons-admin-media',
        'dashicons-admin-page',
        'dashicons-admin-comments',
        'dashicons-admin-appearance',
        'dashicons-admin-plugins',
        'dashicons-admin-users',
        'dashicons-admin-tools',
        'dashicons-admin-settings',
        'dashicons-admin-network',
        'dashicons-admin-generic',
        'dashicons-admin-home',
        'dashicons-admin-collapse',
        'dashicons-admin-links',
        'dashicons-admin-post',
        'dashicons-format-standard',
        'dashicons-format-image',
        'dashicons-format-gallery',
        'dashicons-format-audio',
        'dashicons-format-video',
        'dashicons-format-links',
        'dashicons-format-chat',
        'dashicons-format-status',
        'dashicons-format-aside',
        'dashicons-format-quote',
        'dashicons-welcome-write-blog',
        'dashicons-welcome-edit-page',
        'dashicons-welcome-add-page',
        'dashicons-welcome-view-site',
        'dashicons-welcome-widgets-menus',
        'dashicons-welcome-comments',
        'dashicons-welcome-learn-more',
        'dashicons-image-crop',
        'dashicons-image-rotate-left',
        'dashicons-image-rotate-right',
        'dashicons-image-flip-vertical',
        'dashicons-image-flip-horizontal',
        'dashicons-undo',
        'dashicons-redo',
        'dashicons-editor-bold',
        'dashicons-editor-italic',
        'dashicons-editor-ul',
        'dashicons-editor-ol',
        'dashicons-editor-quote',
        'dashicons-editor-alignleft',
        'dashicons-editor-aligncenter',
        'dashicons-editor-alignright',
        'dashicons-editor-insertmore',
        'dashicons-editor-spellcheck',
        'dashicons-editor-distractionfree',
        'dashicons-editor-expand',
        'dashicons-editor-contract',
        'dashicons-editor-kitchensink',
        'dashicons-editor-underline',
        'dashicons-editor-justify',
        'dashicons-editor-textcolor',
        'dashicons-editor-paste-word',
        'dashicons-editor-paste-text',
        'dashicons-editor-removeformatting',
        'dashicons-editor-video',
        'dashicons-editor-customchar',
        'dashicons-editor-outdent',
        'dashicons-editor-indent',
        'dashicons-editor-help',
        'dashicons-editor-strikethrough',
        'dashicons-editor-unlink',
        'dashicons-editor-rtl',
        'dashicons-editor-break',
        'dashicons-editor-code',
        'dashicons-editor-paragraph',
        'dashicons-align-left',
        'dashicons-align-right',
        'dashicons-align-center',
        'dashicons-align-none',
        'dashicons-lock',
        'dashicons-calendar',
        'dashicons-visibility',
        'dashicons-post-status',
        'dashicons-edit',
        'dashicons-post-trash',
        'dashicons-trash',
        'dashicons-external',
        'dashicons-arrow-up',
        'dashicons-arrow-down',
        'dashicons-arrow-left',
        'dashicons-arrow-right',
        'dashicons-arrow-up-alt',
        'dashicons-arrow-down-alt',
        'dashicons-arrow-left-alt',
        'dashicons-arrow-right-alt',
        'dashicons-arrow-up-alt2',
        'dashicons-arrow-down-alt2',
        'dashicons-arrow-left-alt2',
        'dashicons-arrow-right-alt2',
        'dashicons-leftright',
        'dashicons-sort',
        'dashicons-randomize',
        'dashicons-list-view',
        'dashicons-exerpt-view',
        'dashicons-hammer',
        'dashicons-art',
        'dashicons-migrate',
        'dashicons-performance',
        'dashicons-universal-access',
        'dashicons-universal-access-alt',
        'dashicons-tickets',
        'dashicons-nametag',
        'dashicons-clipboard',
        'dashicons-heart',
        'dashicons-megaphone',
        'dashicons-schedule',
        'dashicons-wordpress',
        'dashicons-wordpress-alt',
        'dashicons-pressthis,',
        'dashicons-update,',
        'dashicons-screenMenuItems',
        'dashicons-info',
        'dashicons-cart',
        'dashicons-feedback',
        'dashicons-cloud',
        'dashicons-translation',
        'dashicons-tag',
        'dashicons-category',
        'dashicons-archive',
        'dashicons-tagcloud',
        'dashicons-text',
        'dashicons-media-archive',
        'dashicons-media-audio',
        'dashicons-media-code',
        'dashicons-media-default',
        'dashicons-media-document',
        'dashicons-media-interactive',
        'dashicons-media-spreadsheet',
        'dashicons-media-text',
        'dashicons-media-video',
        'dashicons-playlist-audio',
        'dashicons-playlist-video',
        'dashicons-yes',
        'dashicons-no',
        'dashicons-no-alt',
        'dashicons-plus',
        'dashicons-plus-alt',
        'dashicons-minus',
        'dashicons-dismiss',
        'dashicons-marker',
        'dashicons-star-filled',
        'dashicons-star-half',
        'dashicons-star-empty',
        'dashicons-flag',
        'dashicons-share',
        'dashicons-share1',
        'dashicons-share-alt',
        'dashicons-share-alt2',
        'dashicons-twitter',
        'dashicons-rss',
        'dashicons-email',
        'dashicons-email-alt',
        'dashicons-facebook',
        'dashicons-facebook-alt',
        'dashicons-networking',
        'dashicons-googleplus',
        'dashicons-location',
        'dashicons-location-alt',
        'dashicons-camera',
        'dashicons-images-alt',
        'dashicons-images-alt2',
        'dashicons-video-alt',
        'dashicons-video-alt2',
        'dashicons-video-alt3',
        'dashicons-vault',
        'dashicons-shield',
        'dashicons-shield-alt',
        'dashicons-sos',
        'dashicons-search',
        'dashicons-slides',
        'dashicons-analytics',
        'dashicons-chart-pie',
        'dashicons-chart-bar',
        'dashicons-chart-line',
        'dashicons-chart-area',
        'dashicons-groups',
        'dashicons-businessman',
        'dashicons-id',
        'dashicons-id-alt',
        'dashicons-products',
        'dashicons-awards',
        'dashicons-forms',
        'dashicons-testimonial',
        'dashicons-portfolio',
        'dashicons-book',
        'dashicons-book-alt',
        'dashicons-download',
        'dashicons-upload',
        'dashicons-backup',
        'dashicons-clock',
        'dashicons-lightbulb',
        'dashicons-microphone',
        'dashicons-desktop',
        'dashicons-tablet',
        'dashicons-smartphone',
        'dashicons-smiley',
      ],
      menuPositionItems: [
        { id: 0, value: '5', name: '5 - below Posts' },
        { id: 1, value: '10', name: '10 - below Media' },
        { id: 2, value: '15', name: '15 - below Links' },
        { id: 3, value: '20', name: '20 - below Pages' },
        { id: 4, value: '25', name: '25 - below Comments' },
        { id: 5, value: '60', name: '60 - below first separator' },
        { id: 7, value: '65', name: '65 - below Plugins' },
        { id: 7, value: '70', name: '70 - below Users' },
        { id: 8, value: '75', name: '75 - below Tools' },
        { id: 9, value: '80', name: '80 - below Settings' },
        { id: 10, value: '100', name: '100 - below second Separator' },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.postTypeNameSingular !== prevState.postTypeNameSingular ||
      this.state.postTypeNamePlural !== prevState.postTypeNamePlural ||
      this.state.postTypeKey !== prevState.postTypeKey ||
      this.state.description !== prevState.description ||
      this.state.textDomain !== prevState.textDomain ||
      this.state.publicState !== prevState.publicState ||
      this.state.showUI !== prevState.showUI ||
      this.state.showInMenu !== prevState.showInMenu ||
      this.state.showInAdminBar !== prevState.showInAdminBar ||
      this.state.showInNavBar !== prevState.showInNavBar ||
      this.state.showInNavMenus !== prevState.showInNavMenus ||
      this.state.canExport !== prevState.canExport ||
      this.state.hasArchive !== prevState.hasArchive ||
      this.state.hierarchical !== prevState.hierarchical ||
      this.state.excludeFromSearch !== prevState.excludeFromSearch ||
      this.state.showInRest !== prevState.showInRest ||
      this.state.publiclyQueryable !== prevState.publiclyQueryable ||
      // this.state.menuPosition !== prevState.menuPosition ||
      // this.state.capabilityType !== prevState.capabilityType ||
      this.state.supportItems !== prevState.supportItems ||
      // this.state.adminSidebarIcon !== prevState.adminSidebarIcon ||
      // this.state.customQueryVariable !== prevState.customQueryVariable ||
      // this.state.rewritePermalink !== prevState.rewritePermalink ||
      this.state.slugPermalinkText !== prevState.slugPermalinkText ||
      this.state.slugAsUrlBase !== prevState.slugAsUrlBase ||
      this.state.feeds !== prevState.feeds ||
      this.state.pagination !== prevState.pagination ||
      this.state.restBase !== prevState.restBase ||
      this.state.restControllerClass !== prevState.restControllerClass ||
      this.state.showInRestApi !== prevState.showInRestApi
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let postTypeNameSingular = this.state.postTypeNameSingular
    let postTypeNamePlural = this.state.postTypeNamePlural
    let postTypeKey = this.state.postTypeKey
    let description = this.state.description
    let textDomain = this.state.textDomain
    let publicState = this.state.publicState
    let showUI = this.state.showUI
    let showInMenu = this.state.showInMenu
    let showInAdminBar = this.state.showInAdminBar
    // let showInNavBar = this.state.showInNavBar;
    let showInNavMenus = this.state.showInNavMenus
    let canExport = this.state.canExport
    let hasArchive = this.state.hasArchive
    let hierarchical = this.state.hierarchical
    let showInRest = this.state.showInRest
    let excludeFromSearch = this.state.excludeFromSearch
    let publiclyQueryable = this.state.publiclyQueryable
    let menuPositionValue = this.state.menuPosition.value
    let capabilityType = this.state.capabilityType
    let supportItems = this.state.supportItems
    let adminSidebarIcon = this.state.adminSidebarIcon
    let customQueryVariable = this.state.customQueryVariable
    let rewritePermalink = this.state.rewritePermalink
    let slugAsUrlBase = this.state.slugAsUrlBase
    let pagination = this.state.pagination
    let feeds = this.state.feeds
    let slugPermalinkText = this.state.slugPermalinkText
    let restBase = this.state.restBase
    let restControllerClass = this.state.restControllerClass
    let taxonomiesKeys = this.state.taxonomiesKeys

    let rewritePermalinkString = ''
    if (rewritePermalink === 'Default (Post Type Key)') {
      rewritePermalinkString = ''
    } else if (rewritePermalink === 'No Permalink') {
      rewritePermalinkString = `'rewrite' => false`
    } else if (rewritePermalink === 'Custom Permalink') {
      rewritePermalinkString = `
      $rewrite = array(
      'slug' => '${slugPermalinkText ? slugPermalinkText : postTypeKey}',
      'with_front' => ${slugAsUrlBase},
      'pages' => ${pagination},
      'feeds' => ${feeds},
    );
      `
    } else {
      rewritePermalinkString = ''
    }

    const taxonomiesKeysLength = taxonomiesKeys.length
    let taxonomiesKeysString = ''
    taxonomiesKeys.map((item, i) => {
      if (taxonomiesKeysLength === i + 1) {
        // console.log(item, i, taxonomiesKeysLength);
        return (taxonomiesKeysString += `'${item}'`)
      } else {
        return (taxonomiesKeysString += `'${item}',`)
      }
    })

    const supportItemLength = supportItems.length
    let supportItemString = ''
    supportItems.map((item, i) => {
      if (supportItemLength === i + 1) {
        // console.log(item, i, supportItemLength);
        return (supportItemString += `'${item}'`)
      } else {
        return (supportItemString += `'${item}',`)
      }
    })

    if (postTypeKey === '') {
      postTypeKey = 'custompost'
    }
    if (postTypeNameSingular === '') {
      postTypeNameSingular = 'Custom Post'
    }

    if (postTypeNamePlural === '') {
      postTypeNamePlural = 'Custom Posts'
    }

    // let restBaseString = ''
    // if (restBase) {
    //   restBaseString = `\n\trest_base' => '${restBase}',`
    // }

    // let restControllerClassString = ''
    // if (restControllerClass) {
    //   restControllerClassString = `\n\t'rest_controller_class' => '${restControllerClass}',`
    // }

    let codeString = `
    // Register Custom Post Type ${postTypeNameSingular}
    function create_${postTypeKey}_cpt() {

	  $labels = array(
		'name' => _x( '${postTypeNamePlural}', 'Post Type General Name', '${textDomain}' ),
		'singular_name' => _x( '${postTypeNameSingular}', 'Post Type Singular Name', '${textDomain}' ),
		'menu_name' => _x( '${postTypeNamePlural}', 'Admin Menu text', '${textDomain}' ),
		'name_admin_bar' => _x( '${postTypeNameSingular}', 'Add New on Toolbar', '${textDomain}' ),
		'archives' => __( '${postTypeNameSingular} Archives', '${textDomain}' ),
		'attributes' => __( '${postTypeNameSingular} Attributes', '${textDomain}' ),
		'parent_item_colon' => __( 'Parent ${postTypeNameSingular}:', '${textDomain}' ),
		'all_items' => __( 'All ${postTypeNamePlural}', '${textDomain}' ),
		'add_new_item' => __( 'Add New ${postTypeNameSingular}', '${textDomain}' ),
		'add_new' => __( 'Add New', '${textDomain}' ),
		'new_item' => __( 'New ${postTypeNameSingular}', '${textDomain}' ),
		'edit_item' => __( 'Edit ${postTypeNameSingular}', '${textDomain}' ),
		'update_item' => __( 'Update ${postTypeNameSingular}', '${textDomain}' ),
		'view_item' => __( 'View ${postTypeNameSingular}', '${textDomain}' ),
		'view_items' => __( 'View ${postTypeNamePlural}', '${textDomain}' ),
		'search_items' => __( 'Search ${postTypeNameSingular}', '${textDomain}' ),
		'not_found' => __( 'Not found', '${textDomain}' ),
		'not_found_in_trash' => __( 'Not found in Trash', '${textDomain}' ),
		'featured_image' => __( 'Featured Image', '${textDomain}' ),
		'set_featured_image' => __( 'Set featured image', '${textDomain}' ),
		'remove_featured_image' => __( 'Remove featured image', '${textDomain}' ),
		'use_featured_image' => __( 'Use as featured image', '${textDomain}' ),
		'insert_into_item' => __( 'Insert into ${postTypeNameSingular}', '${textDomain}' ),
		'uploaded_to_this_item' => __( 'Uploaded to this ${postTypeNameSingular}', '${textDomain}' ),
		'items_list' => __( '${postTypeNamePlural} list', '${textDomain}' ),
		'items_list_navigation' => __( '${postTypeNamePlural} list navigation', '${textDomain}' ),
		'filter_items_list' => __( 'Filter ${postTypeNamePlural} list', '${textDomain}' ),
    );
    $args = array(
      'label' => __( '${postTypeNameSingular}', '${textDomain}' ),
      'description' => __( '${description}', '${textDomain}' ),
      'labels' => $labels,
      'menu_icon' => '${adminSidebarIcon}',
      'supports' => array(${supportItemString}),
      'taxonomies' => array(${taxonomiesKeysString}),
      'public' => ${publicState},
      'show_ui' => ${showUI},
      'show_in_menu' => ${showInMenu},
      'menu_position' => ${menuPositionValue},
      'show_in_admin_bar' => ${showInAdminBar},
      'show_in_nav_menus' => ${showInNavMenus},
      'can_export' => ${canExport},
      'has_archive' => ${hasArchive},
      'hierarchical' => ${hierarchical}, ${
      customQueryVariable ? `\n      'query_var' => '` + customQueryVariable + `'` : ''
    }
      'exclude_from_search' => ${excludeFromSearch},
      'show_in_rest' => ${showInRest}, ${
      restBase ? `\n\t  'rest_base' => '` + restBase + `'` : ''
    }${restControllerClass ? `\n\t  'rest_controller_class' => '` + restControllerClass + `'` : ''}
      'publicly_queryable' => ${publiclyQueryable},
      'capability_type' => '${capabilityType.toLowerCase()}', ${
      rewritePermalink === 'No Permalink' ? `\n\t  ` + rewritePermalinkString : ''
    }
    );

    ${rewritePermalink === 'Custom Permalink' ? rewritePermalinkString : ''}
    register_post_type( '${postTypeKey}', $args );

    }
    add_action( 'init', 'create_${postTypeKey}_cpt', 0 );
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Custom_Post_Generator.txt'
    element.click()
  }

  handlePostTypeNameSingular(event) {
    event.persist()

    let value = event.target.value.trim()

    this.setState({
      postTypeNameSingular: value,
    })
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handlePostTypeNamePlural(event) {
    event.persist()

    let value = event.target.value.trim()

    this.setState({
      postTypeNamePlural: value,
    })
  }
  handlePostTypeKey(event) {
    event.persist()
    let value = event.target.value.trim()

    if (value) {
      this.setState({
        postTypeKey: value,
      })
    } else {
      this.setState({
        postTypeKey: 'custompost',
      })
    }
  }
  handleSupportItems = key => event => {
    event.persist()
    let value = event.target.checked
    // console.log(value);
    this.setState(
      {
        [key]: value,
      },
      () => {
        let title = this.state.title
        let editor = this.state.editor
        let expert = this.state.excerpt
        let featuredImage = this.state.featuredImage
        let revisions = this.state.revisions
        let author = this.state.author
        let comments = this.state.comments
        let trackbacks = this.state.trackbacks
        let pageAttributes = this.state.pageAttributes
        let postFormats = this.state.postFormats
        let customFields = this.state.customFields

        let supportItemArray = []

        if (title) {
          supportItemArray.push('title')
        }

        if (editor) {
          supportItemArray.push('editor')
        }

        if (expert) {
          supportItemArray.push('expert')
        }

        if (featuredImage) {
          supportItemArray.push('featuredImage')
        }

        if (revisions) {
          supportItemArray.push('revisions')
        }

        if (author) {
          supportItemArray.push('author')
        }

        if (comments) {
          supportItemArray.push('comments')
        }

        if (trackbacks) {
          supportItemArray.push('trackbacks')
        }

        if (pageAttributes) {
          supportItemArray.push('pageAttributes')
        }

        if (postFormats) {
          supportItemArray.push('postFormats')
        }

        if (customFields) {
          supportItemArray.push('customFields')
        }
        // console.log(supportItemArray);

        this.setState({
          supportItems: supportItemArray,
        })

        // this.updateCodeString()
      },
    )
  }
  handleCapabilityType(event) {
    event.persist()
    let value = event.target.value
    this.setState(
      {
        capabilityType: value,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleCustomQueryVariable(event) {
    event.persist()
    let value = event.target.value
    this.setState(
      {
        customQueryVariable: value,
      },
      () => {
        this.updateCodeString()
      },
    )

    this.updateCodeString()
  }

  handleRewritePermalink(event) {
    event.persist()
    let value = event.target.value
    // console.log(value);
    this.setState({
      rewritePermalink: value,
    })

    this.updateCodeString()
  }

  handleMenuPosition(event) {
    event.persist()
    let value = event.target.value

    // console.log(value);
    let menuPositionItem = this.state.menuPositionItems.find(item => {
      return item.name === value
    })

    // console.log(menuPositionItem);

    this.setState({
      menuPosition: menuPositionItem,
    })

    this.updateCodeString()
  }

  handleDescription(event) {
    event.persist()
    let value = event.target.value
    this.setState({ description: value })
  }

  handleAddChip(chip) {
    let taxonomiesKeys = this.state.taxonomiesKeys
    // console.log(chip);

    taxonomiesKeys.push(chip.replace(/[^a-zA-Z\d:]/gi, ''))

    this.setState(
      {
        taxonomiesKeys: taxonomiesKeys,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleDeleteChip(chip, index) {
    console.log(chip, index)

    let taxonomiesKeys = this.state.taxonomiesKeys
    if (index > -1) {
      taxonomiesKeys.splice(index, 1)
    }

    this.setState(
      {
        taxonomiesKeys: taxonomiesKeys,
      },
      () => this.updateCodeString(),
    )
  }

  // handleChange(chip) {
  //   chip.persist();
  //   console.log(chip);
  // }

  handleAdminSidebarIcon(event) {
    event.persist()
    let value = event.target.value
    this.setState({ adminSidebarIcon: value }, () => {
      this.updateCodeString()
    })

    // this.updateCodeString()
  }

  handleTextDomain(event) {
    event.persist()
    console.log(event.target.value)
    let value = event.target.value

    if (value) {
      this.setState({ textDomain: value })
    } else {
      value = 'textdomain'
      this.setState({ textDomain: 'textdomain' })
    }
  }
  handleChange = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8} className=" ">
                  <Form>
                    {/* <Label for="functionName">Post Type Name (Singular)</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Type Name (Singular)"
                      type="text"
                      name="Post Type Name (Singular)"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handlePostTypeNameSingular.bind(this)}
                      // onKeyPress={this.handlePostTypeNameSingularKeyPress.bind(
                      //   this
                      // )}
                    />
                  </Form>
                </Col>
                <Col md={8} className=" ">
                  <Form>
                    {/* <Label for="functionName">Post Type Name (Plural)</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Type Name (Plural)"
                      type="text"
                      name="Post Type Name (Plural)"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handlePostTypeNamePlural.bind(this)}
                      // onKeyPress={this.handlePostTypeNamePluralKeyPress.bind(
                      //   this
                      // )}
                    />
                  </Form>
                </Col>
                <Col md={8} className=" ">
                  <Form>
                    {/* <Label for="functionName">Post Type Key</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Type Key"
                      type="text"
                      name="Post Type Key"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handlePostTypeKey.bind(this)}
                      // onKeyPress={this.handlePostTypeKeyKeyPress.bind(this)}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12} className=" ">
                  <Form>
                    {/* <Label for="functionName">Description</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="Description"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleDescription.bind(this)}
                      // onKeyPress={this.handleDescriptionKeyPress.bind(this)}
                    />
                  </Form>
                </Col>
                <Col md={12} className=" ">
                  <Form>
                    {/* <Label for="functionName">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Domain"
                      type="text"
                      name="Text-Domain"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleTextDomain.bind(this)}
                      // onKeyPress={this.handleTextDomainKeyPress.bind(this)}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.hierarchical}
                        onChange={this.handleSupportItems('hierarchical')}
                        value="hierarchical"
                      />
                    }
                    label="Hierarchical"
                  >
                    {/* <Label>Hierarchical</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.excludeFromSearch}
                        onChange={this.handleSupportItems('excludeFromSearch')}
                        value="excludeFromSearch"
                      />
                    }
                    label="Exclude From Search"
                  >
                    {/* <Label>Exclude From Search</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.publiclyQueryable}
                        onChange={this.handleSupportItems('publiclyQueryable')}
                        value="publiclyQueryable"
                      />
                    }
                    label="Publicly Queryable"
                  >
                    {/* <Label>Publicly Queryable</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.hasArchive}
                        onChange={this.handleSupportItems('hasArchive')}
                        value="hasArchive"
                      />
                    }
                    label="Has Archive?"
                  >
                    {/* <Label>Has Archive?</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.publicState}
                        onChange={this.handleSupportItems('publicState')}
                        value="public"
                      />
                    }
                    label="Public"
                  >
                    {/* <Label>Public</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showUI}
                        onChange={this.handleSupportItems('showUI')}
                        value="showUI"
                      />
                    }
                    label="Show UI"
                  >
                    {/* <Label>Show UI</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showInMenu}
                        onChange={this.handleSupportItems('showInMenu')}
                        value="showInMenu"
                      />
                    }
                    label="Show in menu"
                  >
                    {/* <Label for="">Show in menu</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showInAdminBar}
                        onChange={this.handleSupportItems('showInAdminBar')}
                        value="showInAdminBar"
                      />
                    }
                    label="Show in admin bar"
                  >
                    {/* <Label for="">Show in admin bar</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.canExport}
                        onChange={this.handleSupportItems('canExport')}
                        value="canExport"
                      />
                    }
                    label="Can export?"
                  >
                    {/* <Label for="">Can export?</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showInNavMenus}
                        onChange={this.handleSupportItems('showInNavMenus')}
                        value="showInNavMenus"
                      />
                    }
                    label="Show in nav menus"
                  >
                    {/* <Label for="">Show in nav menus</Label> */}
                  </FormControlLabel>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>Support</Col>
                <Col md={8} className=" ">
                  <Form>
                    <Row>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.title}
                            onChange={this.handleSupportItems('title')}
                            value="title"
                          />
                        }
                        label="Title"
                      />
                    </Row>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.editor}
                            onChange={this.handleSupportItems('editor')}
                            value="editor"
                          />
                        }
                        label="Editor (Content)"
                      />
                    </Row>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.excerpt}
                            onChange={this.handleSupportItems('excerpt')}
                            value="excerpt"
                          />
                        }
                        label="Excerpt"
                      />
                    </Row>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.featuredImage}
                            onChange={this.handleSupportItems('featuredImage')}
                            value="featuredImage"
                          />
                        }
                        label="Featured Image"
                      />
                    </Row>
                  </Form>
                </Col>
                <Col md={8} className=" ">
                  <Form>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.revisions}
                            onChange={this.handleSupportItems('revisions')}
                            value="revisions"
                          />
                        }
                        label="Revisions"
                      />
                    </Row>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.author}
                            onChange={this.handleSupportItems('author')}
                            value="author"
                          />
                        }
                        label="Author"
                      />
                    </Row>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.comments}
                            onChange={this.handleSupportItems('comments')}
                            value="comments"
                          />
                        }
                        label="Comments"
                      />
                    </Row>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.trackbacks}
                            onChange={this.handleSupportItems('trackbacks')}
                            value="trackbacks"
                          />
                        }
                        label="Trackbacks"
                      />
                    </Row>
                  </Form>
                </Col>
                <Col md={8} className=" ">
                  <Form>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.pageAttributes}
                            onChange={this.handleSupportItems('pageAttributes')}
                            value="pageAttributes"
                          />
                        }
                        label="Page Attributes"
                      />
                    </Row>
                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.postFormats}
                            onChange={this.handleSupportItems('postFormats')}
                            value="postFormats"
                          />
                        }
                        label="Post Formats"
                      />
                    </Row>

                    <Row gutter={8}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={this.state.customFields}
                            onChange={this.handleSupportItems('customFields')}
                            value="customFields"
                          />
                        }
                        label="Show in REST API"
                      />
                    </Row>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <Form>
                    {/* <Label for="exampleEmail">Menu Position</Label> */}
                    <TextField
                      placeholder=" "
                      select
                      fullWidth
                      variant="outlined"
                      label="Menu Position"
                      type="select"
                      value={this.state.menuPosition.name}
                      onChange={this.handleMenuPosition.bind(this)}
                      multiple={false}
                    >
                      {this.state.menuPositionItems.map((item, index) => {
                        return (
                          <MenuItem key={index} value={item.name}>
                            {item.name}
                          </MenuItem>
                        )
                      })}
                    </TextField>
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="exampleEmail">Capability Type</Label> */}
                    <TextField
                      placeholder=" "
                      select
                      fullWidth
                      variant="outlined"
                      label="Compatability Type"
                      type="select"
                      value={this.state.capabilityType}
                      onChange={this.handleCapabilityType.bind(this)}
                    >
                      <MenuItem key="0" value="Post">
                        Post
                      </MenuItem>
                      <MenuItem key="1" value="Page">
                        Page
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="exampleEmail">Admin Sidebar Icon</Label> */}
                    <TextField
                      placeholder=" "
                      select
                      fullWidth
                      variant="outlined"
                      label="Admin Sidebar Icon"
                      type="select"
                      value={this.state.adminSidebarIcon}
                      onChange={this.handleAdminSidebarIcon.bind(this)}
                    >
                      {this.state.dashicons.map((item, index) => {
                        return (
                          <MenuItem key={index} value={item}>
                            {item}
                          </MenuItem>
                        )
                      })}
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="functionName">Taxonomies (Keys)</Label> */}
                    <ChipInput
                      fullWidthInput
                      variant="outlined"
                      fullWidth
                      label="Taxonomies (Keys)"
                      value={this.state.taxonomiesKeys}
                      onAdd={chip => this.handleAddChip(chip)}
                      onDelete={(chip, index) => this.handleDeleteChip(chip, index)}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      select
                      fullWidth
                      variant="outlined"
                      label="Rewrite (Permalink)"
                      type="select"
                      value={this.state.rewritePermalink}
                      onChange={this.handleRewritePermalink.bind(this)}
                    >
                      <MenuItem key="0" value="Default (Post Type Key)">
                        Default (Post Type Key)
                      </MenuItem>
                      <MenuItem key="1" value="No Permalink">
                        No Permalink
                      </MenuItem>
                      <MenuItem key="2" value="Custom Permalink">
                        Custom Permalink
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Custom Query Variable"
                      type="text"
                      name="Custom query variable"
                      id=""
                      value={this.state.customQueryVariable}
                      onChange={this.handleCustomQueryVariable.bind(this)}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card
              style={{
                display: this.state.rewritePermalink === 'Custom Permalink' ? 'block' : 'none',
              }}
            >
              <Row gutter={8} className="m-4">
                <Col md={6}>
                  <Form>
                    {/* <Label for="functionName">Slug / Pretty permalink text</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Function Name"
                      type="text"
                      name="Slug / Pretty permalink text"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handlePostTypeNameSingular.bind(this)}
                      // onKeyPress={this.handlePostTypeNameSingularKeyPress.bind(
                      //   this
                      // )}
                    />
                  </Form>
                </Col>
                <Col md={6}>
                  <Form>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.slugAsUrlBase}
                          onChange={this.handleChange('slugAsUrlBase')}
                          value="slugAsUrlBase"
                        />
                      }
                      label="Slug as URL base"
                    />
                  </Form>
                </Col>
                <Col md={6}>
                  <Form>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.feeds}
                          onChange={this.handleChange('feeds')}
                          value="feeds"
                        />
                      }
                      label="Feeds"
                    />
                  </Form>
                </Col>
                <Col md={6}>
                  <Form>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.pagination}
                          onChange={this.handleChange('pagination')}
                          value="pagination"
                        />
                      }
                      label="Pagination"
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showInRest}
                        onChange={this.handleChange('showInRest')}
                        value="showInRest"
                      />
                    }
                    label="Show in Rest Api"
                  />
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Rest Base</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Rest Base"
                      type="text"
                      name="Rest Base"
                      id=""
                      value={this.state.restBase}
                      onChange={this.handleInput('restBase')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="functionName">Rest Controller Class</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Rest Controller Class"
                      type="text"
                      name="Rest Controller Class"
                      id=""
                      value={this.state.restControllerClass}
                      onChange={this.handleInput('restControllerClass')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default CustomPostType
