import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
// import { Label } from 'reactstrap'
import {
  Switch,
  FormControlLabel,
  Toolbar,
  Snackbar,
  SnackbarContent,
  Button,
  TextField,
} from '@material-ui/core'
import { Helmet } from 'react-helmet'
import AceEditor from 'react-ace'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class PostStatus extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Post Status',
      helmetDescription: 'Generate code for Wordpress post status',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      postStatusName: '',
      labelSingular: '',
      labelPlural: '',
      textDomain: '',
      publicState: false,
      internalState: false,
      privateState: false,
      excludeFromSearch: false,
      showInAdminAllList: false,
      showInAdminStatusList: false,
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.postStatusName !== prevState.postStatusName ||
      this.state.labelSingular !== prevState.labelSingular ||
      this.state.labelPlural !== prevState.labelPlural ||
      this.state.textDomain !== prevState.textDomain ||
      this.state.publicState !== prevState.publicState ||
      this.state.privateState !== prevState.privateState ||
      this.state.internalState !== prevState.internalState ||
      this.state.excludeFromSearch !== prevState.excludeFromSearch ||
      this.state.showInAdminAllList !== prevState.showInAdminAllList ||
      this.state.showInAdminStatusList !== prevState.showInAdminStatusList
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let postStatusName = this.state.postStatusName
    let labelSingular = this.state.labelSingular
    let labelPlural = this.state.labelPlural
    let textDomain = this.state.textDomain
    let publicState = this.state.publicState
    let internalState = this.state.internalState
    let privateState = this.state.privateState
    let excludeFromSearch = this.state.excludeFromSearch
    let showInAdminAllList = this.state.showInAdminAllList
    let showInAdminStatusList = this.state.showInAdminStatusList

    let postStatusNameString = ''
    if (postStatusName === '') {
      postStatusNameString = 'custompoststatus'
    } else {
      postStatusNameString = postStatusName
    }

    let labelSingularString = ''
    if (labelSingular === '') {
      labelSingularString = 'Custom Post Status'
    } else {
      labelSingularString = labelSingular
    }

    let labelPluralString = ''
    if (labelPlural === '') {
      labelPluralString = 'Custom Post Statuses'
    } else {
      labelPluralString = labelPlural
    }

    let codeString = `
    // Register Post Status: ${postStatusNameString}
  function ${postStatusNameString}_post_status() {

	register_post_status( '${postStatusNameString}', array(
		'label'                     => _x( '${labelSingularString}', '${textDomain}' ),
		'label_count'                     => _n_noop( '${labelSingularString} (%s)',  '${labelPluralString} (%s)', '${textDomain}' ),
		'public'                    => ${publicState},
		'interal'       		    => ${internalState},
		'private'       			=> ${privateState},
		'exclude_from_search'       => ${excludeFromSearch},
		'show_in_admin_all_list'    => ${showInAdminAllList},
		'show_in_admin_status_list' => ${showInAdminStatusList},
	) );
}

add_action( 'init', '${postStatusNameString}_post_status' );
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Post_Status_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Post Status Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Status Name"
                      type="text"
                      name="postStatusName"
                      id=""
                      value={this.state.postStatusName}
                      onChange={this.handleInput('postStatusName')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Label (Name - Singular)</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Label (Name - Singular)"
                      type="text"
                      name="labelSingular"
                      id=""
                      value={this.state.labelSingular}
                      onChange={this.handleInput('labelSingular')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <Form>
                    {/* <Label for="">Label (Name - Plural)</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Label (Name - Plural)"
                      type="text"
                      name="labelPlural"
                      id=""
                      value={this.state.labelPlural}
                      onChange={this.handleInput('labelPlural')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Domain"
                      type="text"
                      name="textDomain"
                      id=""
                      value={this.state.textDomain}
                      onChange={this.handleInput('textDomain')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.publicState}
                        onChange={this.handleCheckbox('publicState')}
                        value="public"
                      />
                    }
                    label="Public"
                  />
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.internalState}
                        onChange={this.handleCheckbox('internalState')}
                        value="internal"
                      />
                    }
                    label="Internal"
                  />
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.privateState}
                        onChange={this.handleCheckbox('privateState')}
                        value="privateState"
                      />
                    }
                    label="Private"
                  />
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.excludeFromSearch}
                        onChange={this.handleCheckbox('excludeFromSearch')}
                        value="excludeFromSearch"
                      />
                    }
                    label="Exclude From Search"
                  />
                  {/* <Form>
                    <Switch
                      checked={this.state.excludeFromSearch}
                      onChange={this.handleCheckbox('excludeFromSearch')}
                      value="excludeFromSearch"
                    />
                  </Form> */}
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showInAdminAllList}
                        onChange={this.handleCheckbox('showInAdminAllList')}
                        value="showInAdminAllList"
                      />
                    }
                    label="Show in admin all lists"
                  />
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showInAdminStatusList}
                        onChange={this.handleCheckbox('showInAdminStatusList')}
                        value="showInAdminStatusList"
                      />
                    }
                    label="Show in admin status list"
                  />
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default PostStatus
