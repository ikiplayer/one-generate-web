import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
// import { Label } from 'reactstrap'
import {
  Button,
  Divider,
  TextField,
  MenuItem,
  Snackbar,
  SnackbarContent,
  Toolbar,
} from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class UserContactMethods extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress User Contact Methods',
      helmetDescription: 'Generate code for Wordpress user contact methods',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      functionName: '',
      textDomain: '',
      items: [
        {
          methodName: '',
          methodDescription: '',
          addOrRemove: 'Add method',
        },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.functionName !== prevState.functionName ||
      this.state.textDomain !== prevState.textDomain
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let functionName = this.state.functionName
    let textDomain = this.state.textDomain
    let items = this.state.items
    let addContactItems = this.state.items.filter(item => {
      return item.addOrRemove === 'Add method'
    })
    let removeContactItems = this.state.items.filter(item => {
      return item.addOrRemove === 'Remove method'
    })

    let functionNameString = ''
    if (functionName === '') {
      functionNameString = 'modify_user_contact_methods'
    } else {
      functionNameString = functionName
    }

    let codeString = `
    // Register User Contact Methods
    function ${functionNameString}( $user_contact ) {

      // Add user contact methods${addContactItems.map(item => {
        if (item.methodName) {
          return `\n\t\t$user_contact['${item.methodName ? item.methodName : ''}']   = __( '${
            item.methodDescription ? item.methodDescription : ''
          }', '${textDomain}' );`
        } else {
          return ''
        }
      })}

      // Remove user contact methods${removeContactItems.map(item => {
        if (item.methodName) {
          return `\n\t\tunset( $user_contact['${item.methodName}'])`
        } else {
          return ''
        }
      })}

      return $user_contact;

    }
    add_filter( 'user_contactmethods', '${functionNameString}' );
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_User_Contact_Methods_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()

    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      methodName: '',
      methodDescription: '',
      addOrRemove: 'Add method',
    })

    this.setState(
      {
        items: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Function Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      label="Function Name"
                      type="text"
                      variant="outlined"
                      name="Function Name"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleInput('functionName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      label="Text Domain)"
                      type="text"
                      variant="outlined"
                      name="Text Domain"
                      id=""
                      value={this.state.textDomain}
                      onChange={this.handleInput('textDomain')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Menus</Label> */}
                  {this.state.items.map((item, index) => {
                    return (
                      <div key={index + 'row'}>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Method Name(Slug)</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Method Name(Slug)"
                                type="text"
                                name="methodName"
                                id=""
                                value={item.methodName}
                                onChange={this.handleItem('methodName', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Method Description</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Method Description"
                                type="text"
                                name="methodDescription"
                                id=""
                                value={this.state.methodDescription}
                                onChange={this.handleItem('methodDescription', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Method Description</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Add Or Remove"
                                type="select"
                                name="addOrRemove"
                                select
                                id=""
                                value={this.state.addOrRemove}
                                onChange={this.handleItem('addOrRemove', index)}
                              >
                                <MenuItem key="0" value="Add method">
                                  Add method
                                </MenuItem>
                                <MenuItem key="1" value="Remove method">
                                  Remove method
                                </MenuItem>
                              </TextField>
                            </Form>
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default UserContactMethods
