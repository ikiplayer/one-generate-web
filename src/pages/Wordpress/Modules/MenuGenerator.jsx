import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import { TextField, Button, Snackbar, SnackbarContent, Toolbar } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'

// import { Label } from 'reactstrap'

import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Menu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Menu',
      helmetDescription: 'Generate code for Wordpress menu',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      functionName: '',
      textDomain: '',
      codeItem: '',
      menusItems: [{ menuSlug: '', menuDescription: '' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.functionName !== prevState.functionName ||
      this.state.textDomain !== prevState.textDomain ||
      this.state.menusItems !== prevState.menusItems
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let functionName = this.state.functionName
    let textDomain = this.state.textDomain

    let functionNameString = ''

    if (functionName === '') {
      functionNameString = 'custom_nav_menus'
    } else {
      functionNameString = functionName
    }

    console.log(
      this.state.menusItems.map(item => {
        return item
      }),
    )

    let codeString = `
    // Register custom navigation menus
      function ${functionNameString}() {

      $locations = array(${this.state.menusItems.map(item => {
        if (item.menuSlug || item.menuDescription) {
          return `\n\t\t  '${item.menuSlug ? item.menuSlug : ''}' => __('${
            item.menuDescription ? item.menuDescription : ''
          }', '${textDomain ? textDomain : ''}')`
        }
        return ''
      })}
      );
      register_nav_menus( $locations );

      }
      add_action( 'init', '${functionNameString}' );
    `

    this.setState({
      codeItem: codeString,
    })
  }
  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Menu_Generator.txt'
    element.click()
  }
  handleItem = (name, index) => event => {
    event.persist()

    let updatedItems = this.state.menusItems
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        menusItems: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let menusItems = this.state.menusItems
    menusItems.push({ menuSlug: '', menuDescription: '' })

    this.setState({
      menusItems: menusItems,
    })
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row className="m-4" gutter={8}>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Function Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Function Name"
                      name="Function Name"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleInput('functionName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Domain"
                      type="text"
                      name="Text Domain"
                      id=""
                      value={this.state.textDomain}
                      onChange={this.handleInput('textDomain')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Menus</Label> */}

                  {this.state.menusItems.map((item, index) => {
                    return (
                      <Row gutter={8} key={index} className="m-4">
                        <Col md={12}>
                          <Form>
                            {/* <Label for="">Menu Slug</Label> */}
                            <TextField
                              placeholder=" "
                              fullWidth
                              variant="outlined"
                              label="Menu Slug"
                              type="text"
                              id=""
                              value={item.menuSlug}
                              onChange={this.handleItem('menuSlug', index)}
                            />
                          </Form>
                        </Col>
                        <Col md={12}>
                          <Form>
                            {/* <Label for="">Menu Description</Label> */}
                            <TextField
                              placeholder=" "
                              fullWidth
                              variant="outlined"
                              label="Menu Description"
                              type="text"
                              name="Text Domain"
                              id=" "
                              value={this.state.menuDescription}
                              onChange={this.handleItem('menuDescription', index)}
                            />
                          </Form>
                        </Col>
                      </Row>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Menu
