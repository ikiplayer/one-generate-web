import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import { TextField, Button, Toolbar, Snackbar, SnackbarContent } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class DashboardWidgetsGenerator extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Dashboard Widgets Generator',
      helmetDescription: 'Generate code for Wordpress dashboard widgets generator',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      functionName: '',
      textDomain: '',
      widgetSlug: '',
      widgetTitle: '',
      renderFunction: '',
      callback: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.functionName !== prevState.functionName ||
      this.state.textDomain !== prevState.textDomain ||
      this.state.widgetSlug !== prevState.widgetSlug ||
      this.state.widgetTitle !== prevState.widgetTitle ||
      this.state.renderFunction !== prevState.renderFunction ||
      this.state.callback !== prevState.callback
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let functionName = this.state.functionName
    let textDomain = this.state.textDomain
    let widgetSlug = this.state.widgetSlug
    let widgetTitle = this.state.widgetTitle
    let renderFunction = this.state.renderFunction
    let callback = this.state.callback

    let functionNameString = ''
    if (functionName === '') {
      functionNameString = 'your_dashboard'
    } else {
      functionNameString = functionName
    }

    let textDomainString = ''
    if (textDomain === '') {
      textDomainString = 'text-domain'
    } else {
      textDomainString = textDomain
    }

    let renderFunctionString = ''
    if (renderFunction === '') {
      renderFunctionString = 'dashboard_widget_function'
    } else {
      renderFunctionString = renderFunction
    }

    let callBackString = ''
    if (callback === '') {
      callBackString = '// Callback Here'
    } else {
      callBackString = callback
    }

    let codeString = `
   // Add a widget to the dashboard.

      function ${functionNameString}_widgets() {

        wp_add_dashboard_widget(
           ${widgetSlug !== '' ? `'` + widgetSlug + `',\n\t\t\t` : ''}${
      widgetTitle !== '' ? `__( '` + widgetTitle + `', '` + textDomainString + `' ),\n\t\t\t` : ''
    }${renderFunction !== '' ? `'` + renderFunction + `',\n\t\t` : ''}
                               
        );
      }
      add_action( 'wp_dashboard_setup', '${functionNameString}' );

      // Create the function to output the contents of Dashboard Widget.

      function ${renderFunctionString}_widget_function() {

        ${callBackString}

      }
    `

    this.setState({
      codeItem: codeString,
    })
  }
  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Dashboard_Widgets_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()

    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      methodName: '',
      methodDescription: '',
      addOrRemove: 'Add method',
    })

    this.setState(
      {
        items: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Function Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      label="Function Name"
                      type="text"
                      variant="outlined"
                      name="Function Name"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleInput('functionName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      label="Text Domain"
                      type="text"
                      variant="outlined"
                      name="Text Domain"
                      id=""
                      value={this.state.textDomain}
                      onChange={this.handleInput('textDomain')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Function Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      label="Widget Slug"
                      type="text"
                      variant="outlined"
                      name="Widget Slug"
                      id=""
                      value={this.state.widgetSlug}
                      onChange={this.handleInput('widgetSlug')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      label="Widget Title"
                      type="text"
                      variant="outlined"
                      name="widgetTitle"
                      id=""
                      value={this.state.widgetTitle}
                      onChange={this.handleInput('widgetTitle')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      label="Render Function"
                      type="text"
                      variant="outlined"
                      name="renderFunction"
                      id=""
                      value={this.state.renderFunction}
                      onChange={this.handleInput('renderFunction')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    <TextField
                      placeholder=" "
                      label="Callback"
                      type="text"
                      fullWidth
                      variant="outlined"
                      multiline={true}
                      name="callback"
                      id=""
                      value={this.state.callback}
                      onChange={this.handleInput('callback')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default DashboardWidgetsGenerator
