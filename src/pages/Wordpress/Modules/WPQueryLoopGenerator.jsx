import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
// import { Label } from 'reactstrap'
import {
  Switch,
  FormControlLabel,
  Button,
  TextField,
  MenuItem,
  Snackbar,
  SnackbarContent,
  Toolbar,
} from '@material-ui/core'
import { Helmet } from 'react-helmet'
import ChipInput from 'material-ui-chip-input'
import copy from 'copy-to-clipboard'
import AceEditor from 'react-ace'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class UserContactMethods extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress user contact methods',
      helmetDescription: 'Generate code for Wordpress user contact methods',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      queryName: '',
      showTheLoop: false,
      paginatePosts: false,
      ignoreStickyPosts: false,
      postTypeKeys: [],
      postStatus: '',
      postsPerPage: '',
      offset: '',
      order: false,
      orderBy: 'date',
      metaKey: '',
      metaValue: '',
      metaValueNum: '',
      metaCompare: '',
      taxonomy: '',
      taxonomyField: 'term_id',
      taxonomyOperator: '',
      //
      taxonomyTerms: [],
      includeChildrenTaxonomies: false,
      postId: '',
      postName: '',
      postTitle: '',
      pageId: '',
      pageName: '',
      postParent: '',
      authorId: '',
      authorName: '',
      categoryId: '',
      categoryName: '',
      tagId: '',
      tagName: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.queryName !== prevState.queryName ||
      this.state.showTheLoop !== prevState.showTheLoop ||
      this.state.paginatePosts !== prevState.paginatePosts ||
      this.state.ignoreStickyPosts !== prevState.ignoreStickyPosts ||
      this.state.postTypeKeys !== prevState.postTypeKeys ||
      this.state.postStatus !== prevState.postStatus ||
      this.state.postsPerPage !== prevState.postsPerPage ||
      this.state.offset !== prevState.offset ||
      this.state.order !== prevState.order ||
      this.state.orderBy !== prevState.orderBy ||
      this.state.metaKey !== prevState.metaKey ||
      this.state.metaValue !== prevState.metaValue ||
      this.state.metaValueNum !== prevState.metaValueNum ||
      this.state.metaCompare !== prevState.metaCompare ||
      this.state.taxonomy !== prevState.taxonomy ||
      this.state.taxonomyField !== prevState.taxonomyField ||
      this.state.taxonomyOperator !== prevState.taxonomyOperator ||
      this.state.taxonomyTerms !== prevState.taxonomyTerms ||
      this.state.includeChildrenTaxonomies !== prevState.includeChildrenTaxonomies ||
      this.state.postId !== prevState.postId ||
      this.state.postName !== prevState.postName ||
      this.state.postTitle !== prevState.postTitle ||
      this.state.pageId !== prevState.pageId ||
      this.state.pageName !== prevState.pageName ||
      this.state.postParent !== prevState.postParent ||
      this.state.authorId !== prevState.authorId ||
      this.state.authorName !== prevState.authorName ||
      this.state.categoryId !== prevState.categoryId ||
      this.state.categoryName !== prevState.categoryName ||
      this.state.tagId !== prevState.tagId ||
      this.state.tagName !== prevState.tagName
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let queryName = this.state.queryName
    let showTheLoop = this.state.showTheLoop
    let paginatePosts = this.state.paginatePosts
    let ignoreStickyPosts = this.state.ignoreStickyPosts
    let postTypeKeys = this.state.postTypeKeys
    let postStatus = this.state.postStatus
    let postsPerPage = this.state.postsPerPage
    let offset = this.state.offset
    let order = this.state.order
    let orderBy = this.state.orderBy
    let metaKey = this.state.metaKey
    let metaValue = this.state.metaValue
    let metaValueNum = this.state.metaValueNum
    let metaCompare = this.state.metaCompare
    let taxonomy = this.state.taxonomy
    let taxonomyField = this.state.taxonomyField
    let taxonomyOperator = this.state.taxonomyOperator
    let taxonomyTerms = this.state.taxonomyTerms
    let includeChildrenTaxonomies = this.state.includeChildrenTaxonomies
    let postId = this.state.postId
    let postName = this.state.postName
    let pageId = this.state.pageId
    let pageName = this.state.pageName
    let postTitle = this.state.postTitle
    let postParent = this.state.postParent
    let authorId = this.state.authorId
    let authorName = this.state.authorName
    let categoryName = this.state.categoryId
    let categoryId = this.state.categoryId
    let tagId = this.state.tagId
    let tagName = this.state.tagName

    const postTypeKeysLength = postTypeKeys.length
    let postTypeKeysString = ''
    postTypeKeys.map((item, i) => {
      if (postTypeKeysLength === i + 1) {
        // console.log(item, i, postTypeKeysLength);
        return (postTypeKeysString += `'${item}'`)
      } else {
        return (postTypeKeysString += `'${item}',`)
      }
    })

    const taxonomyTermsLength = taxonomyTerms.length
    let taxonomyTermsString = ''
    taxonomyTerms.map((item, i) => {
      if (taxonomyTermsLength === i + 1) {
        // console.log(item, i, taxonomyTermsLength);
        return (taxonomyTermsString += `'${item}'`)
      } else {
        return (taxonomyTermsString += `'${item}',`)
      }
    })

    let queryNameString = ''
    if (queryName === '') {
      queryNameString = 'query'
    } else {
      queryNameString = queryName
    }

    let showTheLoopString = ''
    if (showTheLoop) {
      showTheLoopString = `\n\tif ( $${queryNameString}->have_posts() ) {
        while ( $${queryNameString}->have_posts() ) {
          $${queryNameString}->the_post();
        }
      } else {

      }

      wp_reset_postdata();
      `
    }

    let paginatePostsString = ''
    if (!paginatePosts) {
      paginatePostsString = `\n\t'nopaging' => true,`
    }

    let ignoreStickyPostsString = ''
    if (ignoreStickyPosts) {
      ignoreStickyPostsString = `\n\t'ignore_sticky_posts' => true,`
    }

    let postStatusString = ''
    if (postStatus) {
      postStatusString = `\n\t'post_status' => array('${postStatus}'),`
    }

    let postsPerPageString = ''
    if (postsPerPage) {
      postsPerPageString = `\n\t'posts_per_page' => ${postsPerPage},`
    }

    let offsetString = ''
    if (offset) {
      offsetString = `\n\t'offset' => ${offset},`
    }

    let orderByString = ''
    if (orderBy) {
      orderByString = `\n\t'orderby' => '${orderBy}',`
    }

    let metaKeyString = ''
    if (metaKey) {
      metaKeyString = `\n\t'meta_key' => '${metaKey}',`
    }

    let metaValueString = ''
    if (metaValue) {
      metaValueString = `\n\t'meta_value' => '${metaValue}',`
    }

    let metaValueNumString = ''
    if (metaValueNum) {
      metaValueNumString = `\n\t'meta_value_num' => '${metaValueNum}',`
    }

    let metaCompareString = ''
    if (metaCompare) {
      metaCompareString = `\n\t'meta_compare' => '${metaCompare}',`
    }

    let includeChildrenTaxonomiesString = ''
    if (!includeChildrenTaxonomies) {
      includeChildrenTaxonomiesString = `\n\t\tinclude_children' => false,`
    }

    let taxonomyString = ''
    if (taxonomy) {
      taxonomyString = `\n\t'tax_query' => array(
      array(
        'taxonomy' => '${taxonomy}',
        'field' => '${taxonomyField}',${
        taxonomyTerms.length > 0 ? `\n\t\t'terms' => array(${taxonomyTermsString}),` : ''
      }
        'operator' => '${taxonomyOperator}',${includeChildrenTaxonomiesString}
        ),
      ),
      `
    }

    let postIdString = ''
    if (postId) {
      postIdString = `\n\t'p' => ${postId},`
    }

    let postNameString = ''
    if (postName) {
      postNameString = `\n\t'name' => ${postName},`
    }

    let postTitleString = ''
    if (postTitle) {
      postTitleString = `\n\t'title' => ${postTitle},`
    }

    let pageIdString = ''
    if (pageId) {
      pageIdString = `\n\t'page_id' => ${pageId},`
    }

    let pageNameString = ''
    if (pageName) {
      pageNameString = `\n\t'pagename' => '${pageName}'`
    }

    let postParentString = ''
    if (postParent) {
      postParentString = `\n\t'post_parent' => ${postParent},`
    }

    let authorIdString = ''
    if (authorId) {
      authorIdString = `\n\t'author' => ${authorId},`
    }

    let authorNameString = ''
    if (authorName) {
      authorNameString = `\n\t'author_name' => '${authorName}',`
    }

    let categoryIdString = ''
    if (categoryId) {
      categoryIdString = `\n\t'cat' => ${categoryId},`
    }

    let categoryNameString = ''
    if (categoryName) {
      categoryNameString = `\n\t'category_name' => ${categoryName},`
    }

    let tagIdString = ''
    if (tagId) {
      tagIdString = `\n\t'tag_id' => ${tagId},`
    }

    let tagNameString = ''
    if (tagName) {
      tagName = `\n\t'tag' => '${tagName}',`
    }

    let codeString = `
    // Custom WP query ${queryNameString}
    $args_${queryNameString} = array(
    ${
      postTypeKeys.length ? `'post_type' => array(${postTypeKeysString}),` : ''
    }${postStatusString}\n\t'order' => '${
      order ? 'ASC' : 'DESC'
    }',${paginatePostsString}${ignoreStickyPostsString}${postsPerPageString}${offsetString}${orderByString}${metaKeyString}${metaValueString}${metaValueNumString}${metaCompareString}${taxonomyString}${postIdString}${postNameString}${postParentString}${postTitleString}${pageIdString}${pageNameString}${authorIdString}${authorNameString}${categoryIdString}${categoryNameString}${tagIdString}${tagNameString}
    );

    $${queryNameString} = new WP_Query( $args_query );
    ${showTheLoopString}
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_WP_Query_Loop_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handlePostTypeAddChip(chip) {
    let postTypeKeys = this.state.postTypeKeys
    // console.log(chip);

    postTypeKeys.push(chip.replace(/[^a-zA-Z\d:]/gi, ''))

    this.setState(
      {
        postTypeKeys: postTypeKeys,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handlePostTypeDeleteChip(chip, index) {
    console.log(chip, index)

    let postTypeKeys = this.state.postTypeKeys
    if (index > -1) {
      postTypeKeys.splice(index, 1)
    }

    this.setState(
      {
        postTypeKeys: postTypeKeys,
      },
      () => this.updateCodeString(),
    )
  }

  handleTaxonomyTermsAddChip(chip) {
    let taxonomyTerms = this.state.taxonomyTerms
    // console.log(chip);

    taxonomyTerms.push(chip.replace(/[^a-zA-Z\d:]/gi, ''))

    this.setState(
      {
        taxonomyTerms: taxonomyTerms,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleTaxonomyTermsDeleteChip(chip, index) {
    console.log(chip, index)

    let taxonomyTerms = this.state.taxonomyTerms
    if (index > -1) {
      taxonomyTerms.splice(index, 1)
    }

    this.setState(
      {
        taxonomyTerms: taxonomyTerms,
      },
      () => this.updateCodeString(),
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Query Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Query Name"
                      type="text"
                      name="queryName"
                      id=""
                      value={this.state.queryName}
                      onChange={this.handleInput('queryName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.showTheLoop}
                        onChange={this.handleCheckbox('showTheLoop')}
                        value="showTheLoop"
                      />
                    }
                    label="Show the loop?"
                  >
                    {/* <Label>Hierarchical</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.paginatePosts}
                        onChange={this.handleCheckbox('paginatePosts')}
                        value="paginatePosts"
                      />
                    }
                    label="Paginate Posts"
                  >
                    {/* <Label>Hierarchical</Label> */}
                  </FormControlLabel>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.ignoreStickyPosts}
                        onChange={this.handleCheckbox('ignoreStickyPosts')}
                        value="ignoreStickyPosts"
                      />
                    }
                    label="Ignore Sticky Posts"
                  >
                    {/* <Label>Hierarchical</Label> */}
                  </FormControlLabel>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="functionName">Post Type (Keys)</Label> */}
                    <ChipInput
                      fullWidthInput
                      variant="outlined"
                      fullWidth
                      label="Post Type (Keys)"
                      value={this.state.postTypeKeys}
                      onAdd={chip => this.handlePostTypeAddChip(chip)}
                      onDelete={(chip, index) => this.handlePostTypeDeleteChip(chip, index)}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Post Status</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Status"
                      type="text"
                      name="postStatus"
                      id=""
                      value={this.state.postStatus}
                      onChange={this.handleInput('postStatus')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Posts Per Page</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Posts Per Page"
                      type="text"
                      name="postsPerPage"
                      id=""
                      value={this.state.postsPerPage}
                      onChange={this.handleInput('postsPerPage')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Offset</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Offset"
                      type="text"
                      name="postsPerPage"
                      id=""
                      value={this.state.offset}
                      onChange={this.handleInput('offset')}
                    />
                  </Form>
                </Col>
                <Col md={8}>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.order}
                        onChange={this.handleCheckbox('order')}
                        value="order"
                      />
                    }
                    label="Order"
                  >
                    {/* <Label>Hierarchical</Label> */}
                  </FormControlLabel>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="exampleEmail">Order By</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Order By"
                      select
                      type="select"
                      value={this.state.orderBy}
                      onChange={this.handleInput('orderBy')}
                    >
                      <MenuItem key="0" value="date">
                        date
                      </MenuItem>
                      <MenuItem key="1" value="none">
                        none
                      </MenuItem>
                      <MenuItem key="2" value="ID">
                        ID
                      </MenuItem>
                      <MenuItem key="3" value="author">
                        author
                      </MenuItem>
                      <MenuItem key="4" value="title">
                        title
                      </MenuItem>
                      <MenuItem key="5" value="name">
                        name
                      </MenuItem>
                      <MenuItem key="6" value="type">
                        type
                      </MenuItem>
                      <MenuItem key="7" value="modified">
                        modified
                      </MenuItem>
                      <MenuItem key="8" value="parent">
                        parent
                      </MenuItem>
                      <MenuItem key="9" value="rand">
                        rand
                      </MenuItem>
                      <MenuItem key="10" value="comment_count">
                        comment_count
                      </MenuItem>
                      <MenuItem key="11" value="relevance">
                        relevance
                      </MenuItem>
                      <MenuItem key="12" value="menu_order">
                        menu_order
                      </MenuItem>
                      <MenuItem key="13" value="meta_value">
                        meta_value
                      </MenuItem>
                      <MenuItem key="14" value="meta_value_num">
                        meta_value_num
                      </MenuItem>
                      <MenuItem key="15" value="menu_order">
                        menu_order
                      </MenuItem>
                      <MenuItem key="16" value="post_name__in">
                        post_name__in
                      </MenuItem>
                      <MenuItem key="17" value="post_parent__in">
                        post_parent__in
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Meta Key</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Meta Key"
                      type="text"
                      name="metaKey"
                      id=""
                      value={this.state.metaKey}
                      onChange={this.handleInput('metaKey')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Meta Value</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Meta Value"
                      type="text"
                      name="metaValue"
                      id=""
                      value={this.state.metaValue}
                      onChange={this.handleInput('metaValue')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Meta Value Num</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Meta Value Num"
                      type="text"
                      name="metaValueNum"
                      id=""
                      value={this.state.metaValueNum}
                      onChange={this.handleInput('metaValueNum')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Meta Compare</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Meta Compare"
                      type="text"
                      name="metaCompare"
                      id=""
                      value={this.state.metaCompare}
                      onChange={this.handleInput('metaCompare')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Taxonomy</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Taxonomy"
                      type="text"
                      name="taxonomy"
                      id=""
                      value={this.state.taxonomy}
                      onChange={this.handleInput('taxonomy')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="exampleEmail">Taxonomy Field</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Taxonomy Field"
                      type="select"
                      select
                      value={this.state.taxonomyField}
                      onChange={this.handleInput('taxonomyField')}
                    >
                      <MenuItem key="0" value="term_id">
                        term_id
                      </MenuItem>
                      <MenuItem key="1" value="name">
                        name
                      </MenuItem>
                      <MenuItem key="2" value="slug">
                        slug
                      </MenuItem>
                      <MenuItem key="3" value="term_taxonomy_id">
                        term_taxonomy_id
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="exampleEmail">Taxonomy Operator</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Taxonomy Operator"
                      type="select"
                      select
                      value={this.state.taxonomyOperator}
                      onChange={this.handleInput('taxonomyOperator')}
                    >
                      <MenuItem key="0" value="IN">
                        IN
                      </MenuItem>
                      <MenuItem key="1" value="NOT IN">
                        NOT IN
                      </MenuItem>
                      <MenuItem key="2" value="AND">
                        AND
                      </MenuItem>
                      <MenuItem key="3" value="EXISTS">
                        EXISTS
                      </MenuItem>
                      <MenuItem key="4" value="NOT EXISTS">
                        NOT EXISTS
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="functionName">Taxonomy Terms</Label> */}
                    <ChipInput
                      fullWidthInput
                      variant="outlined"
                      fullWidth
                      label="Taxonomy Terms"
                      value={this.state.taxonomyTerms}
                      onAdd={chip => this.handleTaxonomyTermsAddChip(chip)}
                      onDelete={(chip, index) => this.handleTaxonomyTermsDeleteChip(chip, index)}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Post ID</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post ID"
                      type="text"
                      name="postId"
                      id=""
                      value={this.state.postId}
                      onChange={this.handleInput('postId')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Post Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Name"
                      type="text"
                      name="postName"
                      id=""
                      value={this.state.postName}
                      onChange={this.handleInput('postName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Post Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Title"
                      type="text"
                      name="postTitle"
                      id=""
                      value={this.state.postTitle}
                      onChange={this.handleInput('postTitle')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Page ID</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Page ID"
                      type="text"
                      name="pageId"
                      id=""
                      value={this.state.pageId}
                      onChange={this.handleInput('pageId')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Page Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Page Name"
                      type="text"
                      name="pageName"
                      id=""
                      value={this.state.pageName}
                      onChange={this.handleInput('pageName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Post Parent</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Post Parent"
                      type="text"
                      name="postParent"
                      id=""
                      value={this.state.postParent}
                      onChange={this.handleInput('postParent')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Author ID</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Author ID"
                      type="text"
                      name="authorId"
                      id=""
                      value={this.state.authorId}
                      onChange={this.handleInput('authorId')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Author Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Author Name"
                      type="text"
                      name="authorName"
                      id=""
                      value={this.state.authorName}
                      onChange={this.handleInput('authorName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Category ID</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Category ID"
                      type="text"
                      name="categoryId"
                      id=""
                      value={this.state.categoryId}
                      onChange={this.handleInput('categoryId')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Category Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Category Name"
                      type="text"
                      name="categoryName"
                      id=""
                      value={this.state.categoryName}
                      onChange={this.handleInput('categoryName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Tag ID</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Tag ID"
                      type="text"
                      name="tagId"
                      id=""
                      value={this.state.tagId}
                      onChange={this.handleInput('tagId')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Tag Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Tag Names"
                      type="text"
                      name="tagName"
                      id=""
                      value={this.state.tagName}
                      onChange={this.handleInput('tagName')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default UserContactMethods
