import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import { Button, TextField, Snackbar, SnackbarContent, Toolbar } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import AceEditor from 'react-ace'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class QuickTagsForClassicEditor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Quicktags for classic editor',
      helmetDescription: 'Generate code for Wordpress quicktags for classic editor',
      siteName: 'OneGenerate',
      functionName: '',
      items: [
        {
          quickTagId: '',
          quickTagDisplay: '',
          startTag: '',
          endTag: '',
          accessKey: '',
          title: '',
          priority: '',
          instance: '',
        },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.functionName !== prevState.functionName) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let functionName = this.state.functionName
    let items = this.state.items

    let functionNameString = ''
    if (functionName === '') {
      functionNameString = 'add_custom_quicktags'
    } else {
      functionNameString = functionName
    }

    let codeString = `
    // Add Custom Quicktags to Text Editor
  function ${functionNameString}() {

    if ( wp_script_is( 'quicktags' ) ) { ?>
      <script type="text/javascript">${items.map(item => {
        if (item.startTag && item.endTag && item.quickTagId && item.quickTagDisplay) {
          return `\n\t 	QTags.addButton( '${item.quickTagId ? item.quickTagId : ''}', '${
            item.quickTagDisplay ? item.quickTagDisplay : ''
          }', '${item.startTag ? item.startTag : ''}', '${item.endTag ? item.endTag : ''}', '${
            item.accessKey ? item.accessKey : ''
          }', '${item.title ? item.title : ''}', ${item.priority ? item.priority : ''} , '${
            item.instance ? item.instance : ''
          }');`
        }
        return ''
      })}
      </script>
    <?php }

  }
  add_action( 'admin_print_footer_scripts', '${functionNameString}' );
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Quicktag_For_Classic_Editor_Generator.txt'
    element.click()
  }

  handleItem = (name, index) => event => {
    event.persist()
    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      quickTagId: '',
      quickTagDisplay: '',
      startTag: '',
      endTag: '',
      accessKey: '',
      title: '',
      priority: '',
      instance: '',
    })

    this.setState({
      items: items,
    })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={24}>
                  <Form>
                    {/* <Label for="">Function Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Function Name"
                      type="text"
                      name="Function Name"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleInput('functionName')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Quicktags</Label> */}

                  {this.state.items.map((item, index) => {
                    return (
                      <div key={index}>
                        <Row gutter={8} className="m-3">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Quicktag ID</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Quicktag ID"
                                type="text"
                                name="quickTagId"
                                id=""
                                value={item.quickTagId}
                                onChange={this.handleItem('quickTagId', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Quicktag Display</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Quicktag Display"
                                type="text"
                                name="quickTagDisplay"
                                id=""
                                value={item.quickTagDisplay}
                                onChange={this.handleItem('quickTagDisplay', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-3">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Start Tag</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Start Tag"
                                type="text"
                                name="startTag"
                                id=""
                                value={item.startTag}
                                onChange={this.handleItem('startTag', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="End Tag"
                                type="text"
                                name="endTag"
                                id=""
                                value={item.endTag}
                                onChange={this.handleItem('endTag', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Access Key</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Access Key"
                                type="text"
                                name="accessKey"
                                id=""
                                value={item.accessKey}
                                onChange={this.handleItem('accessKey', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Title</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Title"
                                type="text"
                                name="title"
                                id=""
                                value={item.title}
                                onChange={this.handleItem('title', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Priority</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Priority"
                                type="number"
                                name="priority"
                                id=""
                                value={item.priority}
                                onChange={this.handleItem('priority', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Instance</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Instance"
                                type="text"
                                name="instance"
                                id=""
                                value={item.instance}
                                onChange={this.handleItem('instance', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default QuickTagsForClassicEditor
