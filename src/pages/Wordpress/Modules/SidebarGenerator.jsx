import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
// import { Label } from 'reactstrap'
import { Divider, TextField, Snackbar, SnackbarContent, Toolbar, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import AceEditor from 'react-ace'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Sidebar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Sidebar',
      helmetDescription: 'Generate code for Wordpress sidebar',
      siteName: 'OneGenerate',
      functionName: '',
      textDomain: '',
      items: [
        {
          name: '',
          description: '',
          id: '',
          class: '',
          beforeWidget: '',
          afterWidget: '',
          beforeTitle: '',
          afterTitle: '',
        },
      ],
      codeItem: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.functionName !== prevState.functionName ||
      this.state.textDomain !== prevState.textDomain
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let functionName = this.state.functionName
    let textDomain = this.state.textDomain
    let items = this.state.items

    let functionNameString = ''
    if (functionName === '') {
      functionNameString = 'custom_sidebars'
    } else {
      functionNameString = functionName
    }

    let textDomainString = ''
    if (textDomain === '') {
      textDomainString = 'textdomain'
    } else {
      textDomainString = textDomain
    }

    let codeString = `
    // Register custom sidebars
    function ${functionNameString}() {
      ${items.map(item => {
        let descriptionString = ''
        if (item.description) {
          descriptionString = `\n\t\t\t'description'\t=> __('${
            item.description
          }', '${textDomain}' ),`
        }
        let idString = ''
        if (item.id) {
          idString = `\n\t\t\t'id'\t\t\t=> '${item.id}',`
        }

        let classString = ''
        if (item.class) {
          classString = `\n\t\t\t'class'\t\t\t=> '${item.class}',`
        }

        let beforeWidgetString = `<li id="%1$s" class="widget %2$s">`
        if (item.beforeWidget) {
          beforeWidgetString = item.beforeWidget
        }

        let afterWidgetString = `</li>`
        if (item.afterWidget) {
          afterWidgetString = item.afterWidget
        }

        let beforeTitleString = `<h2 class="widgettitle">`
        if (item.beforeTitle) {
          beforeTitleString = item.beforeTitle
        }

        let afterTitleString = `</h2>`
        if (item.afterTitle) {
          afterTitleString = item.afterTitle
        }

        if (item.name) {
          return `
          	$args = array(
            'name'          => __( '${
              item.name
            }', '${textDomain}' ),${descriptionString}${idString}${classString}
            'before_widget' => '${beforeWidgetString}',
            'after_widget'  => '${afterWidgetString}',
            'before_title'  => '${beforeTitleString}',
            'after_title'   => '${afterTitleString}',
          );
          register_sidebar($args);
          `
        } else {
          return ''
        }
      })}
    }
    add_action( 'widgets_init', '${functionNameString}' );
      `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Sidebar_Generator.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()
    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      name: '',
      description: '',
      id: '',
      class: '',
      beforeWidget: '',
      afterWidget: '',
      beforeTitle: '',
      afterTitle: '',
    })

    this.setState({
      items: items,
    })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Function Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Function Name"
                      type="text"
                      name="Function Name"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleInput('functionName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Domain"
                      type="text"
                      name="Text Domain"
                      id=""
                      value={this.state.textDomain}
                      onChange={this.handleInput('textDomain')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Attributes</Label> */}
                  {this.state.items.map((item, index) => {
                    return (
                      <div key={index}>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Name</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Name"
                                type="text"
                                name="name"
                                id=""
                                value={item.name}
                                onChange={this.handleItem('name', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Description</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Description"
                                type="text"
                                name="description"
                                id=""
                                value={item.description}
                                onChange={this.handleItem('description', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">ID</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="ID"
                                type="text"
                                name="id"
                                id=""
                                value={item.id}
                                onChange={this.handleItem('id', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Class</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Class"
                                type="text"
                                name="class"
                                id=""
                                value={item.class}
                                onChange={this.handleItem('class', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Before Widget</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Before Widget"
                                type="text"
                                name="beforeWidget"
                                id=""
                                value={item.beforeWidget}
                                onChange={this.handleItem('beforeWidget', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">After Widget</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="After Widget"
                                type="text"
                                name="afterWidget"
                                id=""
                                value={item.afterWidget}
                                onChange={this.handleItem('afterWidget', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">Before Title</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Before Title"
                                type="text"
                                name="beforeTitle"
                                id=""
                                value={item.beforeTitle}
                                onChange={this.handleItem('beforeTitle', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={12}>
                            <Form>
                              {/* <Label for="">After Title</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="After Title"
                                type="text"
                                name="afterTitle"
                                id=""
                                value={item.afterTitle}
                                onChange={this.handleItem('afterTitle', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Sidebar
