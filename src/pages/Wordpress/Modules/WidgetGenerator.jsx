import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'

// import { Label } from 'reactstrap'
import {
  Divider,
  TextField,
  MenuItem,
  Snackbar,
  SnackbarContent,
  Toolbar,
  Button,
} from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import AceEditor from 'react-ace'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class WidgetGenerator extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Widget Generator',
      helmetDescription: 'Generate code for Wordpress widget generator',
      siteName: 'OneGenerate',
      title: '',
      description: '',
      hasWidgetField: false,
      textDomain: '',
      items: [
        {
          fieldType: 'Textfield',
          label: '',
          default: '',
          id: '',
        },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.title !== prevState.title ||
      this.state.description !== prevState.description ||
      this.state.hasWidgetField !== prevState.hasWidgetField ||
      this.state.textDomain !== prevState.textDomain
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let title = this.state.title
    let description = this.state.description
    let hasWidgetField = this.state.hasWidgetField
    let textDomain = this.state.textDomain
    let items = this.state.items

    let textDomainString = ''
    if (textDomain === '') {
      textDomainString = `textdomain`
    } else {
      textDomainString = textDomain
    }

    let descriptionString = ''
    if (description) {
      descriptionString = `\n\t\t  array( 'description' => esc_html__( '${description}', '${textDomainString}' ), ) // Args`
    }
    let hasWidgetFieldString = ''
    if (hasWidgetField === 'Yes') {
      hasWidgetFieldString = `if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}`
    }

    let codeString = `
    // Adds widget: ${title ? title : 'Widget'}
    class ${title ? title : 'New'}_Widget extends WP_Widget {

      function __construct() {
        parent::__construct(
          'new_widget',
          esc_html__( '${title ? title : 'Widget'}', '${textDomainString}' ),${descriptionString}
        );
      }

      private $widget_fields = array(
        ${items.map(item => {
          if (item.label) {
            let defaultString = ''
            if (item.default) {
              defaultString = `\n\t\t\t\t'default' => '${item.default}'`
            }
            return `
            	array(
                'label' => '${item.label}',
                'id' => '${
                  item.id
                    ? item.id + '_' + this.getFieldTypeString(item.fieldType)
                    : item.label + '_' + this.getFieldTypeString(item.fieldType)
                }',${defaultString}
                'type' => 'checkbox',
              ),
            `
          }
          return ''
        })}
      );

      public function widget( $args, $instance ) {
        echo $args['before_widget'];

        if ( ! empty( $instance['title'] ) ) {
          echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        ${hasWidgetFieldString}
        // Output generated fields${items.map(item => {
          return `\n\t\techo '<p>'.$instance['${
            item.id
              ? item.id + '_' + this.getFieldTypeString(item.fieldType)
              : item.label + '_' + this.getFieldTypeString(item.fieldType)
          }'].'</p>';
        `
        })}
        
        echo $args['after_widget'];
      }

      public function field_generator( $instance ) {
        $output = '';
        foreach ( $this->widget_fields as $widget_field ) {
          $widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $widget_field['default'], 'textdomain' );
          switch ( $widget_field['type'] ) {
            ${items.map(item => {
              if (item.fieldType === 'Textfield') {
                return `
              	case 'checkbox':
                $output .= '<p>';
                $output .= '<TextField placeholder=" " class="checkbox" type="checkbox" '.checked( $widget_value, true, false ).' id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" value="1">';
                $output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'textdomain' ).'</label>';
                $output .= '</p>';
                break;
              `
              }
              return ''
            })}
            default:
              $output .= '<p>';
              $output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'textdomain' ).':</label> ';
              $output .= '<TextField placeholder=" " class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
              $output .= '</p>';
          }
        }
        echo $output;
      }

      public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'textdomain' );
        ?>
        <p>
          <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'textdomain' ); ?></label>
          <TextField placeholder=" " class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
        $this->field_generator( $instance );
      }

      public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        foreach ( $this->widget_fields as $widget_field ) {
          switch ( $widget_field['type'] ) {
            default:
              $instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
          }
        }
        return $instance;
      }
    }

    function register_${title ? title : 'new'}_widget() {
      register_widget( '${title ? title : 'New'}_Widget' );
    }
    add_action( 'widgets_init', 'register_${title ? title : 'new'}_widget' );
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Widget_Generator.txt'
    element.click()
  }

  getFieldTypeString(fieldType = '') {
    if (fieldType === '') {
      return ''
    } else if (fieldType === 'Textfield') {
      return 'text'
    } else {
      return fieldType.toLowerCase()
    }
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()
    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      fieldType: 'Textfield',
      label: '',
      default: '',
      id: '',
    })

    this.setState({
      items: items,
    })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Title"
                      type="text"
                      name="Title"
                      id=""
                      value={this.state.title}
                      onChange={this.handleInput('title')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Description</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Description"
                      type="text"
                      name="Description"
                      id=""
                      value={this.state.description}
                      onChange={this.handleInput('description')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">
                                            Should your widget contain a title field?
                    </Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Should your widget contain a title field?"
                      type="select"
                      select
                      name="Title"
                      value={this.state.hasWidgetField}
                      onChange={this.handleInput('hasWidgetField')}
                    >
                      <MenuItem key="0" value="Yes">
                        Yes
                      </MenuItem>
                      <MenuItem key="1" value="No">
                        No
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Text Domain</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Text Domain"
                      type="text"
                      name="Text Domain"
                      id=""
                      value={this.state.textDomain}
                      onChange={this.handleInput('textDomain')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Widget Fields</Label> */}

                  {this.state.items.map((item, index) => {
                    return (
                      <div key={index + 'row'}>
                        <Row gutter={8} className="m-4">
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Field Type</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Field Type"
                                select
                                type="select"
                                name="name"
                                value={item.fieldType}
                                onChange={this.handleItem('fieldType', index)}
                              >
                                <MenuItem key="0" value="Textfield">
                                  Textfield
                                </MenuItem>
                                <MenuItem key="1" value="Textarea">
                                  Textarea
                                </MenuItem>
                                <MenuItem key="2" value="Checkbox">
                                  Checkbox
                                </MenuItem>
                                <MenuItem key="3" value="Media">
                                  Media
                                </MenuItem>
                                <MenuItem key="4" value="Email">
                                  Email
                                </MenuItem>
                                <MenuItem key="5" value="Url">
                                  Url
                                </MenuItem>
                                <MenuItem key="6" value="Password">
                                  Password
                                </MenuItem>
                                <MenuItem key="7" value="Number">
                                  Number
                                </MenuItem>
                                <MenuItem key="8" value="Tel">
                                  Tel
                                </MenuItem>
                                <MenuItem key="9" value="Date">
                                  Date
                                </MenuItem>
                              </TextField>
                            </Form>
                          </Col>
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Label / Name</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Label / Name"
                                type="text"
                                name="label"
                                id=""
                                value={item.label}
                                onChange={this.handleItem('label', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={4}>
                            <Form>
                              {/* <Label for="">Default</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Default"
                                type="text"
                                name="default"
                                id=""
                                value={item.default}
                                onChange={this.handleItem('default', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={4}>
                            <Form>
                              {/* <Label for="">ID</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="ID"
                                type="text"
                                name="ID"
                                id=""
                                value={item.id}
                                onChange={this.handleItem('id', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Divider variant="middle" />
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={4}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default WidgetGenerator
