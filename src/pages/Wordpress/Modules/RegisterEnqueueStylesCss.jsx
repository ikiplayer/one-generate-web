import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
// import { Label } from 'reactstrap'
import { Button, MenuItem, TextField, Snackbar, SnackbarContent, Toolbar } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import { AddBox } from '@material-ui/icons'
import AceEditor from 'react-ace'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class RegisterEnqueueStylesCss extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Register Enqueue Styles CSS',
      helmetDescription: 'Generate code for Wordpress register enqueue styles CSS',
      siteName: 'OneGenerate',
      functionName: '',
      whereToEnqueue: 'Frontend',
      items: [
        {
          name: '',
          urlPath: 'Full URL of the stylesheet',
          url: '',
          dependencies: '',
          version: '',
          locationOfTheScript: 'Header',
        },
      ],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.functionName !== prevState.functionName ||
      this.state.whereToEnqueue !== prevState.whereToEnqueue
    ) {
      this.updateCodeString()
    }
  }

  updateCodeString() {
    let functionName = this.state.functionName
    let whereToEnqueue = this.state.whereToEnqueue
    let items = this.state.items

    let functionNameString = ''
    if (functionName === '') {
      functionNameString = 'enqueue_custom_styles'
    } else {
      functionNameString = functionName
    }

    let whereToEnqueueString = ''
    if (whereToEnqueue === 'Frontend') {
      whereToEnqueueString = 'wp_enqueue_scripts'
    } else if (whereToEnqueue === 'Admin') {
      whereToEnqueueString = 'admin_enqueue_scripts'
    } else if (whereToEnqueue === 'Login') {
      whereToEnqueueString = 'login_enqueue_scripts'
    } else if (whereToEnqueue === 'Embed') {
      whereToEnqueueString = 'enqueue_embed_scripts'
    } else {
      whereToEnqueueString = 'wp_enqueue_scripts'
    }

    let codeString = `
    // Enqueue own styles
    function ${functionNameString}() {${items.map(item => {
      if (item.name) {
        let urlPathString = ''
        if (item.urlPath === 'Full URL of the stylesheet') {
          urlPathString = ''
        } else {
          urlPathString = 'get_template_directory_uri() .'
        }

        let itemDependenciesString = "'" + item.dependencies + "'"
        return `\n\t wp_enqueue_style( '${item.name}', ${urlPathString}'${item.url}', array(${
          item.dependencies ? itemDependenciesString : ''
        }), ${item.version ? item.version : false}, '${item.media ? item.media : 'all'}' );`
      }
      return ''
    })}


    }
    add_action( '${whereToEnqueueString}', '${functionNameString}' );
    
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Register_Enqueue_Styles_Css.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItem = (name, index) => event => {
    event.persist()
    let updatedItems = this.state.items
    updatedItems[index][name] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleClick = () => event => {
    let items = this.state.items
    items.push({
      name: '',
      urlPath: 'Full URL of the stylesheet',
      url: '',
      dependencies: '',
      version: '',
      locationOfTheScript: 'Header',
    })

    this.setState({
      items: items,
    })
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Function Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Function Name"
                      type="text"
                      name="Function Name"
                      id=""
                      value={this.state.functionName}
                      onChange={this.handleInput('functionName')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="exampleEmail">Where to enqueue?</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      select
                      variant="outlined"
                      label="Where to enqueue?"
                      type="select"
                      value={this.state.whereToEnqueue}
                      onChange={this.handleInput('whereToEnqueue')}
                    >
                      <MenuItem key="0" value="Frontend">
                        Frontend
                      </MenuItem>
                      <MenuItem key="1" value="Admin">
                        Admin
                      </MenuItem>
                      <MenuItem key="2" value="Login">
                        Login
                      </MenuItem>
                      <MenuItem key="3" value="Embed">
                        Embed
                      </MenuItem>
                    </TextField>
                  </Form>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  {/* <Label for="">Quicktags</Label> */}

                  {this.state.items.map((item, index) => {
                    return (
                      <div key={index}>
                        <Row gutter={8} className="m-4">
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Name</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Name"
                                type="text"
                                name="name"
                                id=""
                                value={item.name}
                                onChange={this.handleItem('name', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">URL Path</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                select
                                variant="outlined"
                                label="URL Path"
                                type="select"
                                value={item.urlPath}
                                onChange={this.handleItem('urlPath', index)}
                              >
                                <MenuItem key="0" value="Full URL of the stylesheet">
                                  Full URL of the stylesheet
                                </MenuItem>
                                <MenuItem key="1" value="Relative to the theme">
                                  Relative to the theme
                                </MenuItem>
                              </TextField>
                            </Form>
                          </Col>
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Source / URL</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Source / URL"
                                type="text"
                                name="url"
                                id=""
                                value={item.url}
                                onChange={this.handleItem('url', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                        <Row gutter={8} className="m-4">
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Dependencies</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Dependencies"
                                type="text"
                                name="dependencies"
                                id=""
                                value={item.dependencies}
                                onChange={this.handleItem('dependencies', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Version</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Version"
                                type="number"
                                name="version"
                                id=""
                                value={item.version}
                                onChange={this.handleItem('version', index)}
                              />
                            </Form>
                          </Col>
                          <Col md={8}>
                            <Form>
                              {/* <Label for="">Media</Label> */}
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Media"
                                type="text"
                                name="media"
                                id=""
                                value={item.media}
                                onChange={this.handleItem('media', index)}
                              />
                            </Form>
                          </Col>
                        </Row>
                      </div>
                    )
                  })}
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default RegisterEnqueueStylesCss
