import React, { Component } from 'react'
import {
  Row,
  Col,
  //  Form, Card
} from 'antd'
import {
  Switch,
  FormControlLabel,
  Checkbox,
  TextField,
  MenuItem,
  Toolbar,
  Button,
} from '@material-ui/core'
import { Helmet } from 'react-helmet'
// import ChipInput from 'material-ui-chip-input'
import AceEditor from 'react-ace'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class MetaBox extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'Wordpress Meta Box',
      helmetDescription: 'Generate code for Wordpress Meta Box',
      siteName: 'OneGenerate',
      functionName: '',
      timeStamp: '',
      recurrenceInterval: '',
      customName: '',
      customLabel: '',
      customInterval: '',
      codeItem: `
      if ( ! wp_next_scheduled( '_hook' ) ) {
        wp_schedule_event( time(), 'hourly', '_hook' );
      }

      add_action('_hook', '');

      function () {
        // Your cron function

      };
      `,
    }
  }
  componentDidMount() {}

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Wordpress_Meta_Box_Generator.txt'
    element.click()
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row>
          <Col md={14} />
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default MetaBox
