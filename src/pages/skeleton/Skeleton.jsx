import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Divider, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'; import { Helmet } from 'react-helmet'

import { AddBox } from '@material-ui/icons'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Skeleton extends Component {
  constructor(props) {
    super(props)
    this.state = { helmetTitle: '', helmetDescription: 'Generate code for bootstrap', siteName: 'OneGenerate',
      isSnackBarOpen: '',
      title: '',
      siteName: '',
      siteUrl: '',
      publishDate: '',
      items: [{ url: '' }],
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (
  //     this.state.title !== prevState.title ||
  //     this.state.siteName !== prevState.siteName ||
  //     this.state.siteUrl !== prevState.siteUrl ||
  //     this.state.publishDate !== prevState.publishDate
  //   ) {
  //     this.updateCodeString()
  //   }
  // }

  updateCodeString() {
    let title = this.state.title
    let siteName = this.state.siteName
    let siteUrl = this.state.siteUrl
    let publishDate = this.state.publishDate
    let authorItems = this.state.authorItems
    let imageItems = this.state.imageItems

    let siteNameString = ''
    if (siteName) {
      siteNameString = `\n\t<meta property="og:site_name" content="${siteName}">`
    }

    // let descriptionString = "";
    // if (description) {
    //   descriptionString = `\n\t<meta property="og:description" content="${description}">`;
    // }

    let codeString = `
    <meta property="og:type" content="article">
    <meta property="og:url" content="${siteUrl}">
    <meta property="article:published_time" content="${publishDate}">${siteNameString}${authorItems.map(
      (item, index) => {
        return `\n\t<meta property="article:author" content="${item.url}">`
      },
    )}${imageItems.map((item, index) => {
      return `\n\t<meta property="og:image" content="${item.url}">`
    })}
    `

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'Skeleton.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
       <div className="content"><Helmet title={this.state.helmetTitle}>           <meta property="og:title" content={this.state.helmetTitle}/>           <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>                                   <meta property="og:site_name" content={this.state.siteName}/>           <meta property="og:url" content={window.location.href}/>           <meta property="twitter:title" content={this.state.helmentTitle} />           <meta property="twitter:description" content={this.state.helmetDescription} />         </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Title"
                      type="text"
                      name="title"
                      id=""
                      value={this.state.title}
                      onChange={this.handleInput('title')}
                    />
                  </Form>
                </Col>
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Site Name</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Site Name"
                      type="text"
                      name="siteName"
                      id=""
                      value={this.state.siteName}
                      onChange={this.handleInput('siteName')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Site URL"
                      type="text"
                      name="siteUrl"
                      id=""
                      value={this.state.siteUrl}
                      onChange={this.handleInput('siteUrl')}
                    />
                  </Form>
                </Col>
              </Row>

              <Row gutter={8} className="m-4" />
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.items.map((item, index) => {
                      return (
                        <div key={index}>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <h6>{'Author ' + (index + 1)}</h6>
                              <TextField
                                placeholder=" "
                                key={index}
                                fullWidth
                                variant="outlined"
                                label="Items Url"
                                name="url"
                                id=""
                                value={item.url}
                                onChange={this.handleItem('items', 'url', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
              </Row>
              <Row>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleItemClick('items')}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
            <Card>
              <Row>
                <Col md={24}>
                  <Form>
                    {this.state.imageItems.map((item, index) => {
                      return (
                        <div key={index}>
                          <Row gutter={8} className="m-4">
                            <Col md={24}>
                              <h6>{'Image ' + (index + 1)}</h6>
                              <TextField
                                placeholder=" "
                                fullWidth
                                variant="outlined"
                                label="Image Url"
                                name="image"
                                id=""
                                value={item.url}
                                onChange={this.handleItem('imageItems', 'url', index)}
                              />
                            </Col>
                          </Row>
                          <Divider variant="middle" />
                        </div>
                      )
                    })}
                  </Form>
                </Col>
                <Col md={24} className="m-4" style={{ textAlign: 'center' }}>
                  <Button
                    style={{ marginRight: '30px' }}
                    disableFocusRipple
                    variant="contained"
                    onClick={this.handleImageItemClick()}
                  >
                    <AddBox />
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={5}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={4}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Skeleton
