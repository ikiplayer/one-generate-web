import React, { PureComponent } from 'react'
import { Helmet } from 'react-helmet'
import { Link } from 'react-router-dom'

export default class Index extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: '',
      helmetDescription: 'Generate code for bootstrap',
      siteName: 'OneGenerate',
      digitRendering: [],
    }
  }
  componentDidMount() {
    this.randomDigit()
  }
  randomDigit() {
    for (let index = 0; index < 10; index++) {
      let zeroOrOne = Math.random() > 0.5 ? 1 : 0
      this.state.digitRendering.push(zeroOrOne)
    }

    // let zeroOrOne = Math.random() > 0.5 ? 1 : 0
    // return zeroOrOne
  }
  render() {
    return (
      <div
        style={{
          minHeight: 'calc(100vh - 500px)',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Helmet title={this.state.helmetTitle}>
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        {this.state.digitRendering.map(item => {
          return <h1 style={{ fontSize: '200px' }}>{item}</h1>
        })}
        {/* <div
          style={{
            // maxWidth: '560px',
            backgroundColor: '#fff',
            padding: '80px 30px',
            margin: '100px auto',
            borderRadius: '10px',
            flex: '1',
          }}
        >
          <div
            style={{
              maxWidth: '400px',
              margin: '0 auto',
            }}
          >
            <h1 className="font-size-36 mb-2">Page not found</h1>

            <p className="mb-3">The page is deprecated, deleted, or does not exist at all</p>
            <h1 className="font-size-80 mb-4 font-weight-bold">404 —</h1>
            <Link to="/" className="btn">
              &larr; Go back to the home page
            </Link>
          </div>
        </div> */}
      </div>
    )
  }
}
